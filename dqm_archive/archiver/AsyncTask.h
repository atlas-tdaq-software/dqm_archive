#ifndef DQM_ARCHIVE_ASYNCTASK_H
#define DQM_ARCHIVE_ASYNCTASK_H

//-----------------
// C/C++ Headers --
//-----------------
#include <chrono>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 * @ingroup dqm_archive
 *
 * @brief Interface for asyncronous tasks.
 * 
 * This interface represents any background activity that can run in a
 * separate thread or process. The activity can potentially continue even
 * after the instance if the Task is destroyed, it will just mean that
 * one cannot interact with that activity any more.
 * 
 * It is assumed that activity should start immediately after instance of
 * this class is created, there is no separate abstract method to start the
 * activity, though implementations of this interface can provide additional
 * methods for that.
 */

class AsyncTask {
public:

    /// Status value returned from wait()
    enum class TaskStatus {
        Success,   // Task completed succesfully
        Failure,   // Task completed but failed
    };

    /// Status value returned from waitFor() or waitUntil() method
    enum class WaitStatus {
        Success,   // Task completed succesfully
        Failure,   // Task completed but failed
        Timeout    // Taks has not completed yet
    };

    AsyncTask() = default;

    // copy is disabled
    AsyncTask(AsyncTask const&) = delete;
    AsyncTask& operator=(AsyncTask const&) = delete;

    virtual ~AsyncTask() = default;

    /**
     * Return task name.
     */
    virtual std::string name() const = 0;

    /**
     * Check current status of the task.
     * 
     * Returns true if task is still running
     */
    virtual bool isRunning() = 0;

    /**
     * Wait for task to complete.
     * 
     * Returns task completion status.
     */
    virtual TaskStatus wait() = 0;

    /**
     * Wait for task to complete up to max. `timeout` interval.
     * 
     * Returns either task completion status (Success/Failure) or Timeout if
     * task is still when timeout is reached.
     */
    virtual WaitStatus waitFor(std::chrono::milliseconds timeout) = 0;

    /**
     * Wait until `timePoint` for task to complete.
     * 
     * Returns either task completion status (Success/Failure) or Timeout if
     * task is still when timeout is reached.
     */
    virtual WaitStatus waitUntil(std::chrono::steady_clock::time_point timePoint) = 0;

};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_ASYNCTASK_H
