#ifndef DQM_ARCHIVE_RESULTQUEUE_H
#define DQM_ARCHIVE_RESULTQUEUE_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <vector>
#include <utility>
#include <mutex>
#include <chrono>
#include <condition_variable>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 *  @ingroup dqm_archive
 *
 *  @brief Specialized queue class for DQMF results (or any data type).
 *
 *  Queue stores items which are pairs of string and some arbitrary type.
 *  String represents the name of the object (parameter name in DQM) and
 *  arbitrary type is either dqmf::is::Result or shared_ptr<dqm_core::Result>.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

template <typename T>
class ResultQueue  {
public:

    typedef std::pair<std::string, T> value_type;
    typedef std::vector<value_type> cont_type;

    // Constructor
    explicit ResultQueue(size_t maxSize=10000)
        : m_maxSize(maxSize), m_cont(), m_mutex(), m_condFull(), m_condEmpty()
    {
        // reserve full size in advance to avoid reallocation later,
        // reserve few extra entries for "forced" push.
        m_cont.reserve(maxSize+10);
    }

    // Copy constructor and assignment are disabled
    ResultQueue(const ResultQueue&) = delete;
    ResultQueue operator=(const ResultQueue&) = delete;

    // add one more result to the queue, if the queue is full and
    // force is false then wait until somebody calls pop() unless
    void push(value_type&& res, bool force = false) {
        std::unique_lock<std::mutex> qlock(m_mutex);

        // wait until we have an empty slot
        while (not force and m_cont.size() >= m_maxSize) {
          m_condFull.wait(qlock);
        }

        // store the data
        m_cont.push_back(std::move(res));

        // tell anybody waiting for queue to become non-empty
        m_condEmpty.notify_one();
    }

    // add one more result to the queue, if the queue
    // is full then wait until somebody calls pop() for a maximum
    // given timeout value. Returns true if push was successful, false
    // if timeout expired
    bool push(value_type&& res, const std::chrono::milliseconds& timeout) {
        std::unique_lock<std::mutex> qlock(m_mutex);

        // wait until we have an empty slot
        while (m_cont.size() >= m_maxSize) {
          if (m_condFull.wait_for(qlock, timeout) == std::cv_status::timeout) return false;
        }

        // store the data
        m_cont.push_back(std::move(res));

        // tell anybody waiting for queue to become non-empty
        m_condEmpty.notify_one();

        return true;
    }


    // return all results from a queue in one call, if the queue
    // is empty then wait until somebody calls push(). Container
    // passed as an argument must be empty before call.
    void pop(cont_type& cont) {
        std::unique_lock<std::mutex> qlock(m_mutex);

        // wait unil we have something in the queue
        while (m_cont.empty()) {
          m_condEmpty.wait(qlock);
        }

        // grab all data
        std::swap(m_cont, cont);

        // tell anybody waiting for queue to become non-full
        m_condFull.notify_one();
    }

    // Return true if queue is full
    bool full() const {
        std::unique_lock<std::mutex> qlock(m_mutex);
        return m_cont.size() >= m_maxSize;
    }

    // Return fraction of the queue capacity already filled.
    double usage() const {
        std::unique_lock<std::mutex> qlock(m_mutex);
        return double(m_cont.size()) /  double(m_maxSize);
    }

protected:

private:

    size_t m_maxSize;
    cont_type m_cont;
    mutable std::mutex m_mutex ;
    std::condition_variable m_condFull ;
    std::condition_variable m_condEmpty ;

};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_RESULTQUEUE_H
