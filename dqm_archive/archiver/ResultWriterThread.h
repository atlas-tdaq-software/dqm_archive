#ifndef DQM_ARCHIVE_RESULTWRITERTHREAD_H
#define DQM_ARCHIVE_RESULTWRITERTHREAD_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>
#include <mutex>
#include <string>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dqm_archive/archiver/ResultQueue.h"
#include "dqm_core/Result.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
class OWLSemaphore;
namespace dqm_archive {
class IDqmArchiver;
}

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 *  @ingroup dqm_archive
 *
 *  Thread which sends DQMF results to ROOT file.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class ResultWriterThread  {
public:

    typedef ResultQueue<std::shared_ptr<dqm_core::Result>> CoreResultQueue;

    /**
     *  Constructor starts the thread.
     *
     *  @param[in] arch       Archiver instance
     *  @param[in] queue      Input queue with the results
     *  @param[in] semaphore  Semaphore to be raised when this thread exits.
     */
    ResultWriterThread(std::unique_ptr<IDqmArchiver> arch,
        const std::shared_ptr<CoreResultQueue>& queue,
        OWLSemaphore& semaphore);
    
    // operator called by thread
    void operator()();

protected:

private:

    std::unique_ptr<IDqmArchiver> m_arch;
    std::shared_ptr<CoreResultQueue> m_queue;
    OWLSemaphore& m_semaphore;
};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_RESULTWRITERTHREAD_H
