#ifndef DQM_ARCHIVE_DQMAROOTINDEXER_H
#define DQM_ARCHIVE_DQMAROOTINDEXER_H
//--------------------------------------------------------------------------
// File and Version Information:
//  $Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
class TFile;

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 *  @ingroup dqm_archive
 *
 * Class which performs EoR indexing of the ROOT file.
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @author Andy Salnikov
 */

class DqmaRootIndexer {
public:

    /**
     *  @param resultTreeName:  name of the tree contained original results
     *  @param indexTreeName:  name of the new tree for indexed results
     *  @param statusTreeName:  name of the new tree for status indexes
     */
    DqmaRootIndexer(const std::string& resultTreeName,
                    const std::string& indexTreeName,
                    const std::string& statusTreeName);

    /// Perform indexing
    void index(TFile* file);

protected:

private:

    const std::string m_resultTreeName;
    const std::string m_indexTreeName;
    const std::string m_statusTreeName;

};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_DQMAROOTINDEXER_H
