#ifndef DQM_ARCHIVE_ASYNCTASKPROCESS_H
#define DQM_ARCHIVE_ASYNCTASKPROCESS_H

//-----------------
// C/C++ Headers --
//-----------------
#include <vector>
#include <string>
#include <chrono>
#include <sys/types.h>

//----------------------
// Base Class Headers --
//----------------------
#include "dqm_archive/archiver/AsyncTask.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 * @brief Asyncronous tasks implementation based on porcess running a command.
 * 
 * Asyncronous activity for this class is a process running a command provided
 * in class constructor.
 */

class AsyncTaskProcess : public AsyncTask {
public:

    /**
     * @brief Constructor creates new child process.
     *
     * If `background` is true then process will not be terminated when destructor
     * is called.
     *
     * @param commandLine  Command line (argv), first element is executable name.
     * @param pollTime     Polling interval.
     * @param background   If false then child process will be killed in destructor.
     *
     * @throw ErrnoError Thrown when fork system call fails.
     */
    AsyncTaskProcess(const std::vector<std::string>& commandLine,
                     const std::chrono::milliseconds& pollTime,
                     const std::string& name,
                     bool background = false);

    AsyncTaskProcess(const AsyncTaskProcess&) = delete;
    AsyncTaskProcess& operator=(const AsyncTaskProcess&) = delete;

    // Destructor terminates non-background process if still running
    ~AsyncTaskProcess();

    /**
     *  @brief Forces stop of the child process.
     *
     *  Sends sequence of signals [TERM, TERM, KILL] to the child waiting max 2 seconds
     *  after each signal for child to stop.
     */
    void terminate();

    /**
     * Return task name.
     */
    std::string name() const override;

    /**
     * Check current status of the task.
     * 
     * Returns true if task is still running
     */
    bool isRunning() override;

    /**
     * Wait for task to complete.
     * 
     * Returns task completion status.
     */
    TaskStatus wait() override;

    /**
     * Wait for task to complete up to max. `timeout` interval.
     * 
     * Returns either task completion status (Success/Failure) or Timeout if
     * task is still when timeout is reached.
     */
    WaitStatus waitFor(std::chrono::milliseconds timeout) override;

    /**
     * Wait until `timePoint` for task to complete.
     * 
     * Returns either task completion status (Success/Failure) or Timeout if
     * task is still when timeout is reached.
     */
    WaitStatus waitUntil(std::chrono::steady_clock::time_point timePoint) override;

private:

    /**
     *  @brief Sends a signal to child process.
     *
     *  @param[in] signum Signal number.
     */
    void kill (int signum);

    // Data members
    const std::chrono::milliseconds m_pollTime;
    const std::string m_name;
    bool m_background;       ///< If true then do not kill child process in destructor
    pid_t m_childPid = 0;    ///< Child process ID, set to 0 when process stops
    int m_status = -1;       ///< process return code

//------------------
// Static Members --
//------------------
public:

    /// Delay execution for a given period of time.
    static void tsleep (const std::chrono::milliseconds& pollTime);

};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_ASYNCTASKPROCESS_H
