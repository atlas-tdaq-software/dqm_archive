#ifndef DQM_ARCHIVE_DQMARESULTCONVERTER_H
#define DQM_ARCHIVE_DQMARESULTCONVERTER_H

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>

//----------------------
// Base Class Headers --
//----------------------
#include "is/inforeceiver.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dqm_archive/archiver/DqmaRootReceiver.h"
#include "dqm_archive/archiver/ResultQueue.h"
#include "dqm_core/Result.h"
#include "dqmf/is/Result.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 *  @ingroup dqm_archive
 *
 *  Class which is responsible for retrieving OH histograms for
 *  DQM results. It receives results from a queue populated by
 *  DqmaResultReceiver, retrieves histograms if necessary, adds
 *  them to result and pushes result to output queue. It has to
 *  run in a separate thread.
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @author Andy Salnikov
 */

class DqmaResultConverter {
public:

    typedef ResultQueue<std::shared_ptr<dqmf::is::Result>> IsResultQueue;
    typedef ResultQueue<std::shared_ptr<dqm_core::Result>> CoreResultQueue;

    // constructor
    DqmaResultConverter(
        const std::shared_ptr<IsResultQueue>& inQueue,
        const std::shared_ptr<CoreResultQueue>& outQueue,
        const std::shared_ptr<DqmaRootReceiver>& rootReceiver
    );

    // operator called by thread
    void operator()();

    // process single buffer of input results, only made public for testing
    void _process_buffer(IsResultQueue::cont_type const& input_results);

protected:

private:

    IPCPartition m_partition;
    std::shared_ptr<IsResultQueue> m_inQueue;
    std::shared_ptr<CoreResultQueue> m_outQueue;
    std::shared_ptr<DqmaRootReceiver> m_rootReceiver;
};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_DQMARESULTCONVERTER_H
