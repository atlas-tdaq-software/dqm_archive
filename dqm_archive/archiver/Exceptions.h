#ifndef DQM_ARCHIVE_ARCHIVE_EXCEPTIONS_H
#define DQM_ARCHIVE_ARCHIVE_EXCEPTIONS_H

//-----------------
// C/C++ Headers --
//-----------------
#include <cerrno>
#include <cstring>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include <ers/ers.h>

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

namespace dqm_archive {

ERS_DECLARE_ISSUE(errors, Exception, ERS_EMPTY, ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE(errors,
        FileNameTemplateIdError,
        errors::Exception,
        "Unknown identifier \"" << id << "\" in file name template \"" << templateString << "\"",
        ERS_EMPTY,
        ((std::string)templateString)
        ((std::string)id)
)

ERS_DECLARE_ISSUE_BASE(errors,
        ErrnoError,
        errors::Exception,
        "Call to `" << syscall << "' failed with error: " << ::strerror(errno),
        ERS_EMPTY,
        ((std::string) syscall)
)


} // namespace dqm_archive

#endif // DQM_ARCHIVE_ARCHIVE_EXCEPTIONS_H
