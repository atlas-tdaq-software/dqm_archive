#ifndef DQM_ARCHIVE_ASYNCTASKTHREAD_H
#define DQM_ARCHIVE_ASYNCTASKTHREAD_H

//-----------------
// C/C++ Headers --
//-----------------
#include <chrono>
#include <future>
#include <thread>

//----------------------
// Base Class Headers --
//----------------------
#include "dqm_archive/archiver/AsyncTask.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 * @ingroup dqm_archive
 *
 * @brief Asyncronous tasks implementation based on thread and a future.
 * 
 * Asyncronous activity for this class is a thread passed to a class
 * constructor. To signal thread completion and return status value a
 * std::future<bool> is used which is supposed to be set by thread.
 *
 * If the instance is destroyed while thread is still executing thread
 * will be detached.
 */

class AsyncTaskThread : public AsyncTask {
public:

    AsyncTaskThread(std::thread&& thread, std::future<bool>&& future, const std::string& name);

    // copy is disabled
    AsyncTaskThread(AsyncTaskThread const&) = delete;
    AsyncTaskThread operator=(AsyncTaskThread const&) = delete;

    ~AsyncTaskThread() override;

    /**
     * Return task name.
     */
    std::string name() const override;

    /**
     * Check current status of the task.
     * 
     * Returns true if task is still running
     */
    bool isRunning() override;

    /**
     * Wait for task to complete.
     * 
     * Returns task completion status.
     */
    TaskStatus wait() override;

    /**
     * Wait for task to complete up to max. `timeout` interval.
     * 
     * Returns either task completion status (Success/Failure) or Timeout if
     * task is still when timeout is reached.
     */
    WaitStatus waitFor(std::chrono::milliseconds timeout) override;

    /**
     * Wait until `timePoint` for task to complete.
     * 
     * Returns either task completion status (Success/Failure) or Timeout if
     * task is still when timeout is reached.
     */
    WaitStatus waitUntil(std::chrono::steady_clock::time_point timePoint) override;

protected:

    /**
     * Default constructor, should only be used by sub-classes.
     * See setThread() method for details.
     */
    explicit AsyncTaskThread(const std::string& name) : m_name(name) {}

    /**
     * When constructing sub-classes of this class there could be a problem
     * providing both a thread instance and a future instance to a non-default
     * constructor of this class (they both need an instance of a promise).
     * For these cases it is more to initialize things in constructor
     * body using setThread() method and a default constructor.
     */
    void setThread(std::thread&& thread, std::future<bool>&& future);

private:

    std::thread m_thread;
    std::future<bool> m_future;
    const std::string m_name;
    WaitStatus m_status = WaitStatus::Timeout;
};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_ASYNCTASKTHREAD_H
