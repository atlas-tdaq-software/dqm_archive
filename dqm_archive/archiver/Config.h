#ifndef DQM_ARCHIVE_CONFIG_H
#define DQM_ARCHIVE_CONFIG_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include <chrono>
#include <boost/filesystem.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "config/Configuration.h"
#include "dqm_archive/archiver/DqmArchiveMgr.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace daq {
  namespace core {
    class Partition;
  }
}

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 *  @ingroup dqm_archive
 *
 *  Class responsible for configuration part of the archiving process.
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class Config  {
public:

    typedef std::map<std::string, std::vector<std::string>> ParamHistMap;

    // constructor
    Config();

    // Destructor
    ~Config();

    /**
     *  Create and return archiver manager instance for given run number
     */
    std::unique_ptr<DqmArchiveMgr> makeArchiver(unsigned run);

protected:

    void readParamTree(Configuration& config, const daq::core::Partition& partition);

private:

    std::string m_serverName;           ///< IS server name to read data from
    unsigned m_queueSize;               ///< Size of the result queue
    bool m_archiveHist;                 ///< Archive histograms if true
    boost::filesystem::path m_storageDir;  ///< Directory where to create files
    std::string m_dataset;              ///< CoCa dataset name
    std::string m_cocaPartition;        ///< CoCa server partition name
    std::string m_cocaServerName;       ///< CoCa server name
    int m_cocaPriority;                 ///< Priority for files in coca
    unsigned m_cocaDTC;                 ///< DTC for files in coca
    unsigned m_updateInterval;          ///< Seconds between updates when result is not changing
    unsigned m_checkpointMinutes;       ///< Minutes between flushing trees to ROOT file
    unsigned m_maxFileTimeIntervalSec;  ///< Limit on time period stored in one file (seconds, 0 for no limit)
    unsigned m_maxFileResultCount;      ///< Limit on number of results stored in one file (0 for no limit)
    unsigned m_finalizeTimeoutSec;      ///< Timeout on finalization process in seconds (0 means no timeout)
    std::string m_fileNameTemplate;     ///< Template for ROOT file names
    unsigned m_isWorkerThreads;         ///< number of IS worker threads
    std::map<std::string, std::string> m_paramTree;  ///< maps parameter name to its parent parameter name or empty string for top-level regions
    std::shared_ptr<ParamHistMap> m_paramHist;  ///< maps parameter name to the list of histogram names
};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_CONFIG_H
