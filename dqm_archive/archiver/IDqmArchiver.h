#ifndef DQM_ARCHIVE_IDQMARCHIVER_H
#define DQM_ARCHIVE_IDQMARCHIVER_H

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>
#include <string>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dqm_core/Result.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 *  @ingroup dqm_archive
 *
 *  @brief Interface for DQM Archiver Database.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class IDqmArchiver : boost::noncopyable {
public:

    // Destructor
    virtual ~IDqmArchiver() = default;

    IDqmArchiver(const IDqmArchiver&) = delete;
    IDqmArchiver& operator=(const IDqmArchiver&) = delete;

    /**
     *  @brief Store new DQMF result.
     *
     *  The results that are passed to this methods should be approximately
     *  ordered in time, if they are unordered it should not be over too
     *  long period. If order is broken too much then some fraction of results
     *  may not be stored.
     *
     *  @param[in] name       DQParameter name
     *  @param[in] result     New result to store
     *  @return   True if store was successful, false if time order is violated.
     */
    virtual bool store(const std::string& name, const std::shared_ptr<dqm_core::Result>& result) = 0;

    /**
     *  @brief Finish archiving, close all output files, etc.
     *
     *  Implementation should finalize its activity by closing everything and
     *  optionally sending its collected data to long-term storage. Returns
     *  boolean value which is true if the process completed successfully.
     *
     *  Note that finalization is expected to finish promptly to avoid delays
     *  in run control transitions. If finalization takes longer than expected
     *  then implementation is allowed to make best effort in saving data,
     *  e.g. run finalization thread/process in background after returning
     *  from this method (or even after destruction of this object).
     */
    virtual void finish() = 0;

protected:

    // Default constructor
    IDqmArchiver() = default;
};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_IDQMARCHIVER_H
