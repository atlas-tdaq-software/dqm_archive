#ifndef DQM_ARCHIVE_DQMARESULTRECEIVER_H
#define DQM_ARCHIVE_DQMARESULTRECEIVER_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <map>
#include <vector>

//----------------------
// Base Class Headers --
//----------------------
#include "is/inforeceiver.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dqm_archive/archiver/ResultQueue.h"
#include "dqmf/is/Result.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
class IPCPartition;

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 *  @ingroup dqm_archive
 *
 *  C++ source file code template. The first sentence is a brief summary of 
 *  what the class is for. It is followed by more detailed information
 *  about how to use the class. This doc comment must immediately precede the 
 *  class definition.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class DqmaResultReceiver : public ISInfoReceiver {
public:

    typedef ResultQueue<std::shared_ptr<dqmf::is::Result>> IsResultQueue;
    typedef std::map<std::string, std::vector<std::string>> ParamHistMap;

    // constructor
    DqmaResultReceiver(const IPCPartition& partition, const std::string& server,
            const std::shared_ptr<IsResultQueue>& queue, bool archiveHist,
            unsigned updateInterval, unsigned isWorkerThreads,
            const std::shared_ptr<ParamHistMap>& paramHist);

    // Destructor
    ~DqmaResultReceiver();

    // subscribe to data from a server
    void start();

    // stop subscription, guaranteed that no new  data will be pushed
    // into queue after this method completes
    void stop();

    // callback method called on every update of IS objects,
    // pay attention, this may be called from many threads simultaneously
    void callback(ISCallbackInfo* cb);

protected:

    // return true if histograms need to be archived for the result
    bool needHistos(const std::string& param,
            dqmf::is::Result::Status newStatus);

private:

    // Structure which keeps last saved result
    class LastResult {
    public:
        LastResult() {}
        explicit LastResult(const dqmf::is::Result& isResult);
        // get status
        dqmf::is::Result::Status status() const { return m_status; }
        // Returns true if result value is the same and time difference is less than delta seconds
        bool changed(const LastResult& other, unsigned delta) const;
    private:
        boost::posix_time::ptime  m_time;
        dqmf::is::Result::Status m_status;
        std::vector<std::pair<std::string, double>> m_tags;
    };

    const std::string m_server;
    std::shared_ptr<IsResultQueue> m_queue;
    const bool m_archiveHist;
    const unsigned m_updateInterval; ///< Seconds between updates when result is not changing
    std::shared_ptr<ParamHistMap> m_paramHist;    ///< maps parameter name to the list of histogram names
    const ISCriteria m_criteria;
    bool m_subscribed;
    std::map<std::string, LastResult> m_prevResult; // map parameter name to previous result
    std::mutex m_callbackMutex;       // to synchronize operations in callback()
    unsigned long m_counter;    // total count of results received since start()
    unsigned long m_skipped;      // count of results filtered out since start()
};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_DQMARESULTRECEIVER_H
