#ifndef DQM_ARCHIVE_DQMARCHIVERSQLITE_H
#define DQM_ARCHIVE_DQMARCHIVERSQLITE_H

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <deque>
#include <map>
#include <memory>
#include <chrono>
#include <cstdint>
#include <sqlite3.h>

//----------------------
// Base Class Headers --
//----------------------
#include "dqm_archive/archiver/IDqmArchiver.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dqm_config/dal/DQRegion.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 *  @ingroup dqm_archive
 *
 *  @brief Implementation of IDqmArchiver which stores data in SQLite database.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class DqmArchiverSQLite : public IDqmArchiver {
public:

    /**
     *  @param[in] fileName Path to the database file, does not need to exist.
     *  @param[in] paramTree Map from parameter name to its parent parameter name.
     */
    DqmArchiverSQLite(const std::string& fileName,
                      const std::map<std::string, std::string>& paramTree);

    // Destructor
    ~DqmArchiverSQLite() override;

    DqmArchiverSQLite(const DqmArchiverSQLite&) = delete;
    DqmArchiverSQLite& operator=(const DqmArchiverSQLite&) = delete;

    bool store(const std::string& name, const std::shared_ptr<dqm_core::Result>& result) override;

    void finish() override;

protected:

    // write count of results from buffer into file
    void flush(unsigned count);

    /**
     *  @brief Store DQM configuration.
     *
     *  Store parameter configuration information. Currently takes the
     *  parameter tree in the form of map from parameter name to its parent
     *  parameter name or to empty string if parameter is top-level.
     *
     *  This method must be called before any other calls to store().
     *
     *  @param[in] paramTree Map from parameter name to its parent parameter name.
     */
    void storeConfig(const std::map<std::string, std::string>& paramTree);

private:

    typedef std::pair<std::string, std::shared_ptr<dqm_core::Result>> ResultPair;
    typedef std::uint32_t ParmId;
    typedef std::uint32_t ResultId;
    typedef std::uint32_t TagId;
    typedef std::int32_t  Status;

    sqlite3 *m_ppDb;              ///< Database handle
    std::map<std::string, ParmId> m_parm2id;  ///< Mapping from parameter name to parm_id
    std::map<std::string, TagId> m_tag2id;  ///< Mapping from tag name to tag ID
    std::deque<ResultPair> m_buffer;   ///< Temporary buffer
    unsigned m_resCount;
    unsigned m_tagCount;
    std::map<std::pair<ParmId, Status>, std::uint32_t> m_statusCount; ///< counter for each <result ID, status> pair
};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_DQMARCHIVERSQLITE_H
