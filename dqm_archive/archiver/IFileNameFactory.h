#ifndef DQM_ARCHIVE_IFILENAMEFACTORY_H
#define DQM_ARCHIVE_IFILENAMEFACTORY_H

//-----------------
// C/C++ Headers --
//-----------------
#include <boost/filesystem.hpp>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 * @ingroup dqm_archive
 *
 * @brief Interface for classes implementing Long-Term Storage archiving methods.
 *
 * This is rather simple interface as it have just a single method called
 * `store()` which takes the name of the file to save in the archive. This
 * method is called by DQM archiver to send archived data to external storage,
 * for example CoCa.
 */

class IFileNameFactory {
public:

    // Destructor
    virtual ~IFileNameFactory() = default;

    /**
     *  @brief Make next file name.
     *
     *  @return File name to use for next DQM archive.
     */
    virtual boost::filesystem::path makeFileName() = 0;

};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_IFILENAMEFACTORY_H
