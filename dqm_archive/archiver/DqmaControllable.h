#ifndef DQM_ARCHIVE_DQMACONTROLLABLE_H
#define DQM_ARCHIVE_DQMACONTROLLABLE_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>

//----------------------
// Base Class Headers --
//----------------------
#include "RunControl/Common/Controllable.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dqm_archive/archiver/Config.h"
#include "dqm_archive/archiver/DqmArchiveMgr.h"
#include "ipc/partition.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 *  @ingroup dqm_archive
 *
 * Implementatuin of the COntrollable which drives archiving process.
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class DqmaControllable : public daq::rc::Controllable {
public:

    // Default constructor
    DqmaControllable();

    // Destructor
    virtual ~DqmaControllable() throw();

private:

    virtual void configure(const daq::rc::TransitionCmd& cmd);
    virtual void prepareForRun(const daq::rc::TransitionCmd& cmd);
    virtual void stopArchiving(const daq::rc::TransitionCmd& cmd);
    virtual void unconfigure(const daq::rc::TransitionCmd& cmd);

protected:

private:

    std::unique_ptr<Config> m_config;
    std::unique_ptr<DqmArchiveMgr> m_archiver;

};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_DQMACONTROLLABLE_H
