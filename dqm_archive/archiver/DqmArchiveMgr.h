#ifndef DQM_ARCHIVE_DQMARCHIVEMGR_H
#define DQM_ARCHIVE_DQMARCHIVEMGR_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>
#include <mutex>
#include <thread>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dqm_archive/archiver/IDqmArchiver.h"
#include "dqm_archive/archiver/DqmaResultReceiver.h"
#include "dqm_archive/archiver/ResultQueue.h"
#include "dqm_core/Result.h"
#include "dqmf/is/Result.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 *  @ingroup dqm_archive
 *
 *  Class that manages archiving process.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class DqmArchiveMgr  {
public:

    typedef std::map<std::string, std::vector<std::string>> ParamHistMap;

    // Default constructor
    DqmArchiveMgr (const IPCPartition& partition,
            std::unique_ptr<IDqmArchiver> archiver,
            const std::string& serverName,
            unsigned queueSize,
            bool archiveHist,
            unsigned updateInterval,
            unsigned isWorkerThreads,
            const std::shared_ptr<ParamHistMap>& paramHist,
            std::mutex& rootMutex);
    
    // Destructor
    ~DqmArchiveMgr () ;

    /**
     *  Start archiving, can only be called once
     */
    void start();

    /**
     *  Stop archiving, can only be called once (after start()).
     *
     *  All activities will be stopped, and writer thread is stopped. If "detach"
     *  option is set to true then writer thread is detached instead of joined. It
     *  will result in faster return from this method (writer thread will continue
     *  to run in background until it finishes its job). Use with caution, if
     *  application exits before writer thread finishes then bad things will happen.
     *  Typically it may be set to true during normal EOR sequence, as EOR will not
     *  kill application immediately.
     *
     *  @param[in] detach  If true then writer thread is detached instead of joined.
     */
    void stop(bool detach=false);

protected:

private:

    typedef ResultQueue<std::shared_ptr<dqmf::is::Result>> IsResultQueue;
    typedef ResultQueue<std::shared_ptr<dqm_core::Result>> CoreResultQueue;

    IPCPartition m_partition;
    std::unique_ptr<IDqmArchiver> m_archiver;     ///< Archiver instance
    const std::string m_serverName;               ///< Name of the IS server to get data from
    const unsigned m_queueSize;                   ///< timeout for actions like finalizing output
    const bool m_archiveHist;                     ///< Archive histograms if true
    const unsigned m_updateInterval;              ///< Seconds between updates when result is not changing
    const unsigned m_isWorkerThreads;             ///< Number of IS workers
    std::shared_ptr<ParamHistMap> m_paramHist;    ///< maps parameter name to the list of histogram names
    std::mutex& m_rootMutex;                      ///< Mutex to synchronize access to ROOT
    std::shared_ptr<IsResultQueue> m_isQueue;     ///< In-flight result storage for IS result objects
    std::shared_ptr<CoreResultQueue> m_coreQueue; ///< In-flight result storage for DQMF result objects
    std::thread m_writerThread;                   ///< Thread which writes results to archive
    std::thread m_cvtThread;                      ///< Thread which converts data from IS representation
    std::unique_ptr<DqmaResultReceiver> m_recv;   ///< Receiver for IS data
};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_DQMARCHIVEMGR_H
