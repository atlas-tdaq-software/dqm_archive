#ifndef DQM_ARCHIVE_FILENAMEFACTORYTEMPLATE_H
#define DQM_ARCHIVE_FILENAMEFACTORYTEMPLATE_H

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <vector>

//----------------------
// Base Class Headers --
//----------------------
#include "dqm_archive/archiver/IFileNameFactory.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 * @ingroup dqm_archive
 *
 * @brief Implementation of IFileNameFactory based on template string.
 *
 * This implementation is based on substituting braced identifiers in template
 * string with equivalent values. Braced identifier can take the form:
 *
 *      "{identifier}" - replaced with corresponding value.
 *      "{identifier:NUM}" - same as above but replacement value will be
 *          formatted to fit at least NUM characters, padded with 0
 *          on left side
 * 
 * Identifiers are alpha-numeric strings with underscores. Note that
 * non-identifier strings inside curly braces are ignored and passed to the
 * output without change, e.g. "{partition-run}" will be copied to output
 * literally with braces.
 *
 * This class supports following identifiers:
 * 
 *      run - run number (specified in constructor).
 *      partition - partition name (specified in constructor).
 *      seq - sequence number, starting with 0, incremented on every call
 *          to makeFileName()
 *      seq1 - sequence number, starting with 1, incremented on every call
 *          to makeFileName(). Note that "seq" and "seq1" share the same
 *          counter, it is not specified what happens if seq/seq1 appear more
 *          than once in the same template.
 *      timesec - current timestamp (at the time of makeFileName() call)
 *          formatted as number of seconds since Epoch
 *
 * Identifiers that are not in this list will cause exception in class
 * constructor.
 *
 * Few examples of the template strings:
 * 
 *      "dqm_results-{partition}-{run:10}.root"
 *      "dqm_results-{partition}-{run:10}-{seq:6}.root"
 *      "dqm_results-{partition}-{run:10}-{timesec:10}.root"
 */

class FileNameFactoryTemplate : public IFileNameFactory {
public:

    /**
     * @param templateString  Template string
     * @param dir             Directory for a file
     * @param parition        Partition name
     * @param run             Run number to use
     * 
     * @except FileNameTemplateIdError if template string contains unknown id.
     */
    FileNameFactoryTemplate(const std::string& templateString,
                            const boost::filesystem::path& dir,
                            const std::string& partition,
                            unsigned run);

    FileNameFactoryTemplate(const FileNameFactoryTemplate&) = delete;
    FileNameFactoryTemplate& operator=(const FileNameFactoryTemplate&) = delete;

    // Destructor
    ~FileNameFactoryTemplate() override = default;

    /**
     * @brief Make next file name.
     *
     * Returned file path starts with a path of the directory specified in
     * construcor.
     *
     * @return File name to use for next DQM archive.
     */
    boost::filesystem::path makeFileName() override;

private:

    // Position of the matched identifier in tempalte string
    struct IdMatch {
        std::string id;
        unsigned begin;
        unsigned end;
        unsigned width;
    };

    // return formatted identifier value
    std::string _formatMatch(const IdMatch& match);

    const std::string m_templateString;
    const boost::filesystem::path m_dir;
    const std::string m_partition;
    const unsigned m_run;
    unsigned m_seq = 0;
    std::vector<IdMatch> m_matches;
};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_FILENAMEFACTORYTEMPLATE_H
