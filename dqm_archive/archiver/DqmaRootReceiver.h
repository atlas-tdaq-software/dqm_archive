#ifndef DQM_ARCHIVE_DQMAROOTRECEIVER_H
#define DQM_ARCHIVE_DQMAROOTRECEIVER_H

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>
#include <mutex>
#include <string>
#include <vector>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ipc/partition.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
class TObjArray;

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 *  @ingroup dqm_archive
 *
 *  Class which is responsible for retrieving histograms from OH server.
 */

class DqmaRootReceiver {
public:

    // constructor
    DqmaRootReceiver() = default;

    DqmaRootReceiver(DqmaRootReceiver const&) = delete;
    DqmaRootReceiver& operator=(DqmaRootReceiver const&) = delete;

    virtual ~DqmaRootReceiver() = default;

    // Retrieve histograms with given names
    virtual std::unique_ptr<TObjArray> get_histograms(std::vector<std::string> const& objects) = 0;

private:

};

/**
 *  @ingroup dqm_archive
 *
 *  Default implementation of DqmaRootReceiver.
 */
class DqmaDefaultRootReceiver : public DqmaRootReceiver {
public:

    // constructor
    DqmaDefaultRootReceiver(const IPCPartition& partition, std::mutex& rootMutex);

    DqmaDefaultRootReceiver(DqmaRootReceiver const&) = delete;
    DqmaDefaultRootReceiver& operator=(DqmaRootReceiver const&) = delete;

    virtual ~DqmaDefaultRootReceiver() = default;

    // Retrieve histograms with given names
    virtual std::unique_ptr<TObjArray> get_histograms(std::vector<std::string> const& objects);

private:

    IPCPartition m_partition;
    std::mutex& m_rootMutex;      ///< Mutex to synchronize access to ROOT
};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_DQMAROOTRECEIVER_H
