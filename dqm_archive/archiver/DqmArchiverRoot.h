#ifndef DQM_ARCHIVE_DQMARCHIVERROOT_H
#define DQM_ARCHIVE_DQMARCHIVERROOT_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <boost/filesystem.hpp>
#include <chrono>
#include <cstdint>
#include <deque>
#include <map>
#include <memory>
#include <mutex>
#include <string>

//----------------------
// Base Class Headers --
//----------------------
#include "dqm_archive/archiver/IDqmArchiver.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dqm_archive/archiver/ILTSArchiver.h"
#include "dqm_archive/common/RTypes.h"
#include "dqm_archive/common/TTreeSequence.h"
#include "dqm_config/dal/DQRegion.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
class TFile;
class TTree;
namespace dqm_archive {
class AsyncTaskList;
class IFileNameFactory;
}

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 * @ingroup dqm_archive
 *
 * @brief Implementation of IDqmArchiver which stores data in ROOT files.
 *
 * Note that methods of this class are only called from a single thread
 * (see ResultWriterThread) so it does not provide any thread safety
 * guarantees. Operations with ROOT files are protected/synchronized
 * via locking the mutex passed to constructor of the class (mutex is
 * shared with all other classes that use ROOT).
 */

class DqmArchiverRoot : public IDqmArchiver {
public:

    /**
     * @param fileNameFactory Factory object for ROOT file names
     * @param ltsArchiver Long-term storage archiver, can be nullptr
     * @param paramTree Map of parameter name to it's parent parameter name
     * @param checkpointMinutes Interval in minutes between checkpoints
     * @param rootMutex Mutex to protect ROOT operations
     */
    DqmArchiverRoot(std::unique_ptr<IFileNameFactory> fileNameFactory,
                    std::shared_ptr<ILTSArchiver> ltsArchiver,
                    const std::map<std::string, std::string>& paramTree,
                    unsigned checkpointMinutes,
                    unsigned maxFileTimeIntervalSec,
                    unsigned maxFileResultCount,
                    std::chrono::milliseconds finalizeTimeout,
                    std::mutex& rootMutex);

    // Destructor
    ~DqmArchiverRoot() override;

    DqmArchiverRoot(const DqmArchiverRoot&) = delete;
    DqmArchiverRoot& operator=(const DqmArchiverRoot&) = delete;

    bool store(const std::string& name, const std::shared_ptr<dqm_core::Result>& result) override;

    void finish() override;

protected:

    // stores all file-related info
    struct FileContext {

        // Opens a file and creates all trees
        explicit FileContext(boost::filesystem::path const& path_);

        // set trees' entries and flush them to the file
        void flush();

        boost::filesystem::path path;         ///< Path to the ROOT file
        std::unique_ptr<TFile> file;          ///< ROOT file
        time_t lastCheckpoint = 0;            ///< time of the last checkpoint

        TTree* resultTree = 0;                ///< tree with the results
        TTreeSequence<RTypes::ResultRepr> resultSeq;

        TTree* tagNameTree = 0;               ///< tree with the names of the tags
        TTreeSequence<TString*> tagNameSeq;

        TTree* tagTree = 0;                   ///< tree with the tag values
        TTreeSequence<RTypes::TagValueRepr> tagValueSeq;

        TTree* histoTree = 0;                 ///< tree with serialized histograms
        TTreeSequence<TString*> histoSeq;
        TTreeSequence<RTypes::HistoIdRepr> histoIdSeq;

        std::map<std::string, RTypes::TagId> tag2id;  ///< Mapping from tag name to tag ID

        RTypes::Timestamp firstTimeStamp = 0; ///< Timestamp of the first result stored in file (microsec)
        RTypes::Timestamp lastTimeStamp = 0;  ///< Timestamp of the last result stored in file (microsec)
        uint64_t resultCount = 0;             ///< Total number of results stored in file
    };

    // Open next ROOT file
    std::unique_ptr<FileContext> openNextFile();

    // write count of results from buffer into file
    void flush(unsigned count);

    // Close current file, index it, and send to CoCa.
    // Finalization can be executed asynchronously if `async` parameter is true.
    void finalize(bool async);

    /**
     * @brief Store parameters configuration.
     *
     * This method must be called once for every new file.
     *
     * @param fdata Context for currently open file.
     */
    void storeConfig(FileContext& fcontext);

    /**
     * Return true if it is time to switch to a new ROOT file.
     */
    bool decideSwitch();

private:

    // Result name and result pointer
    using ResultPair = std::pair<std::string, std::shared_ptr<dqm_core::Result>>;

    std::unique_ptr<IFileNameFactory> m_fileNameFactory;  ///< Factory of ROOT file names
    std::shared_ptr<ILTSArchiver> m_ltsArchiver;  ///< LTS archiver instance, possibly null
    const unsigned m_checkpointMinutes;   ///< Minutes between flushing trees to ROOT file
    const unsigned m_maxFileTimeIntervalSec;  ///< Limit on time period stored in one file (seconds, 0 for no limit)
    const unsigned m_maxFileResultCount;  ///< Limit on number of results stored in one file (0 for no limit)
    const std::chrono::milliseconds m_finalizeTimeout;  ///< Timeout on finalization process in (0 means no timeout)
    std::mutex& m_rootMutex;      ///< Mutex to synchronize access to ROOT

    unsigned long m_counter = 0;   ///< Counter of results received
    unsigned long m_dropped = 0;   ///< Counter of results dropped

    std::deque<ResultPair> m_buffer;   ///< Temporary buffer for ordering
    std::map<std::string, RTypes::ParmId> m_parm2id;  ///< Mapping from parameter name to parm_id
    std::vector<int> m_parm2parentId;  ///< "Map" opf parameter ID to its parent ID (or -1 if no parent)

    std::unique_ptr<FileContext> m_fileContext;   ///< Context info for currently open TFile
    boost::posix_time::ptime m_lastStored;  ///< Timestamp of the last result saved to (any) file

    unsigned m_statResults = 0;      ///< Counter for saved results
    unsigned m_statTags = 0;         ///< Counter for saved tags
    unsigned m_statHistos = 0;       ///< Counter for saved histograms

    std::shared_ptr<AsyncTaskList> m_finalizers;
};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_DQMARCHIVERROOT_H
