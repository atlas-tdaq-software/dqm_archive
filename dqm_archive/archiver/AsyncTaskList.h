#ifndef DQM_ARCHIVE_ASYNCTASKLIST_H
#define DQM_ARCHIVE_ASYNCTASKLIST_H

//-----------------
// C/C++ Headers --
//-----------------
#include <list>
#include <memory>
#include <string>

//----------------------
// Base Class Headers --
//----------------------
#include "dqm_archive/archiver/AsyncTask.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 * @ingroup dqm_archive
 *
 * @brief Task class which a set of AsyncTask instances.
 */

class AsyncTaskList : public AsyncTask {
public:

    explicit AsyncTaskList(const std::string& name="") : m_name(name) {}

    // copy is disabled
    AsyncTaskList(AsyncTaskList const&) = delete;
    AsyncTaskList& operator=(AsyncTaskList const&) = delete;

    ~AsyncTaskList() override = default;

    /**
     * Add one more task to the list.
     */
    void addTask(std::shared_ptr<AsyncTask> const& task);

    /**
     * Remove finished finalizers, can be called periodically.
     */
    void cleanup();

    /**
     * Return task name.
     */
    std::string name() const override;

    /**
     * Check current status of the task.
     *
     * Returns true if task is still running
     */
    bool isRunning() override;

    /**
     * Wait for task to complete.
     *
     * Returns task completion status.
     */
    TaskStatus wait() override;

    /**
     * Wait for task to complete up to max. `timeout` interval.
     *
     * Returns either task completion status (Success/Failure) or Timeout if
     * task is still when timeout is reached.
     */
    WaitStatus waitFor(std::chrono::milliseconds timeout) override;

    /**
     * Wait until `timePoint` for task to complete.
     *
     * Returns either task completion status (Success/Failure) or Timeout if
     * task is still when timeout is reached.
     */
    WaitStatus waitUntil(std::chrono::steady_clock::time_point timePoint) override;

protected:

private:

    const std::string m_name;
    std::list<std::shared_ptr<AsyncTask>> m_tasks;

};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_ASYNCTASKLIST_H
