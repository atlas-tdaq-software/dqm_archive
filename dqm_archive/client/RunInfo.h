#ifndef DQM_ARCHIVE_RUNINFO_H
#define DQM_ARCHIVE_RUNINFO_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 *  @ingroup dqm_archive
 *
 *  Class providing information about single run.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class RunInfo  {
public:

    /**
     * Constructor takes run number and partition name.
     *
     * @param[in] run        Run number
     * @param[in] partition  Partition name
     * @param[in] statusSet  Set of status values for a parameter (for given run number for example).
     */
    RunInfo(unsigned run, const std::string& partition)
        : m_run(run), m_partition(partition) {}

    /// Get run number.
    unsigned run() const { return m_run; }

    /// Get partition name.
    const std::string& partition() const { return m_partition; }

    bool operator==(RunInfo const& other) const {
        return std::tie(m_run, m_partition) == std::tie(other.m_run, other.m_partition);
    }
    bool operator!=(RunInfo const& other) const {
        return !operator==(other);
    }

protected:

private:

    unsigned m_run;
    std::string m_partition;
    
};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_RUNINFO_H
