#ifndef DQM_ARCHIVE_DQMARCHIVECLIENT_H
#define DQM_ARCHIVE_DQMARCHIVECLIENT_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>
#include <string>
#include <vector>
#include <map>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dqm_archive/client/ParamInfo.h"
#include "dqm_archive/client/Result.h"
#include "dqm_archive/client/RunInfo.h"
#include "TObjArray.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 *  @ingroup dqm_archive
 *
 *  @brief Definition of the client interface for DQM archive.
 *
 *  This abstract class defines an interface to be used by client code
 *  to access data from DQM archive.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class DqmArchiveClient  {
public:

    typedef std::vector<Result> ResultList;
  
    /**
     *  @brief Create and return an instance of this class.
     *
     *  Parameter string passed to this method will determine exact type
     *  of the instance. Parameter string should have a format
     *
     *  type[;optional]
     *
     *  where type determines the type of the instance returned and optional
     *  parameters are passed to the instance constructor.
     *
     *  Client code should not make an assumption about
     *  lifetime or identity of the returned instance (meaning that new
     *  instance may be returned on every call).
     *
     *  @return Zero pointer will be returned if parameter string has unexpected format.
     *  @throw Exception is thrown in case of severe errors.
     */
    static std::shared_ptr<DqmArchiveClient> instance(const std::string& param = "rootcoca");
    
    // Destructor
    virtual ~DqmArchiveClient () ;

    /**
     *  @brief Return total number of known runs.
     *
     *  @return Total count of runs in database
     *
     *  @throw Exception
     */
    virtual unsigned runCount() const = 0;

    /**
     *  @brief Return the list of known runs.
     *
     *  This method returns the list of runs known to the archive. It returns up to count
     *  of latest runs (optionally skipping few latest ones). Runs are returned in the 
     *  reverse time order which is not necessary the same as a run number order.
     *
     *  @param[out] runs   List of runs returned
     *  @param[in] count   Max. number of runs to return
     *  @param[in] skip    Number of last runs to skip
     *
     *  @throw Exception
     */
    virtual void runs(std::vector<RunInfo>& runs, unsigned count, unsigned skip = 0) const = 0;

    /**
     *  @brief Return the list of all parameter names for a given run.
     *  
     *  Returned list is likely to be large as it includes name of every defined parameter. 
     *  
     *  @param[out] names   List of parameter names
     *  @param[in] run      Run number
     */
    virtual void parameters(std::vector<std::string>& names, unsigned run) const = 0;
    
    /**
     *  @brief Return the list of parameters for a given run and a parent region name.
     *  
     *  If parent region name is empty then the list of top-level regions is returned. For every 
     *  child parameter of a given parent region returned list contains a pair whose first item is 
     *  child parameter name and second item is a boolean which is set to true if parameter is a 
     *  leaf (has no other children).
     *  
     *  @param[out] children List of child parameters.
     *  @param[in] run      Run number
     *  @param[in] parent   Parent region name
     */
    virtual void childParameters(std::vector<ParamInfo>& children, unsigned run, const std::string& parent) const = 0;
    
    /**
     *  @brief Return the list of all DQM results for a given run and parameter name.
     *  
     *  @param[out] results List of results
     *  @param[in] run      Run number
     *  @param[in] param    DQM Parameter name
     */
    virtual void results(ResultList& results, unsigned run, const std::string& param) const = 0;
    
    /**
     *  @brief Return the all DQM results for a given run and parameter name plus children parameters.
     *  
     *  This method returns a map from parameter names to result lists. Parameter names should include
     *  parameter specified as an argument and all its direct children (if there are any).
     *  Results of the children of specified parameter will not have their corresponding tags set, but
     *  results for the parameter itself will have all tags.
     *  
     *  @param[out] results Mapping of the parameter name to result list
     *  @param[in] run      Run number
     *  @param[in] param    DQM Parameter name
     */
    virtual void childResults(std::map<std::string, ResultList>& results, unsigned run, const std::string& param) const = 0;
    
    /**
     *  @brief Returns histograms associated with the result.
     *
     *  Returns the list (possibly empty) of TH1 type instances or its sub-classes.
     *  On input one has to specify run number, parameter name, and timestamp of the result.
     *  Timestamp should be the value obtained from Result object returned from one of the
     *  above methods.
     *
     *  The histograms produced by this method will be owned by histos array (the method calls
     *  histos.SetOwner(kTRUE) on array object).
     *
     *  @param[out] histos  Returned list of histogram objects
     *  @param[in] run      Run number
     *  @param[in] param    Parameter name
     *  @param[in] time     Time of the result
     */
    virtual void histograms(TObjArray& histos, unsigned run, const std::string& param,
            const std::chrono::system_clock::time_point& time) const = 0;

protected:

    // Default constructor
    DqmArchiveClient () ;

private:

};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_DQMARCHIVECLIENT_H
