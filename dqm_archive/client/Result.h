#ifndef DQM_ARCHIVE_RESULT_H
#define DQM_ARCHIVE_RESULT_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <utility>
#include <vector>
#include <chrono>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dqm_core/Result.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 *  @ingroup dqm_archive
 *
 *  @brief Representation of DQMF result which includes only parts relevant to DQMA clients.
 *  
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class Result {
public:

    typedef std::pair<std::string, double> Tag;
    typedef std::vector<Tag> TagList;

    // Default constructor
    Result() {}

    /// Construct from relevant pieces
    Result(dqm_core::Result::Status status,
            const std::chrono::system_clock::time_point& time,
            const TagList& tags, bool hasHistograms) :
            m_status(status), m_time(time), m_tags(tags), m_histos(
                    hasHistograms)
    {
    }

    /// get status 
    dqm_core::Result::Status status() const
    {
        return m_status;
    }

    /// get time
    std::chrono::system_clock::time_point time() const
    {
        return m_time;
    }

    /// get tags
    const TagList& tags() const
    {
        return m_tags;
    }

    /// Returns true if result has associated histograms (one or more)
    bool hasHistograms() const
    {
        return m_histos;
    }

protected:

private:

    dqm_core::Result::Status m_status;     ///< status value
    std::chrono::system_clock::time_point m_time;  ///< time of the result
    TagList m_tags;                        ///< List of tags
    bool m_histos;                         ///< If true then result has histograms
};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_RESULT_H
