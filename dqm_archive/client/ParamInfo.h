#ifndef DQM_ARCHIVE_PARAMINFO_H
#define DQM_ARCHIVE_PARAMINFO_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dqm_archive/common/StatusSet.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 *  @ingroup dqm_archive
 *
 *  Class providing information about single DQM parameter.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class ParamInfo  {
public:

    /**
     * Constructor that takes parameter name and additional info.
     *
     * @param[in] name       Parameter name
     * @param[in] leaf       If true then parameter is a leaf parameter
     * @param[in] statusSet  Set of status values for a parameter (for given run number for example).
     */
    ParamInfo(const std::string& name, bool leaf, StatusSet statusSet = StatusSet())
        : m_name(name), m_leaf(leaf), m_sset(statusSet) {}

    /// Get parameter name.
    const std::string& name() const { return m_name; }

    /// Return true if parameter is a leaf
    bool leaf() const { return m_leaf; }

    /// Set of status values for a parameter.
    StatusSet statusSet() const { return m_sset; }
    
protected:

private:

    std::string m_name;
    bool m_leaf;
    StatusSet m_sset;
    
};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_PARAMINFO_H
