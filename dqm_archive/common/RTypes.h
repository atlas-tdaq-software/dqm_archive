#ifndef DQM_ARCHIVE_RTYPES_H
#define DQM_ARCHIVE_RTYPES_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------


//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 *  @ingroup dqm_archive
 *
 *  Set of types used to store/retrieve data from ROOT files.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

namespace RTypes  {

typedef std::uint32_t ParmId;
typedef std::uint32_t ResultId;
typedef std::uint32_t TagId;
typedef std::int32_t  Status;
typedef std::int64_t  Timestamp;

struct ParamTreeRepr {
    std::int32_t parent_id;
    ParmId       parm_id;
    static const char* leafspec() { return "parent_id/I:parm_id/i"; }

    bool operator<(const ParamTreeRepr& rhs) const {
        if (parent_id < rhs.parent_id) return true;
        if (parent_id > rhs.parent_id) return false;
        return parm_id < rhs.parm_id;
    }
};

struct ResultRepr {
    Timestamp time_usec;
    ParmId    parm_id;
    Status    status;
    std::int32_t tag_index;
    std::int32_t histo_index;
    static const char* leafspec() { return "time_usec/L:parm_id/i:status/I:tag_index/I:histo_index/I"; }
};

struct TagValueRepr {
    ResultId result_id;
    TagId    tag_name_id;
    double   value;
    static const char* leafspec() { return "result_id/i:tag_name_id/i:value/D"; }
};

struct StatusCounters {
    ParmId        parm_id;
    Status        status;
    std::uint32_t counter;
    static const char* leafspec() { return "parm_id/i:status/I:counter/i"; }

    bool operator<(const StatusCounters& rhs) const {
        if (parm_id < rhs.parm_id) return true;
        if (parm_id > rhs.parm_id) return false;
        return status < rhs.status;
    }
};

struct ResultIndex {
    Timestamp time_usec;
    ParmId    parm_id;
    Status    status;
    std::int32_t tag_index;
    std::int32_t histo_index;
    ResultId  result_id;
    static const char* leafspec() { return "time_usec/L:parm_id/i:status/I:tag_index/I:histo_index/I:result_id/i"; }

    bool operator<(const ResultIndex& rhs) const {
        if (parm_id < rhs.parm_id) return true;
        if (parm_id > rhs.parm_id) return false;
        return time_usec < rhs.time_usec;
    }
};

// two indices are defined for histograms - (parm_id, time_usec) and (result_id)
struct HistoIdRepr {
    Timestamp time_usec;
    ParmId    parm_id;
    ResultId  result_id;
    static const char* leafspec() { return "time_usec/L:parm_id/i:result_id/i"; }
};

} // namespace RTypes

} // namespace dqm_archive

#endif // DQM_ARCHIVE_RTYPES_H
