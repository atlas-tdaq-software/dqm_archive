#ifndef DQM_ARCHIVE_ROOTSERIALIZER_H
#define DQM_ARCHIVE_ROOTSERIALIZER_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "TString.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 *  @ingroup dqm_archive
 *
 *  @brief Serializer/deserializer for ROOT objects.
 *  
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class RootSerializer {
public:

    /**
     *  Constructor takes default version as an argument. If version
     *  is negative then serializer will use latest know version.
     */
    RootSerializer(int version = -1);

    /**
     *  Serialize the object and return buffer as TString.
     */
    TString serialize(TObject* tobj) const;

    /**
     *  Deserialize the object from buffer that was created earlier with serialize() method. Can return 0 pointer.
     */
    TObject* deserialize(const TString& data) const;

protected:

private:

    int m_version;  // version number to use for serializing

};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_ROOTSERIALIZER_H
