//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	TTreeSequence class which emulates STL collection
//	based on the TTree object
//
// Environment:
//      This software was developed for the BaBar collaboration.  If you
//      use all or part of it, please give an appropriate acknowledgement.
//
// Author List:
//      Andy Salnikov
//
//------------------------------------------------------------------------

#ifndef DQM_ARCHIVE_TTREESEQUENCE_H
#define DQM_ARCHIVE_TTREESEQUENCE_H

//-----------------
// C/C++ Headers --
//-----------------
#include <algorithm>
#include <cstddef>
#include <iterator>
#include <memory>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "TTree.h"
#include "TBranch.h"
#include "TBranchElement.h"
#include "TBranchObject.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

namespace detail {

/**
 *  @ingroup dqm_archive
 *
 *  @brief Base class for templated iterator, defines few common type-independent operations
 */
struct TTreeSequenceIteratorBase {

    TTreeSequenceIteratorBase() : m_branch(0), m_idx(0) {}
    TTreeSequenceIteratorBase(TBranch* branch, ULong_t idx) : m_branch{branch}, m_idx{idx} {}

    // compare two iterators
    bool operator==(const TTreeSequenceIteratorBase& o) const { return m_branch == o.m_branch && m_idx == o.m_idx; }
    bool operator!=(const TTreeSequenceIteratorBase& o) const { return not operator==(o); }
    bool operator<(const TTreeSequenceIteratorBase& o) const { return m_idx < o.m_idx; }
    bool operator>(const TTreeSequenceIteratorBase& o) const { return m_idx > o.m_idx; }
    bool operator<=(const TTreeSequenceIteratorBase& o) const { return m_idx <= o.m_idx; }
    bool operator>=(const TTreeSequenceIteratorBase& o) const { return m_idx >= o.m_idx; }

    TBranch* m_branch;
    ULong_t m_idx;
};

// non-specialized implementation which works for non-object type branches
template <typename Type>
struct TTreeBranchAddress {
    static Type* address(TBranch* branch) { return static_cast<Type*>((void*)branch->GetAddress()); }
    static void destroy(Type *) {}
    static void fillBranch(TBranch* branch, Type& store, const Type& newData) {
        store = newData;
        branch->Fill();
    }
};

// specialized implementation which works for object type branches
template <typename Type>
struct TTreeBranchAddress<Type*> {
    static Type** address(TBranch* branch) { return (Type**)((void**)branch->GetAddress()); }
    static void destroy(Type **ptr) { delete *ptr; }
    static void fillBranch(TBranch* branch, Type*& store, Type* newData) {
        Type* oldptr = store;
        store = newData;
        branch->Fill();
        store = oldptr;
    }
};

/**
 *  @ingroup dqm_archive
 *
 *  @brief Iterator class.
 */
template <typename Type>
struct TTreeSequenceIterator: public TTreeSequenceIteratorBase {

    using difference_type = ptrdiff_t;
    using value_type = Type;
    using pointer = const Type*;
    using reference = const Type&;
    using iterator_category = std::random_access_iterator_tag;

    TTreeSequenceIterator() : TTreeSequenceIteratorBase() { }
    TTreeSequenceIterator(TBranch* branch, ULong_t idx) : TTreeSequenceIteratorBase{branch, idx} {}
    TTreeSequenceIterator(const TTreeSequenceIterator& o) : TTreeSequenceIteratorBase{o} {}

    // moving around
    TTreeSequenceIterator& operator++()
    {
        ++ m_idx;
        return *this;
    }
    TTreeSequenceIterator operator++(int)
    {
        TTreeSequenceIterator tmp(*this);
        ++ m_idx;
        return tmp;
    }
    TTreeSequenceIterator& operator--()
    {
        -- m_idx;
        return *this;
    }
    TTreeSequenceIterator operator--(int)
    {
        TTreeSequenceIterator tmp(*this);
        -- m_idx;
        return tmp;
    }
    TTreeSequenceIterator& operator+=(int n)
    {
        m_idx += n;
        return *this;
    }
    TTreeSequenceIterator& operator-=(int n)
    {
        m_idx -= n;
        return *this;
    }

    // distance
    typename TTreeSequenceIterator::difference_type operator-(const TTreeSequenceIteratorBase& o) const
    {
        return m_idx - o.m_idx;
    }

    // bracket op
    typename TTreeSequenceIterator::reference operator[](size_t n) const { return *(*this + n); }

    // dereferencing
    typename TTreeSequenceIterator::reference operator*() const { return *ptr(); }
    typename TTreeSequenceIterator::pointer operator->() const { return ptr(); }

private:

    typename TTreeSequenceIterator::pointer ptr() const
    {
        m_branch->GetEntry(m_idx);
        return TTreeBranchAddress<Type>::address(m_branch);
    }
};

template <typename Type>
TTreeSequenceIterator<Type> operator+(const TTreeSequenceIterator<Type>& iter, int n)
{
    return TTreeSequenceIterator<Type>(iter) += n;
}
template <typename Type>
TTreeSequenceIterator<Type> operator+(int n, const TTreeSequenceIterator<Type>& iter)
{
    return TTreeSequenceIterator<Type>(iter) += n;
}
template <typename Type>
TTreeSequenceIterator<Type> operator-(const TTreeSequenceIterator<Type>& iter, int n)
{
    return TTreeSequenceIterator<Type>(iter) -= n;
}

} // namespace detail


/**
 *  @ingroup dqm_archive
 *
 *  @brief STL-like container for data in TTree branches.
 *
 *  The classes below define the collection class which stores its data
 *  inside TTree object. Collection class almost satisfies the requirements
 *  for the C++ sequence types. Some hard-to implement or inefficient
 *  methods were intentionally left out. Sequence supports random insertion
 *  (which is the main reason why this class was created) but it is not very
 *  efficient and its done by cloning the complete tree.
 *
 *  This software was originally developed for the BaBar collaboration and
 *  adopted for ATLAS use.  If you use all or part of it, please give an
 *  appropriate acknowledgement.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

template<typename T>
class TTreeSequence {

public:

    // standard STL typedefs
    typedef T value_type;
    typedef const T& reference;
    typedef const T& const_reference;
    typedef detail::TTreeSequenceIterator<T> iterator;
    typedef iterator const_iterator;
    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef reverse_iterator const_reverse_iterator;
    typedef typename iterator::difference_type difference_type;
    typedef size_t size_type;

    /**
     *  @brief Constructor from an existing branch.
     *
     *  Takes a tree and pointer to the branch object.
     */
    explicit TTreeSequence(TBranch* branch = 0)
            : m_branch(branch), m_object()
    {
        if (m_branch) m_branch->SetAddress(&m_object);
    }

    /**
     *  @brief Constructor from an existing branch.
     *
     *  This constructor takes a tree and uses the first branch to store and read the data,
     *  it's good to use with the single-branch trees.
     */
    explicit TTreeSequence(TTree* ttree)
            : m_branch(0), m_object()
    {
        if (ttree) {
            TObjArray* branches = ttree->GetListOfBranches();
            if (branches and branches->GetSize()) {
                // take first branch
                m_branch = dynamic_cast<TBranch*>((*branches)[0]);
                if (m_branch) m_branch->SetAddress(&m_object);
            }
        }
    }

    /**
     *  @brief Constructor from an existing branch.
     *
     *  This constructor takes a tree and uses the named branch to store and read the data.
     */
    TTreeSequence(TTree* ttree, const std::string& branchName)
            : m_branch(0), m_object()
    {
        if (ttree) {
            m_branch = ttree->GetBranch(branchName.c_str());
            if (m_branch) m_branch->SetAddress(&m_object);
        }
    }

    /**
     *  Destructor may need to destroy data depending on whether it is object or non-object branch
     */
    ~TTreeSequence() { detail::TTreeBranchAddress<T>::destroy(&m_object); }


    TTreeSequence(const TTreeSequence& other)
            : m_branch(other.m_branch), m_object()
    {
        if (m_branch) m_branch->SetAddress(&m_object);
    }

    TTreeSequence& operator=(const TTreeSequence& other)
    {
        if (this != &other) {
            m_branch = other.m_branch;
            m_object = T();
            if (m_branch) m_branch->SetAddress(&m_object);
        }
        return *this;
    }

    // iterators
    iterator begin() const { return iterator(m_branch, 0); }
    iterator end() const  { return iterator(m_branch, m_branch ? m_branch->GetEntries() : 0); }
    const_iterator cend() const { return const_iterator(m_branch, m_branch ? m_branch->GetEntries() : 0); }
    const_iterator cbegin() const { return const_iterator(m_branch, 0); }
    reverse_iterator rbegin() const { return reverse_iterator(end()); }
    reverse_iterator rend() const { return reverse_iterator(begin()); }
    const_reverse_iterator crend() const { return const_reverse_iterator(begin()); }
    const_reverse_iterator crbegin() const { return const_reverse_iterator(end()); }

    // compare
    bool operator==(const TTreeSequence& o) const { return m_branch && m_branch == o.m_branch; }
    bool operator!=(const TTreeSequence& o) const { return not operator==(o); }

    // swap
    void swap(TTreeSequence& o);

    // size
    size_type size() const { return m_branch ? m_branch->GetEntries() : 0; }
    bool empty() const { return not m_branch || m_branch->GetEntries() == 0; }
    size_type max_size() const { return size_type(-1); }

    // insert
    iterator insert(iterator p, const_reference obj)
    {
        this->insert(p, &obj, &obj+1);
        return iterator(m_branch, p.m_idx);
    }
    // Beware, this is inefficient and makes memory copy of the whole branch if p != end()
    template<typename Iterator> void insert(iterator p, Iterator q, Iterator r)
    {
      if (not m_branch) return;
      if (q == r) return ;

      size_type oldSize = m_branch->GetEntries();
      if (p.m_idx == oldSize) {

        // insert at the end
        for ( ; q != r ; ++ q) {
          detail::TTreeBranchAddress<T>::fillBranch(m_branch, m_object, *q);
        }

      } else {

        // insert in the middle or before begin

        // first make an in-memory copy of the stuff
        size_type oldSize = m_branch->GetEntries() ;
        std::vector<T> memCopy ;
        memCopy.reserve ( oldSize ) ;
        for ( size_type i = 0 ; i < oldSize ; ++ i ) {
          memCopy.push_back(at(i));
        }

        // clear the current content
        m_branch->Reset() ;

        // copy stuff
        for ( size_type i = 0 ; i != p.m_idx ; ++ i ) {
          detail::TTreeBranchAddress<T>::fillBranch(m_branch, m_object, memCopy[i]);
        }
        for ( ; q != r ; ++ q ) {
          detail::TTreeBranchAddress<T>::fillBranch(m_branch, m_object, *q);
        }
        for ( size_type i = p.m_idx ; i != oldSize ; ++ i ) {
          detail::TTreeBranchAddress<T>::fillBranch(m_branch, m_object, memCopy[i]);
        }

      }

    }
    void push_front(const_reference obj) { insert(begin(), obj); }
    void push_back(const_reference obj) { insert(end(), obj); }

    // erase
    iterator erase(iterator q)
    {
      if (empty()) return q ;
      if (q.m_idx == m_branch->GetEntries()) return q;
      return erase(q, q+1);
    }
    // Do not use - this is inefficient and makes memory copy of the whole branch
    iterator erase(iterator q1, iterator q2)
    {
      if (empty()) return q1;
      if (q1 >= q2) return q2;

      size_type oldSize = m_branch->GetEntries();

      // first make an in-memory copy of the stuff
      std::vector<T> memCopy ;
      memCopy.reserve(oldSize - q2.m_idx + q1.m_idx);
      for (size_type i = 0 ; i != q1.m_idx ; ++ i ) {
          memCopy.push_back(at(i));
      }
      for (size_type i = q2.m_idx ; i != oldSize ; ++ i) {
          memCopy.push_back(at(i));
      }

      // clear the current content
      m_branch->Reset() ;

      // copy stuff
      for (const T& elem: memCopy) {
        detail::TTreeBranchAddress<T>::fillBranch(m_branch, m_object, elem);
      }

      // get the iterator back
      return iterator(m_branch, q1.m_idx);
    }

    void clear()
    {
        if (empty()) return;
        m_branch->Reset();
    }

    // element access
    const_reference front() const { return at(0); }
    const_reference back() const { return at(size()-1); }
    const_reference operator[](ULong_t n) const { return at(n); }
    const_reference at(ULong_t n) const
    {
        m_branch->GetEntry(n);
        return m_object;
    }

    // get access to the underlying branch object
    TBranch* branch() const { return m_branch; }

private:

    TBranch* m_branch;        ///< ROOT branch object which keeps the data
    T        m_object;        ///< The object which stores the data, if T is a pointer type then this is a pointer which points to data

};

template<typename T>
class TTreeSequenceFactory {
public:
    /**
     *  @brief Factory method for creating new branches.
     *
     *  This constructor takes a tree and creates named branch of "struct" type with the
     *  leaves defined in the leafList.
     */
    static TTreeSequence<T> create(TTree* ttree, const std::string& branchName, const std::string& leafList, int bufsize = 32000)
    {
        return TTreeSequence<T>(ttree->Branch(branchName.c_str(), (void*)0, leafList.c_str(), bufsize));
    }
};

template<typename T>
class TTreeSequenceFactory<T*> {
public:
    /**
     *  @brief Factory method for creating new branches.
     *
     *  This constructor takes a tree and creates named branch of "object" type.
     */
    static TTreeSequence<T*> create(TTree* ttree, const std::string& branchName, int bufsize = 32000, int splitlevel=99)
    {
        return TTreeSequence<T*>(ttree->Branch(branchName.c_str(), (T**)0, bufsize, splitlevel));
    }
};


} // namespace dqm_archive

#endif  // DQM_ARCHIVE_TTREESEQUENCE_H
