#ifndef DQM_ARCHIVE_STATUSSET_H
#define DQM_ARCHIVE_STATUSSET_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dqm_core/Result.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 *  @ingroup dqm_archive
 *
 *  @brief Class defining a set of dqm_core::Result::Status values.
 *
 *  This class defines an efficient representation of the set of possible
 *  status values, such as set of statuses for particular parameter during
 *  the run.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class StatusSet  {
public:

    /**
     *  Default constructor makes an empty set.
     */
    StatusSet() : m_bitset(0) {}

    /**
     *  Add status value to the set.
     */
    void insert(dqm_core::Result::Status status) {
        switch(status) {
        case dqm_core::Result::Disabled:
            m_bitset |= DisabledMask;
            break;
        case dqm_core::Result::Undefined:
            m_bitset |= UndefinedMask;
            break;
        case dqm_core::Result::Red:
            m_bitset |= RedMask;
            break;
        case dqm_core::Result::Yellow:
            m_bitset |= YellowMask;
            break;
        case dqm_core::Result::Green:
            m_bitset |= GreenMask;
            break;
        }
    }

    /**
     *  Check if specified status is in the set.
     */
    bool contains(dqm_core::Result::Status status) {
        switch(status) {
        case dqm_core::Result::Disabled:
            return bool(m_bitset & DisabledMask);
        case dqm_core::Result::Undefined:
            return bool(m_bitset & UndefinedMask);
        case dqm_core::Result::Red:
            return bool(m_bitset & RedMask);
        case dqm_core::Result::Yellow:
            return bool(m_bitset & YellowMask);
        case dqm_core::Result::Green:
            return bool(m_bitset & GreenMask);
        }
        return false;
    }
    
protected:

private:

    enum Masks {
        DisabledMask = 0x1,
        UndefinedMask = 0x2,
        RedMask = 0x4,
        YellowMask = 0x8,
        GreenMask = 0x10
    };

    char m_bitset;
    
};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_STATUSSET_H
