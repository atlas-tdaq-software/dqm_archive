#ifndef DQM_ARCHIVE_COCAFILENAMECACHE_H
#define DQM_ARCHIVE_COCAFILENAMECACHE_H

//-----------------
// C/C++ Headers --
//-----------------
#include <map>
#include <string>
#include <vector>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
#include "coca/client/IDBClient.h"
#include "dqm_archive/client/RunInfo.h"

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 * @ingroup dqm_archive
 *
 * @brief Caching layer for CoCa file name information.
 *
 * New implementation of dqm_archive produces multiple files per run, this
 * complicates few operations which now need to know full list of file names
 * in coca. This class helps to avoid retrieveing full list multipl;e times
 * by keeping in-memory cache which is regularly updated from CoCa.
 *
 * The class is not thread-safe.
 */
class CocaFileNameCache {
public:

    /**
     * Constructor takes a reference to coca client instance, this reference
     * is kept for the lifetime of the instance.
     */
    explicit CocaFileNameCache(daq::coca::IDBClient& coca,
                               std::string const& dataset="DQM-Archive") :
        m_coca(coca),
        m_dataset(dataset)
    {}

    // Return total number of known runs.
    unsigned runCount() const;

    /** Return the list of known runs.
     * Runs are returned in the same way as they are returned from coca - in the
     * reverse order of registration (newest files returned first).
     */
    void runs(std::vector<RunInfo>& runs, unsigned count, unsigned skip=0) const;

    /** Return the list of files for a run.
     *
     * Returned array is ordered by file name. Empty list is returned if run
     * is not known.
     */
    std::vector<std::string> files(int run) const;

protected:

    // update local cache from coca
    void _updateCache() const;

    // Insert file into cache, return false if already there. Returns true if
    // file name is in unexpected format.
    bool _insert(std::string const& relPath) const;

private:

    struct RunAndFiles {

        RunAndFiles(int run_) : run(run_) {}

        // add a file, returns false if file is already there
        bool addFile(std::string const& relPath);

        int run;
        std::vector<std::string> files;  // names of files, ordered by name
    };

    // List of file names per run, ordered by registration time.
    // Note that we do not really know regitration time, that ordering
    // is inferred from the order in which CoCa returns its data.
    using RunData = std::vector<RunAndFiles>;

    struct RunCache {

        // Add a file, returns false if file is already there.
        // This method must be called for each file in the order of the file
        // registartion time (the time itself does not matter but the order
        // must be preserved).
        bool addFile(std::string const& relPath);

        void clear() {
            cache.clear();
            runIndex.clear();
        }

        RunData cache;                              // Cached runs/files ordered by time
        std::vector<std::pair<int, int>> runIndex;  // Maps run number into index in cache
    };

    daq::coca::IDBClient& m_coca;           // coca client instance
    std::string const m_dataset;
    mutable RunCache m_cache;
};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_COCAFILENAMECACHE_H
