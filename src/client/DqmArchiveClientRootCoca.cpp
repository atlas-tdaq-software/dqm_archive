//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "DqmArchiveClientRootCoca.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <sstream>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/client/DBClient.h"
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace dqm_archive {

//----------------
// Constructors --
//----------------
DqmArchiveClientRootCoca::DqmArchiveClientRootCoca(const std::string& options)
    : DqmArchiveClientRoot()
    , m_connStr()
    , m_dataset("DQM-Archive")
    , m_coca()
{
    // get all options out of string
    std::vector<std::string> optlist;
    boost::split(optlist, options, boost::is_any_of(";"), boost::token_compress_on);
    for (const auto& option: optlist) {
        std::string::size_type p = option.find('=');
        if (p != std::string::npos) {
            // check known options, ignore anything else
            if (option.compare(0, p, "dataset") == 0) {
                m_dataset = option.substr(p+1);
            } else if (option.compare(0, p, "connStr") == 0) {
                m_connStr = option.substr(p+1);
            }
        }
    }

    // can potentially delay but OK to do in constructor
    if (m_connStr.empty()) {
        m_coca.reset(new daq::coca::DBClient());
    } else {
        m_coca.reset(new daq::coca::DBClient(m_connStr));
    }
    m_cocaCache.reset(new CocaFileNameCache(*m_coca, m_dataset));
}

DqmArchiveClientRootCoca::DqmArchiveClientRootCoca(std::unique_ptr<daq::coca::IDBClient> coca,
                                                   const std::string& options)
    : DqmArchiveClientRoot()
    , m_connStr()
    , m_dataset("DQM-Archive")
    , m_coca(std::move(coca))
{
    // get all options out of string
    std::vector<std::string> optlist;
    boost::split(optlist, options, boost::is_any_of(";"), boost::token_compress_on);
    for (const auto& option: optlist) {
        std::string::size_type p = option.find('=');
        if (p != std::string::npos) {
            // check known options, ignore anything else
            if (option.compare(0, p, "dataset") == 0) {
                m_dataset = option.substr(p+1);
            }
        }
    }
    m_cocaCache.reset(new CocaFileNameCache(*m_coca, m_dataset));
}

//--------------
// Destructor --
//--------------
DqmArchiveClientRootCoca::~DqmArchiveClientRootCoca()
{
}

// Return total number of known runs.
unsigned 
DqmArchiveClientRootCoca::runCount() const 
{
    unsigned total = m_cocaCache->runCount();
    return total;
}

// Return the list of known run numbers.
void
DqmArchiveClientRootCoca::runs(std::vector<RunInfo>& runs, unsigned count, unsigned skip) const
{
    m_cocaCache->runs(runs, count, skip);
}

// Return single ROOT file for given run number.
std::shared_ptr<TFile>
DqmArchiveClientRootCoca::file(unsigned run) const
{
    auto flist = files(run);
    if (not flist.empty()) {
        return flist.front();
    }
    return std::shared_ptr<TFile>();
}

// Return all ROOT files for given run number.
std::vector<std::shared_ptr<TFile>>
DqmArchiveClientRootCoca::files(unsigned run) const
{
    std::vector<std::shared_ptr<TFile>> files;

    // check in cache first
    auto range = m_fileCache.equal_range(run);
    if (range.first != range.second) {
        ERS_DEBUG(1, "DqmArchiveClientRootCoca::file: Found run " << run << " in cache");
        for (auto iter = range.first; iter != range.second; ++ iter) {
            // mark them as used
            iter->second.last_used = time(nullptr);
            files.push_back(iter->second.file);
        }
        // cleanup expired cache entries
        purgeCache();
        return files;
    }

    // cleanup expired cache entries
    purgeCache();

    // get ordered list of file names for this run, ordered by name also
    // means ordered by time by naming convention.
    auto fileNames = m_cocaCache->files(run);
    if (fileNames.empty()) {
        ERS_DEBUG(1, "DqmArchiveClientRootCoca::file: no matching files found for run " << run);
    }

    for (auto&& relPath: fileNames) {

        ERS_DEBUG(1, "DqmArchiveClientRootCoca::file: looking for file " << relPath);

        // get the list of file locations, limit to archived data only
        std::vector<std::string> locations = m_coca->fileLocations(relPath, m_dataset, daq::coca::DBClient::LocArchive);

        if (not locations.empty()) {
            // open file and save to cache
            ERS_DEBUG(1, "DqmArchiveClientRootCoca::file: Opening URL " << locations.front());
            std::shared_ptr<TFile> file(TFile::Open(locations.front().c_str(), "READ"));
            FileCacheEntry entry{file, time(nullptr)};
            m_fileCache.insert(std::make_pair(run, entry));
            files.push_back(file);
        }
    }

    return files;
}

/**
 * Remove expired entries from cache
 */
void
DqmArchiveClientRootCoca::purgeCache() const
{
    const time_t now = time(nullptr);
    const int expire = 10*60;  // 10 minutes
    for (auto iter = m_fileCache.begin(); iter != m_fileCache.end(); ) {
        if (now > iter->second.last_used + expire) {
            iter = m_fileCache.erase(iter);
        } else {
            ++ iter;
        }
    }
}


} // namespace dqm_archive
