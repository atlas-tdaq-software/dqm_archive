#ifndef DQM_ARCHIVE_DQMARCHIVECLIENTTEST_H
#define DQM_ARCHIVE_DQMARCHIVECLIENTTEST_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include "dqm_archive/client/DqmArchiveClient.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 *  @ingroup dqm_archive
 *
 *  @brief Implementation of DqmArchiveClient for test purposes.
 *
 *  This is the implementation which returns some fake data, only useful for testing.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class DqmArchiveClientTest : public DqmArchiveClient {
public:

    // constructor
    DqmArchiveClientTest(const std::string& options);
    
    // Destructor
    ~DqmArchiveClientTest () override;

    /**
     *  @brief Return total number of known runs.
     *
     *  @return Total count of runs in database
     *
     *  @throw Exception
     */
    unsigned runCount() const override;

    /**
     *  @brief Return the list of known runs.
     *
     *  This method returns the list of runs known to the archive. It returns up to count
     *  of latest run numbers (optionally skipping few latest ones). Runs are returned in the
     *  reverse time order which is not necessary the same as a run number order.
     *
     *  @param[out] runs   List of runs returned
     *  @param[in] count   Max. number of runs to return
     *  @param[in] skip    Number of last runs to skip
     *
     *  @throw Exception
     */
    void runs(std::vector<RunInfo>& runs, unsigned count, unsigned skip = 0) const override;

    /**
     *  @brief Return the list of all parameter names for a given run.
     *  
     *  Returned list is likely to be large as it includes name of every defined parameter. 
     *  
     *  @param[out] names   List of parameter names
     *  @param[in] run      Run number
     */
    void parameters(std::vector<std::string>& names, unsigned run) const override;
    
    /**
     *  @brief Return the list of parameters for a given run and a parent region name.
     *  
     *  If parent region name is empty then the list of top-level regions is returned. For every 
     *  child parameter of a given parent region returned list contains a pair whose first item is 
     *  child parameter name and second item is a boolean which is set to true if parameter is a 
     *  leaf (has no other children).
     *  
     *  @param[out] children List of child parameters.
     *  @param[in] run      Run number
     *  @param[in] parent   Parent region name
     */
    void childParameters(std::vector<ParamInfo>& children, unsigned run, const std::string& parent) const override;
    
    /**
     *  @brief Return the list of all DQM results for a given run and parameter name.
     *  
     *  @param[out] results List of results
     *  @param[in] run      Run number
     *  @param[in] param    DQM Parameter name
     */
    void results(ResultList& results, unsigned run, const std::string& param) const override;
    
    /**
     *  @brief Return the all DQM results for a given run and parameter name plus children parameters.
     *  
     *  This method returns a map from parameter names to result lists. Parameter names should include
     *  parameter specified as an argument and all its direct children (if there are any).
     *  Results of the children of specified parameter will not have their corresponding tags set, but
     *  results for the parameter itself will have all tags.
     *  
     *  @param[out] results Mapping of the parameter name to result list
     *  @param[in] run      Run number
     *  @param[in] param    DQM Parameter name
     */
    void childResults(std::map<std::string, ResultList>& results, unsigned run, const std::string& param) const override;

    /**
     *  @brief Returns histograms associated with the result.
     *
     *  Returns the list (possibly empty) of TH1 type instances or its sub-classes.
     *  On input one has to specify run number, parameter name, and timestamp of the result.
     *  Timestamp should be the value obtained from Result object returned from one of the
     *  above methods.
     *
     *  The histograms produced by this method will be owned by histos array (the method calls
     *  histos.SetOwner(kTRUE) on array object).
     *
     *  @param[out] histos  Returned list of histogram objects
     *  @param[in] run      Run number
     *  @param[in] param    Parameter name
     *  @param[in] time     Time of the result
     */
    void histograms(TObjArray& histos, unsigned run, const std::string& param,
            const std::chrono::system_clock::time_point& time) const override;

protected:

private:

    const std::string m_options;

};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_DQMARCHIVECLIENTTEST_H
