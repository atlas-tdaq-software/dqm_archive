#ifndef DQM_ARCHIVE_DQMARCHIVECLIENTROOTCOCA_H
#define DQM_ARCHIVE_DQMARCHIVECLIENTROOTCOCA_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <ctime>
#include <map>
#include <memory>
#include <vector>

//----------------------
// Base Class Headers --
//----------------------
#include "DqmArchiveClientRoot.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
#include "CocaFileNameCache.h"
#include "coca/client/IDBClient.h"

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 *  @ingroup dqm_archive
 *
 *  @brief Test implementation of DqmArchiveClient interface for ROOT archives.
 *
 *  This implementation provides some fake data for run numbers and points
 *  to simulated data in ROOT archives. Useful for testing only.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class DqmArchiveClientRootCoca: public DqmArchiveClientRoot {
public:

    /**
     *  @brief Constructor takes option string.
     *
     *  Option string provides a bunch of options in the semicolon-separated
     *  format, possible options are:
     *    * connStr=str  -- gives CoCa database connection string
     *    * dataset=name  -- gives CoCa dataset name
     *
     *  If any of these options is missing then defaults are used:
     *    * connStr=<default connection string>
     *    * dataset=DQM-Results
     */
    DqmArchiveClientRootCoca(const std::string& options);

    /**
     *  @brief Constructor from existing CoCa client instance.
     *
     *  This method is useful mostly for testing.
     *
     *  Option string provides a bunch of options in the semicolon-separated
     *  format, possible options are:
     *    * dataset=name  -- gives CoCa dataset name
     *
     *  If any of these options is missing then defaults are used:
     *    * dataset=DQM-Results
     */
    DqmArchiveClientRootCoca(std::unique_ptr<daq::coca::IDBClient> coca,
                             const std::string& options=std::string());

    // Destructor
    ~DqmArchiveClientRootCoca() override;

    /**
     *  @brief Return total number of known runs.
     *
     *  @return Total count of runs in database
     *
     *  @throw Exception
     */
    unsigned runCount() const override;

    /**
     *  @brief Return the list of known runs.
     *
     *  This method returns the list of runs known to the archive. It returns up to count
     *  of latest run numbers (optionally skipping few latest ones). Runs are returned in the 
     *  reverse time order which is not necessary the same as a run number order.
     *
     *  @param[out] runs   List of runs returned
     *  @param[in] count   Max. number of runs to return
     *  @param[in] skip    Number of last runs to skip
     *
     *  @throw Exception
     */
    void runs(std::vector<RunInfo>& runs, unsigned count, unsigned skip = 0) const override;

protected:

    /**
     * @brief Return single ROOT file for given run number.
     *
     * For runs with multiple files it is not specified which one of the files
     * is returned.
     *
     * @return zero pointer if run is not archived.
     */
    std::shared_ptr<TFile> file(unsigned run) const override;

    /**
     * @brief Return all ROOT files for given run number.
     *
     * Returned files must be ordered by taime range, earlier files come first
     * in the returned list.
     *
     * @return empty list if run is not archived.
     */
    std::vector<std::shared_ptr<TFile>> files(unsigned run) const override;

    /**
     * Remove expired entries from cache
     */
    void purgeCache() const;

private:

    struct FileCacheEntry {
        std::shared_ptr<TFile> file;
        time_t last_used;
    };

    std::string m_connStr;             // connection string for coca database
    std::string m_dataset;             // CoCa dataset which stores DQM archives
    std::unique_ptr<daq::coca::IDBClient> m_coca;      // coca client instance
    std::unique_ptr<CocaFileNameCache> m_cocaCache;
    mutable std::multimap<unsigned, FileCacheEntry> m_fileCache;   // cache of the recently used files

};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_DQMARCHIVECLIENTROOTCOCA_H
