
//-----------------------
// This Class's Header --
//-----------------------
#include "CocaFileNameCache.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <regex>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "boost/lexical_cast.hpp"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {



/*
 * Parse name in the format prefix-partition-runNumber.root or
 * prefix-partition-runNumber-numericSuffix.root and return run number
 * and partition name. Returns -1 for run if file name cannot be parsed;
 */
std::pair<int, std::string> parseFileName(std::string const& relPath) {

    static std::regex reWithSuffix("\\w+-(.+)-(\\d+)-\\d+\\.root");
    static std::regex reWithoutSuffix("\\w+-(.+)-(\\d+)\\.root");

    std::smatch match;
    if (std::regex_match(relPath, match, reWithSuffix) ||
        std::regex_match(relPath, match, reWithoutSuffix)) {
        std::string runStr = match[2];
        try {
            return std::make_pair(boost::lexical_cast<int>(runStr), match[1]);
        } catch (boost::bad_lexical_cast const& exc) {
            // ignore it, returns below
        }
    }
    return std::make_pair(-1, std::string());
}

}


//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace dqm_archive {

bool
CocaFileNameCache::RunAndFiles::addFile(std::string const& relPath)
{
    // files array is always ordered, do binary search and insert in order
    auto iter = std::lower_bound(files.begin(), files.end(), relPath);
    if (iter != files.end() and *iter == relPath) {
        // already there
        return false;
    }
    files.insert(iter, relPath);
    return true;
}

bool
CocaFileNameCache::RunCache::addFile(std::string const& relPath)
{
    // get run number from file name
    auto runAndPart = ::parseFileName(relPath);
    auto runNumber = runAndPart.first;
    if (runNumber < 0) {
        return true;
    }

    int pos = cache.size();
    auto iter = std::lower_bound(runIndex.begin(), runIndex.end(), std::make_pair(runNumber, 0));
    if (iter == runIndex.end() or iter->first != runNumber) {
        // not in cache, append to an end
        cache.emplace_back(runNumber);
        runIndex.emplace(iter, runNumber, pos);
    } else {
        pos = iter->second;
    }
    return cache[pos].addFile(relPath);
}

unsigned
CocaFileNameCache::runCount() const
{
    _updateCache();
    return m_cache.cache.size();
}

void
CocaFileNameCache::runs(std::vector<RunInfo>& runs, unsigned count, unsigned skip) const
{
    _updateCache();
    if (m_cache.cache.size() <= skip) {
        return;
    }

    // we need to scan in reverse insertion order
    auto iter = m_cache.cache.rbegin() + skip;
    auto end = m_cache.cache.rend();
    if (m_cache.cache.size() > skip+count) {
        end = iter + count;
    }
    for (; iter != end; ++iter) {
        std::string partition;
        if (not iter->files.empty()) {
            // take partition from the first file, potentially they can be
            // different but that is not we can handle here.
            auto runAndPart = ::parseFileName(iter->files.front());
            partition = runAndPart.second;
        }
        runs.emplace_back(iter->run, partition);
    }
}

std::vector<std::string>
CocaFileNameCache::files(int run) const
{
    _updateCache();

    auto iter = std::lower_bound(m_cache.runIndex.begin(), m_cache.runIndex.end(), std::make_pair(run, 0));
    if (iter == m_cache.runIndex.end() or iter->first != run) {
        // not known
        return std::vector<std::string>();
    }
    auto index = iter->second;
    return m_cache.cache[index].files;
}

void
CocaFileNameCache::_updateCache() const
{
    if (not m_cache.cache.empty()) {
        // cache is already populated means that that we only need to get
        // updates since the last update. Trouble is that there is no API
        // for exactly that sort of query. We could use time-based API using
        // file registration tile but that is not reliable, especially with
        // mock CoCa client. Instead we ask for a reasonably small bunch of
        // recent files, if there is no overlap with cached info then we
        // reset cache and start fresh.
        static int numRecentFiles = 100;
        auto const cocaFiles = m_coca.files(m_dataset, std::string(), std::string(), numRecentFiles);
        bool overlap = false;
        // coca returns files in revers registration order, for addFile() we
        // need to reverse that reversed order.
        for (auto it = std::rbegin(cocaFiles); it != std::rend(cocaFiles); ++it) {
            if (not m_cache.addFile(it->relPath())) {
                // if any file was seen previously means we have fiull history now
                overlap = true;
            }
        }
        if (not overlap) {
            // just reset damn thing and start anew
            m_cache.clear();
        }
     }
    if (m_cache.cache.empty()) {
        // first call or cache has been reset, read everything
        // need to pass large number for 'count' argument to make it result ordered correctly
        auto const cocaFiles = m_coca.files(m_dataset, std::string(), std::string(), 1000000);
        for (auto it = std::rbegin(cocaFiles); it != std::rend(cocaFiles); ++it) {
            m_cache.addFile(it->relPath());
        }
    }
}

} // namespace dqm_archive
