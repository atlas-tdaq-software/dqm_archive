//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "dqm_archive/client/DqmArchiveClient.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "DqmArchiveClientRootCoca.h"
#include "DqmArchiveClientRootTest.h"
#include "DqmArchiveClientTest.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace dqm_archive {

/**
 *  @brief Create and return an instance of this class.
 *
 *  Parameter string passed to this method will determine exact type
 *  of the instance. Client code should not make an assumption about
 *  lifetime or identity of the returned instance (meaning that new
 *  instance may be returned on every call).
 *
 *  @return Zero pointer will be returned if parameter string has unexpected format.
 *  @throw Exception is thrown in case of severe errors.
 */
std::shared_ptr<DqmArchiveClient>
DqmArchiveClient::instance(const std::string& param)
{
    std::string type = param;
    std::string options;
    std::string::size_type p = param.find(';');
    if (p != std::string::npos) {
        type.erase(p);
        options = param.substr(p+1);
    }

    if (type == "test") {
        return std::make_shared<DqmArchiveClientTest>(options);
    } else if (type == "rootcoca") {
        return std::make_shared<DqmArchiveClientRootCoca>(options);
    } else if (type == "roottest") {
        return std::make_shared<DqmArchiveClientRootTest>(options);
    }

    return std::shared_ptr<DqmArchiveClient>();
}

//----------------
// Constructors --
//----------------
DqmArchiveClient::DqmArchiveClient()
{
}

//--------------
// Destructor --
//--------------
DqmArchiveClient::~DqmArchiveClient()
{
}

} // namespace dqm_archive
