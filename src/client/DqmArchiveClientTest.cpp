//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "DqmArchiveClientTest.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

std::map<std::string, std::vector<std::string>> parmTreeInit()
{
    std::map<std::string, std::vector<std::string>> res;

    res[""].push_back("RegionA");
    res[""].push_back("RegionB");
    res[""].push_back("RegionC");

    res["RegionA"].push_back("RegionA1");
    res["RegionA"].push_back("RegionA2");
    res["RegionA"].push_back("RegionA3");

    res["RegionB"].push_back("RegionB1");
    res["RegionB"].push_back("RegionB2");
    res["RegionB"].push_back("RegionB3");

    res["RegionC"].push_back("RegionC1");
    res["RegionC"].push_back("RegionC2");
    res["RegionC"].push_back("RegionC3");

    res["RegionA1"].push_back("ParamA1_1");
    res["RegionA1"].push_back("ParamA1_2");
    res["RegionA1"].push_back("ParamA1_3");
    res["RegionA2"].push_back("ParamA2_1");
    res["RegionA2"].push_back("ParamA2_2");
    res["RegionA2"].push_back("ParamA2_3");
    res["RegionA3"].push_back("ParamA3_1");
    res["RegionA3"].push_back("ParamA3_2");
    res["RegionA3"].push_back("ParamA3_3");

    res["RegionB1"].push_back("ParamB1_1");
    res["RegionB1"].push_back("ParamB1_2");
    res["RegionB1"].push_back("ParamB1_3");
    res["RegionB2"].push_back("ParamB2_1");
    res["RegionB2"].push_back("ParamB2_2");
    res["RegionB2"].push_back("ParamB2_3");
    res["RegionB3"].push_back("ParamB3_1");
    res["RegionB3"].push_back("ParamB3_2");
    res["RegionB3"].push_back("ParamB3_3");

    res["RegionC1"].push_back("ParamC1_1");
    res["RegionC1"].push_back("ParamC1_2");
    res["RegionC1"].push_back("ParamC1_3");
    res["RegionC2"].push_back("ParamC2_1");
    res["RegionC2"].push_back("ParamC2_2");
    res["RegionC2"].push_back("ParamC2_3");
    res["RegionC3"].push_back("ParamC3_1");
    res["RegionC3"].push_back("ParamC3_2");
    res["RegionC3"].push_back("ParamC3_3");

    return res;
}

const std::map<std::string, std::vector<std::string>> g_parmTree = parmTreeInit();

}


//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace dqm_archive {

//----------------
// Constructors --
//----------------
DqmArchiveClientTest::DqmArchiveClientTest(const std::string& options)
    : DqmArchiveClient()
    , m_options(options)
{
}

//--------------
// Destructor --
//--------------
DqmArchiveClientTest::~DqmArchiveClientTest()
{
}

// Return total number of known runs.
unsigned 
DqmArchiveClientTest::runCount() const 
{
    const unsigned lastrun = 23768;

    return lastrun;
}

// Return the list of known run numbers.
void
DqmArchiveClientTest::runs(std::vector<RunInfo>& runs, unsigned count, unsigned skip) const
{
    const unsigned lastrun = runCount();

    runs.clear();
    if (skip >= lastrun) return;
    unsigned startrun = lastrun - skip;
    if (count > startrun) count = startrun;
    unsigned endrun = startrun - count;
    runs.reserve(count);
    for (unsigned run = startrun; run != endrun; -- run) {
        runs.push_back(RunInfo(run, "TEST_PART"));
    }
}

// Return the list of all parameter names for a given run.
void
DqmArchiveClientTest::parameters(std::vector<std::string>& names, unsigned) const
{
    names.clear();
    for (const auto& pair: g_parmTree) {
        names.insert(names.end(), pair.second.begin(), pair.second.end());
    }
}

// Return the list of parameter names for a given run and a parent region name.
void
DqmArchiveClientTest::childParameters(std::vector<ParamInfo>& children, unsigned /*run*/, const std::string& parent) const
{
    children.clear();
    auto iter = g_parmTree.find(parent);
    if (iter != g_parmTree.end()) {
        for (const auto& parm: iter->second) {

            char x = parm[parm.size()-1];

            bool leaf = g_parmTree.find(parm) == g_parmTree.end();
            StatusSet sset;
            if (x == '1' or x == '3' or x != '2') {
                sset.insert(dqm_core::Result::Red);
            }
            if (x == '2' or x == '3' or x != '1') {
                sset.insert(dqm_core::Result::Yellow);
            }
            sset.insert(dqm_core::Result::Green);
            children.push_back(ParamInfo(parm, leaf, sset));
        }
    }
}

// Return the list of all DQM results for a given run and parameter name.
void
DqmArchiveClientTest::results(ResultList& results, unsigned run, const std::string& param) const
{
    // calculate begin/end time of a run, in seconds
    time_t t0 = time_t(run)*10000;
    time_t t1 = t0 + 9666;

    char x = param[param.size()-1];

    // generate result every 97 seconds
    for (time_t t = t0; t < t1; t += 97) {

        int count = 5;
        dqm_core::Result::Status status = dqm_core::Result::Green;
        if (x == '1' or x == '3' or x != '2') {
            if (t-t0 == 97*55) {
                status = dqm_core::Result::Red;
                count -= 2;
            }
        }
        if (x == '2' or x == '3' or x != '1') {
            if (t-t0 == 97*33 or t-t0 == 97*77) {
                status = dqm_core::Result::Yellow;
                count -= 1;
            }
        }
        
        Result::TagList tags;
        // for leaf parameters add some tags
        if (g_parmTree.find(param) == g_parmTree.end()) {
            tags.push_back(std::make_pair("GreenCount", double(count)));
        }
        
        std::chrono::system_clock::time_point tp = std::chrono::system_clock::time_point() + std::chrono::seconds(t);
        Result result(status, tp, tags, false);

        results.push_back(result);
    }
}

// Return the all DQM results for a given run and parameter name plus children parameters,
void
DqmArchiveClientTest::childResults(std::map<std::string, ResultList>& results, unsigned run, const std::string& param) const
{
    this->results(results[param], run, param);

    auto iter = g_parmTree.find(param);
    if (iter != g_parmTree.end()) {
        for (const auto& child: iter->second) {
            this->results(results[child], run, child);
        }
    }
}

// Returns histograms associated with the result.
void
DqmArchiveClientTest::histograms(TObjArray&, unsigned, const std::string&,
        const std::chrono::system_clock::time_point&) const
{
}

} // namespace dqm_archive
