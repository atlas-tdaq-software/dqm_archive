//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "DqmArchiveClientRootTest.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace dqm_archive {

//----------------
// Constructors --
//----------------
DqmArchiveClientRootTest::DqmArchiveClientRootTest(const std::string&)
    : DqmArchiveClientRoot()
    , m_dir("root://eosatlas//eos/atlas/user/s/salnikov/dqma/")
{
}

//--------------
// Destructor --
//--------------
DqmArchiveClientRootTest::~DqmArchiveClientRootTest()
{
}

// Return total number of known runs.
unsigned 
DqmArchiveClientRootTest::runCount() const 
{
    const unsigned lastrun = 23768;

    return lastrun;
}

// Return the list of known run numbers.
void
DqmArchiveClientRootTest::runs(std::vector<RunInfo>& runs, unsigned count, unsigned skip) const
{
    const unsigned lastrun = runCount();

    runs.clear();
    if (skip >= lastrun) return;
    unsigned startrun = lastrun - skip;
    if (count > startrun) count = startrun;
    unsigned endrun = startrun - count;
    runs.reserve(count);
    for (unsigned run = startrun; run != endrun; -- run) {
        runs.push_back(RunInfo(run, "TEST_PART"));
    }
}

// Return single ROOT file for given run number
std::shared_ptr<TFile>
DqmArchiveClientRootTest::file(unsigned run) const
{
    auto flist = files(run);
    if (not flist.empty()) {
        return flist.front();
    }
    return std::shared_ptr<TFile>();
}

// Return all ROOT files for given run number.
std::vector<std::shared_ptr<TFile>>
DqmArchiveClientRootTest::files(unsigned run) const
{
    std::string path = m_dir;
    if (run % 2) {
        path += "dqma-r00001.root";
    } else {
        path += "dqma-r00002.root";
    }

    ERS_DEBUG(1, "DqmArchiveClientRootTest::file: opening file " << path);
    std::vector<std::shared_ptr<TFile>> files;
    files.push_back(std::shared_ptr<TFile>(TFile::Open(path.c_str(), "READ")));
    return files;
}

} // namespace dqm_archive
