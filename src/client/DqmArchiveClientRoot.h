#ifndef DQM_ARCHIVE_DQMARCHIVECLIENTROOT_H
#define DQM_ARCHIVE_DQMARCHIVECLIENTROOT_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include "dqm_archive/client/DqmArchiveClient.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dqm_archive/common/RTypes.h"
#include "dqm_archive/common/TTreeSequence.h"
#include "TFile.h"
#include "TTree.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 *  @ingroup dqm_archive
 *
 *  @brief Partial implementation of DqmArchiveClient interface for ROOT archives.
 *
 *  Implementation of DqmArchiveClient interface based on ROOT file format for 
 *  DQM archive. This class provides partial implementation only, access to ROOT 
 *  files (such as CoCa/EOS storage) will be provided by one of the subclasses.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class DqmArchiveClientRoot: public DqmArchiveClient {
public:

    /**
     *  @brief Constructor does not need any parameters.
     */
    DqmArchiveClientRoot();

    // Destructor
    ~DqmArchiveClientRoot() override;

    /**
     *  @brief Return the list of all parameter names for a given run.
     *  
     *  Returned list is likely to be large as it includes name of every defined parameter. 
     *  
     *  @param[out] names   List of parameter names
     *  @param[in] run      Run number
     */
    void parameters(std::vector<std::string>& names, unsigned run) const override;

    /**
     *  @brief Return the list of parameters for a given run and a parent region name.
     *  
     *  If parent region name is empty then the list of top-level regions is returned. For every 
     *  child parameter of a given parent region returned list contains a pair whose first item is 
     *  child parameter name and second item is a boolean which is set to true if parameter is a 
     *  leaf (has no other children).
     *  
     *  @param[out] children List of child parameters.
     *  @param[in] run      Run number
     *  @param[in] parent   Parent region name
     */
    void childParameters(std::vector<ParamInfo>& children, unsigned run,
            const std::string& parent) const override;

    /**
     *  @brief Return the list of all DQM results for a given run and parameter name.
     *  
     *  @param[out] results List of results
     *  @param[in] run      Run number
     *  @param[in] param    DQM Parameter name
     */
    void results(ResultList& results, unsigned run, const std::string& param) const override;

    /**
     *  @brief Return the all DQM results for a given run and parameter name plus children parameters.
     *  
     *  This method returns a map from parameter names to result lists. Parameter names should include
     *  parameter specified as an argument and all its direct children (if there are any).
     *  Results of the children of specified parameter will not have their corresponding tags set, but
     *  results for the parameter itself will have all tags.
     *  
     *  @param[out] results Mapping of the parameter name to result list
     *  @param[in] run      Run number
     *  @param[in] param    DQM Parameter name
     */
    void childResults(std::map<std::string, ResultList>& results, unsigned run,
            const std::string& param) const override;

    /**
     *  @brief Returns histograms associated with the result.
     *
     *  Returns the list (possibly empty) of TH1 type instances or its sub-classes.
     *  On input one has to specify run number, parameter name, and timestamp of the result.
     *  Timestamp should be the value obtained from Result object returned from one of the
     *  above methods.
     *
     *  The histograms produced by this method will be owned by histos array (the method calls
     *  histos.SetOwner(kTRUE) on array object).
     *
     *  @param[out] histos  Returned list of histogram objects
     *  @param[in] run      Run number
     *  @param[in] param    Parameter name
     *  @param[in] time     Time of the result
     */
    void histograms(TObjArray& histos, unsigned run, const std::string& param,
            const std::chrono::system_clock::time_point& time) const override;

protected:

    /**
     * @brief Return single ROOT file for given run number.
     *
     * For runs with multiple files it is not specified which one of the files
     * is returned.
     *
     * @return zero pointer if run is not archived.
     */
    virtual std::shared_ptr<TFile> file(unsigned run) const = 0;

    /**
     * @brief Return all ROOT files for given run number.
     *
     * Returned files must be ordered by taime range, earlier files come first
     * in the returned list.
     *
     * @return empty list if run is not archived.
     */
    virtual std::vector<std::shared_ptr<TFile>> files(unsigned run) const = 0;

    /**
     *  Find parameter Id from parameter name, for empty parameter name returns -1,
     *  for non-existing parameter name returns std::numeric_limits<int>::min()
     */
    static int findParm(const TTreeSequence<TString*>& parmseq, const std::string& param);

private:

    typedef TTreeSequence<RTypes::ParamTreeRepr> ParamTreeSeq;
    typedef TTreeSequence<RTypes::StatusCounters> StatCountersSeq;
    typedef TTreeSequence<RTypes::ResultIndex> ResultIndexSeq;
    typedef TTreeSequence<RTypes::TagValueRepr> TagValueSeq;
    typedef TTreeSequence<RTypes::HistoIdRepr> HistoIdSeq;

};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_DQMARCHIVECLIENTROOT_H
