//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "DqmArchiveClientRoot.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <algorithm>
#include <limits>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dqm_archive/common/RootSerializer.h"
#include "ers/ers.h"
#include "TH1.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

  struct HistoTsCmp {
      bool operator()(const dqm_archive::RTypes::HistoIdRepr& hid, dqm_archive::RTypes::Timestamp ts) const {
          return hid.time_usec < ts;
      }
      bool operator()(dqm_archive::RTypes::Timestamp ts, const dqm_archive::RTypes::HistoIdRepr& hid) const {
          return ts < hid.time_usec;
      }
  };

}


//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace dqm_archive {

//----------------
// Constructors --
//----------------
DqmArchiveClientRoot::DqmArchiveClientRoot()
    : DqmArchiveClient()
{
}

//--------------
// Destructor --
//--------------
DqmArchiveClientRoot::~DqmArchiveClientRoot()
{
}

// Return the list of all parameter names for a given run.
void
DqmArchiveClientRoot::parameters(std::vector<std::string>& names, unsigned run) const
{
    names.clear();

    std::shared_ptr<TFile> pfile = this->file(run);
    if (not pfile) return;

    TTree* parmtree = static_cast<TTree*>(pfile->Get("Parameters"));
    if (not parmtree) return;

    typedef TTreeSequence<TString*> NameSeq;
    NameSeq parmseq(parmtree, "name");
    for (const auto& pname: parmseq) {
        names.push_back(pname->Data());
    }
}

// Return the list of parameter names for a given run and a parent region name.
void 
DqmArchiveClientRoot::childParameters(std::vector<ParamInfo>& children, unsigned run, const std::string& parent) const
{
    auto pfiles = this->files(run);
    if (pfiles.empty()) return;

    using StatusAndLeaf = std::pair<StatusSet, bool>;
    std::map<std::string, StatusAndLeaf> childMap;

    // need to merge status info from all files
    bool firstFile = true;
    for (auto&& pfile: pfiles) {

        TTree* parmtree = static_cast<TTree*>(pfile->Get("Parameters"));
        if (not parmtree) return;

        TTree* childtree = static_cast<TTree*>(pfile->Get("ParmTree"));
        if (not childtree) return;

        TTree* stattree = static_cast<TTree*>(pfile->Get("StatusCounters"));
        if (not stattree) return;

        TTreeSequence<TString*> parmseq(parmtree, "name");
        ParamTreeSeq ctreeseq(childtree, "param_tree");
        StatCountersSeq statseq(stattree, "counters");

        ERS_DEBUG(2, "DqmArchiveClientRoot::childParameters: opened all trees");

        int parentId = findParm(parmseq, parent);
        if (parentId == std::numeric_limits<int>::min()) return;

        ERS_DEBUG(2, "DqmArchiveClientRoot::childParameters: found parameter = " << parentId);

        // find parent in ParmTree
        // - items in ctreeseq are ordered by (parent_id, parm_id), this means that all
        //   children will be iterated in order of their ID
        // - items in statseq are ordered by (parm_id, status), together with the above it
        //   means that to find something in statseq we do not need to search from begin to
        //   end but instead we can start at the place where we finished last
        // - same applies to search in ctreeseq
        auto it = std::lower_bound(ctreeseq.begin(), ctreeseq.end(), RTypes::ParamTreeRepr{parentId, 0});
        auto cit = ctreeseq.begin();
        auto sit = statseq.begin();
        ERS_DEBUG(2, "DqmArchiveClientRoot::childParameters: got parameter range for param " << parentId);
        for (; it != ctreeseq.end() and it->parent_id == parentId; ++ it) {

            unsigned childId = it->parm_id;
            std::string const childName(parmseq[childId]->Data());
            ERS_DEBUG(2, "DqmArchiveClientRoot::childParameters: child name " << childName);

            auto& statAndLeaf = childMap[childName];

            // leaf flag need only be checked for the first file, they all must be the same
            if (firstFile) {
                if (cit != ctreeseq.end() and cit->parent_id != int(childId)) {
                    cit = std::lower_bound(cit, ctreeseq.end(), RTypes::ParamTreeRepr{int(childId), 0});
                }
                bool leaf = cit == ctreeseq.end() or cit->parent_id != int(childId);
                statAndLeaf.second = leaf;
                ERS_DEBUG(2, "DqmArchiveClientRoot::childParameters: determined leaf status for " << childId);
            }

            // maybe we are already at correct location?
            if (sit != statseq.end() and sit->parm_id != childId) {
                sit = std::lower_bound(sit, statseq.end(), RTypes::StatusCounters{childId, -999, 0});
            }
            for (; sit != statseq.end() and sit->parm_id == childId; ++ sit) {
                if (sit->counter > 0) statAndLeaf.first.insert(dqm_core::Result::Status(sit->status));
            }

            ERS_DEBUG(2, "DqmArchiveClientRoot::childParameters: determined status set for " << childId);
        }

        firstFile = false;
    }

    // copy merged stuff into final destination
    for (auto&& pair: childMap) {
        auto&& childName = pair.first;
        auto&& sset = pair.second.first;
        auto&& leaf = pair.second.second;
        children.push_back(ParamInfo{childName, leaf, sset});
    }
}

// Return the list of all DQM results for a given run and parameter name.
void
DqmArchiveClientRoot::results(ResultList& results, unsigned run, const std::string& param) const
{
    auto pfiles = this->files(run);
    if (pfiles.empty()) return;

    // need to merge results from all files
    for (auto&& pfile: pfiles) {

        TTree* parmtree = static_cast<TTree*>(pfile->Get("Parameters"));
        if (not parmtree) return;
        ERS_DEBUG(2, "DqmArchiveClientRoot::results: found Parameters tree");

        TTree* childtree = static_cast<TTree*>(pfile->Get("ParmTree"));
        if (not childtree) return;
        ERS_DEBUG(2, "DqmArchiveClientRoot::results: found ParmTree tree");

        TTree* restree = static_cast<TTree*>(pfile->Get("ResultIndex"));
        if (not restree) return;
        ERS_DEBUG(2, "DqmArchiveClientRoot::results: found ResultIndex tree");

        TTree* tagstree = static_cast<TTree*>(pfile->Get("Tags"));
        if (not tagstree) return;
        ERS_DEBUG(2, "DqmArchiveClientRoot::results: found Tags tree");

        TTree* tagnametree = static_cast<TTree*>(pfile->Get("TagNames"));
        if (not tagnametree) return;
        ERS_DEBUG(2, "DqmArchiveClientRoot::results: found TagNames tree");

        TTreeSequence<TString*> parmseq(parmtree, "name");
        ParamTreeSeq ctreeseq(childtree, "param_tree");
        ResultIndexSeq resseq(restree, "index");
        TagValueSeq tagvalseq(tagstree, "tags");
        TTreeSequence<TString*> tagnameseq(tagnametree, "name");

        // find parameter ID
        int iParamId = findParm(parmseq, param);
        if (iParamId < 0) return;
        unsigned paramId = iParamId;

        ERS_DEBUG(2, "DqmArchiveClientRoot::results: found parameter = " << paramId);

        // local cache for tag names to avoid too much seeking in trees
        std::map<RTypes::TagId, std::string> tagNameCache;

        // get all its results together with tags
        auto rit = std::lower_bound(resseq.begin(), resseq.end(),
                RTypes::ResultIndex{RTypes::Timestamp(0), paramId, -1, 0, 0, 0});
        for (; rit != resseq.end() and rit->parm_id == paramId; ++ rit) {

            // get all its tags
            Result::TagList tags;
            if (rit->tag_index >= 0) {
                for (auto tvit = tagvalseq.begin() + rit->tag_index; tvit != tagvalseq.end() and tvit->result_id == rit->result_id; ++ tvit) {
                    std::string& tagname = tagNameCache[tvit->tag_name_id];
                    if (tagname.empty()) {
                        tagname = tagnameseq[tvit->tag_name_id]->Data();
                    }
                    tags.push_back(std::make_pair(tagname, tvit->value));
                }
            }

            std::chrono::system_clock::time_point tp = std::chrono::system_clock::time_point() + std::chrono::microseconds(rit->time_usec);
            Result result(dqm_core::Result::Status(rit->status), tp, tags, rit->histo_index > 0);
            results.push_back(result);
        }
    }
}

// Return the all DQM results for a given run and parameter name plus children parameters,
void
DqmArchiveClientRoot::childResults(std::map<std::string, ResultList>& results, unsigned run, const std::string& param) const
{
    auto pfiles = this->files(run);
    if (pfiles.empty()) return;

    // need to merge info from all files
    for (auto&& pfile: pfiles) {

        TTree* parmtree = static_cast<TTree*>(pfile->Get("Parameters"));
        if (not parmtree) return;

        TTree* childtree = static_cast<TTree*>(pfile->Get("ParmTree"));
        if (not childtree) return;

        TTree* restree = static_cast<TTree*>(pfile->Get("ResultIndex"));
        if (not restree) return;

        TTree* tagstree = static_cast<TTree*>(pfile->Get("Tags"));
        if (not tagstree) return;

        TTree* tagnametree = static_cast<TTree*>(pfile->Get("TagNames"));
        if (not tagnametree) return;


        TTreeSequence<TString*> parmseq(parmtree, "name");
        ParamTreeSeq ctreeseq(childtree, "param_tree");
        ResultIndexSeq resseq(restree, "index");
        TagValueSeq tagvalseq(tagstree, "tags");
        TTreeSequence<TString*> tagnameseq(tagnametree, "name");

        ERS_DEBUG(2, "DqmArchiveClientRoot::childResults: opened all trees");

        // find parameter ID
        int iParamId = findParm(parmseq, param);
        if (iParamId < 0) return;
        RTypes::ParmId paramId = iParamId;

        ERS_DEBUG(2, "DqmArchiveClientRoot::childResults: found parameter = " << paramId);

        // local cache for tag names to avoid too much seeking in trees
        std::map<RTypes::TagId, std::string> tagNameCache;

        // get all its results together with tags
        ResultList& rlist = results[param];
        auto rit = std::lower_bound(resseq.begin(), resseq.end(),
                RTypes::ResultIndex{RTypes::Timestamp(0), paramId, -1, 0, 0, 0});
        ERS_DEBUG(2, "DqmArchiveClientRoot::childResults: got parameter range for param " << paramId);
        for (; rit != resseq.end() and rit->parm_id == paramId; ++ rit) {

            // get all its tags
            Result::TagList tags;
            if (rit->tag_index >= 0) {
                for (auto tvit = tagvalseq.begin() + rit->tag_index; tvit != tagvalseq.end() and tvit->result_id == rit->result_id; ++ tvit) {
                    std::string& tagname = tagNameCache[tvit->tag_name_id];
                    if (tagname.empty()) {
                        tagname = tagnameseq[tvit->tag_name_id]->Data();
                    }
                    tags.push_back(std::make_pair(tagname, tvit->value));
                }
            }

            std::chrono::system_clock::time_point tp = std::chrono::system_clock::time_point() + std::chrono::microseconds(rit->time_usec);
            Result result(dqm_core::Result::Status(rit->status), tp, tags, rit->histo_index > 0);
            rlist.push_back(result);

        }

        // loop over children of this parameter
        // - items in ctreeseq are ordered by (parent_id, parm_id), this means that all
        //   children will be iterated in order of their ID
        // - items in resseq are ordered by (parm_id, time), together with the above it
        //   means that to find something in resseq we do not need to search from begin to
        //   end but instead we can start at the place where we finished last
        auto it = std::lower_bound(ctreeseq.begin(), ctreeseq.end(), RTypes::ParamTreeRepr{int(paramId), 0});
        rit = resseq.begin();
        for (; it != ctreeseq.end() and it->parent_id == int(paramId); ++ it) {
            unsigned childId = it->parm_id;
            ERS_DEBUG(2, "DqmArchiveClientRoot::childResults: found child parameter = " << childId);
            std::string const childName(parmseq[childId]->Data());

            // get results for this child but skip the tags
            ResultList& rlist = results[childName];
            if (rit != resseq.end() and rit->parm_id != childId) {
                rit = std::lower_bound(rit, resseq.end(), RTypes::ResultIndex{RTypes::Timestamp(0), childId, -1, 0, 0, 0});
            }
            ERS_DEBUG(2, "DqmArchiveClientRoot::childResults: got parameter range for param " << childId);
            for (; rit != resseq.end() and rit->parm_id == childId; ++ rit) {

                Result::TagList tags;
                std::chrono::system_clock::time_point tp = std::chrono::system_clock::time_point() + std::chrono::microseconds(rit->time_usec);
                Result result(dqm_core::Result::Status(rit->status), tp, tags, rit->histo_index > 0);
                rlist.push_back(result);

            }

        }
    }

    ERS_DEBUG(2, "DqmArchiveClientRoot::childResults: done");
}

// Returns histograms associated with the result.
void
DqmArchiveClientRoot::histograms(TObjArray& histos, unsigned run, const std::string& param,
        const std::chrono::system_clock::time_point& time) const
{
    auto pfiles = this->files(run);
    if (pfiles.empty()) return;

    // need to merge info from all files
    for (auto&& pfile: pfiles) {

        TTree* parmtree = static_cast<TTree*>(pfile->Get("Parameters"));
        if (not parmtree) return;

        TTree* htree = static_cast<TTree*>(pfile->Get("Histograms"));
        if (not htree) return;

        TTreeSequence<TString*> parmseq(parmtree, "name");
        HistoIdSeq histoIdSeq(htree, "histo_id");
        TTreeSequence<TString*> blobseq(htree, "histo_blob");

        ERS_DEBUG(2, "DqmArchiveClientRoot::histograms: opened all trees");

        // find parameter ID
        int iParamId = findParm(parmseq, param);
        if (iParamId < 0) return;
        RTypes::ParmId paramId = iParamId;

        ERS_DEBUG(2, "DqmArchiveClientRoot::histograms: found parameter = " << paramId);

        // histoIdSeq is ordered by time_usec, so do binary search on time column
        std::chrono::system_clock::duration d = time - std::chrono::system_clock::time_point();
        RTypes::Timestamp ts = std::chrono::duration_cast<std::chrono::microseconds>(d).count();
        ERS_DEBUG(2, "DqmArchiveClientRoot::histograms: ts=" << ts);

        RootSerializer serializer;

        // find all matching timestamps, loop over them, take ones with matching parameter id
        auto range = std::equal_range(histoIdSeq.cbegin(), histoIdSeq.cend(), ts, ::HistoTsCmp());
        ERS_DEBUG(2, "DqmArchiveClientRoot::histograms: " << std::distance(range.first, range.second) << " entries for this timestamp");
        for (auto it = range.first; it != range.second; ++ it) {
            ERS_DEBUG(2, "DqmArchiveClientRoot::histograms: loop param_id = " << it->parm_id);
            if (it->parm_id == paramId) {
                auto index = std::distance(histoIdSeq.cbegin(), it);
                ERS_DEBUG(2, "DqmArchiveClientRoot::histograms: index = " << index);
                const TString& blob = *blobseq[index];

                ERS_DEBUG(2, "DqmArchiveClientRoot::histograms: blob length = " << blob.Length());

                if (TObject* obj = serializer.deserialize(blob)) {

                    ERS_DEBUG(2, "DqmArchiveClientRoot::histograms: deserialized object");

                    // is it histogram?
                    if (TH1* th = dynamic_cast<TH1*>(obj)) {
                        ERS_DEBUG(2, "DqmArchiveClientRoot::histograms: deserialized object is TH1");
                        // make sure that it is user-owned
                        th->SetDirectory(0);
                        th->SetBit(kCanDelete);
                        histos.Add(th);
                    } else {
                        delete obj;
                    }
                }
            }
        }

        // histograms for a given timestamp can only exist in one file,
        // so we can stop after the first file where it was found
        if (histos.GetEntries() > 0) {
            break;
        }
    }

    // all histograms should be owned by array
    histos.SetOwner(kTRUE);
}

//  Find parameter Id from parameter name.
int
DqmArchiveClientRoot::findParm(const TTreeSequence<TString*>& parmseq, const std::string& param)
{
    if (param.empty()) return -1;

    const TString tparam(param.c_str());
    TTreeSequence<TString*>::const_iterator it = std::lower_bound(parmseq.begin(), parmseq.end(), &tparam,
            [](const TString* lhs, const TString* rhs) { return *lhs < *rhs; });

    if (it != parmseq.end() and *(*it) == tparam) {
        return int(it - parmseq.begin());
    }
    return std::numeric_limits<int>::min();
}

} // namespace dqm_archive
