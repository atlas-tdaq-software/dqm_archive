#ifndef DQM_ARCHIVE_DQMARCHIVECLIENTROOTTEST_H
#define DQM_ARCHIVE_DQMARCHIVECLIENTROOTTEST_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include "DqmArchiveClientRoot.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 *  @ingroup dqm_archive
 *
 *  @brief Test implementation of DqmArchiveClient interface for ROOT archives.
 *
 *  This implementation provides some fake data for run numbers and points
 *  to simulated data in ROOT archives. Useful for testing only.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class DqmArchiveClientRootTest : public DqmArchiveClientRoot {
public:

    /**
     *  @brief Constructor takes option string (needed by factory), not used currently.
     */
    DqmArchiveClientRootTest(const std::string& options);
    
    // Destructor
    ~DqmArchiveClientRootTest() override;

    /**
     *  @brief Return total number of known runs.
     *
     *  @return Total count of runs in database
     *
     *  @throw Exception
     */
    unsigned runCount() const override;

    /**
     *  @brief Return the list of known runs.
     *
     *  This method returns the list of runs known to the archive. It returns up to count
     *  of latest run numbers (optionally skipping few latest ones). Runs are returned in the
     *  reverse time order which is not necessary the same as a run number order.
     *
     *  @param[out] runs   List of runs returned
     *  @param[in] count   Max. number of runs to return
     *  @param[in] skip    Number of last runs to skip
     *
     *  @throw Exception
     */
    void runs(std::vector<RunInfo>& runs, unsigned count, unsigned skip = 0) const override;

protected:

    /**
     * @brief Return single ROOT file for given run number.
     *
     * For runs with multiple files it is not specified which one of the files
     * is returned.
     *
     * @return zero pointer if run is not archived.
     */
    std::shared_ptr<TFile> file(unsigned run) const override;

    /**
     * @brief Return all ROOT files for given run number.
     *
     * Returned files must be ordered by taime range, earlier files come first
     * in the returned list.
     *
     * @return empty list if run is not archived.
     */
    std::vector<std::shared_ptr<TFile>> files(unsigned run) const override;

private:
    
    std::string m_dir;
};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_DQMARCHIVECLIENTROOTTEST_H
