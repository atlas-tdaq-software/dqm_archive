//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <boost/python.hpp>
#include <boost/filesystem.hpp>
#include <stdlib.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dqm_archive/client/DqmArchiveClient.h"
#include "ers/ers.h"
#include "TPython.h"
#include "TH1.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------
using namespace dqm_archive;
using namespace boost::python;
namespace boopy = boost::python;
namespace boofs = boost::filesystem;

#if PY_MAJOR_VERSION >= 3
#define PY3MIG_STRING_FROM_STRING(STR) PyUnicode_FromString(STR)
#else
#define PY3MIG_STRING_FROM_STRING(STR) PyString_FromString(STR)
#endif

namespace {

/**
 *  Wrapper for dqm_core::Result
 */
class PyDqmResult {
public:

    enum Status { Disabled = -1, Undefined, Red, Yellow, Green };

    PyDqmResult(const Result& res) : m_result(res) {}

    Status status() const { return Status(m_result.status()); }
    double time() const;
    boopy::dict tags() const;
    bool hasHistograms() const;

    void print(std::ostream& out) const;

private:
    Result m_result;
};

double
PyDqmResult::time() const
{
    std::chrono::system_clock::duration d = m_result.time() - std::chrono::system_clock::time_point();
    return std::chrono::duration_cast<std::chrono::microseconds>(d).count() / 1e6;
}

bool
PyDqmResult::hasHistograms() const
{
    return m_result.hasHistograms();
}

boopy::dict
PyDqmResult::tags() const
{
    // convert tags to Python dict
    boopy::dict tags;
    for (const auto& pair: m_result.tags()) {
        tags[pair.first] = pair.second;
    }
    return tags;
}

void
PyDqmResult::print(std::ostream& out) const
{
    out << "Result(" << m_result.status();
    for (const auto& pair: m_result.tags()) {
        out << ", ['" << pair.first << "':" << pair.second << "]";
    }
    out << ", " << long(time() * 1e6 + 0.5) << "us"
        << ", histo=" << (m_result.hasHistograms() ? "yes" : "no")
        << ")";
}



/**
 *  Wrapper for dqm_archive::ParamInfo
 */
class PyDqmParamInfo {
public:

    PyDqmParamInfo(const ParamInfo& pinfo) : m_pinfo(pinfo) {}

    std::string name() const { return m_pinfo.name(); }
    bool leaf() const { return m_pinfo.leaf(); }
    boopy::list statusSet() const;

    void print(std::ostream& out) const;

private:
    dqm_archive::ParamInfo m_pinfo;
};

boopy::list
PyDqmParamInfo::statusSet() const
{
    // convert StatusSet to Python tuple
    StatusSet sset = m_pinfo.statusSet();
    boopy::list pyset;
    if (sset.contains(dqm_core::Result::Disabled)) pyset.append(PyDqmResult::Status::Disabled);
    if (sset.contains(dqm_core::Result::Undefined)) pyset.append(PyDqmResult::Status::Undefined);
    if (sset.contains(dqm_core::Result::Red)) pyset.append(PyDqmResult::Status::Red);
    if (sset.contains(dqm_core::Result::Yellow)) pyset.append(PyDqmResult::Status::Yellow);
    if (sset.contains(dqm_core::Result::Green)) pyset.append(PyDqmResult::Status::Green);
    return pyset;
}

void
PyDqmParamInfo::print(std::ostream& out) const
{
    out << "ParamInfo(\"" << m_pinfo.name() << "\", ";
    out << "leaf=" << (m_pinfo.leaf()?"True":"False");

    out << ", status=[";
    StatusSet sset = m_pinfo.statusSet();
    const char* sep = "";
    if (sset.contains(dqm_core::Result::Disabled)) {
        out << sep << "Disabled";
        sep = ", ";
    }
    if (sset.contains(dqm_core::Result::Undefined)) {
        out << sep << "Undefined";
        sep = ", ";
    }
    if (sset.contains(dqm_core::Result::Red)) {
        out << sep << "Red";
        sep = ", ";
    }
    if (sset.contains(dqm_core::Result::Yellow)) {
        out << sep << "Yellow";
        sep = ", ";
    }
    if (sset.contains(dqm_core::Result::Green)) {
        out << sep << "Green";
        sep = ", ";
    }
    out << "])";
}


/**
 *  Wrapper for dqm_archive::RunInfo
 */
class PyDqmRunInfo {
public:

    PyDqmRunInfo(const RunInfo& pinfo) : m_pinfo(pinfo) {}

    unsigned run() const { return m_pinfo.run(); }
    std::string partition() const { return m_pinfo.partition(); }

    void print(std::ostream& out) const;

private:
    RunInfo m_pinfo;
};

void
PyDqmRunInfo::print(std::ostream& out) const
{
    out << "RunInfo(" << run() << ", \"" << partition() << "\")";
}


/**
 *  Wrapper for DqmArchiveClient class.
 */
class PyDqmArchiveClient {
public:

    PyDqmArchiveClient(const std::string& param = "rootcoca")
        : m_instance(DqmArchiveClient::instance(param))
    {
        if (not m_instance) {
            throw std::runtime_error("failed to create DqmArchiveClient instance, param: "+param);
        }
    }

    unsigned runCount() { return m_instance->runCount(); }
    boopy::list runs(unsigned count, unsigned skip = 0);
    boopy::list parameters(unsigned run);
    boopy::list childParameters(unsigned run, const std::string& parent);
    boopy::list results(unsigned run, const std::string& param);
    boopy::dict childResults(unsigned run, const std::string& param);
    boopy::list histograms(unsigned run, const std::string& param, double time);

private:
    std::shared_ptr<DqmArchiveClient> m_instance;
};

boopy::list
PyDqmArchiveClient::runs(unsigned count, unsigned skip)
{
    // call C+ method
    std::vector<RunInfo> runs;
    m_instance->runs(runs, count, skip);

    // convert vector into list
    boopy::list pyruns;
    for (auto run: runs) {
        pyruns.append(PyDqmRunInfo(run));
    }

    return pyruns;
}

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(runs_overloads, runs, 1, 2)

boopy::list
PyDqmArchiveClient::parameters(unsigned run)
{
    // call C+ method
    std::vector<std::string> names;
    m_instance->parameters(names, run);

    // convert vector into list
    boopy::list res;
    for (const auto& name: names) {
        res.append(name);
    }
    return res;
}

boopy::list
PyDqmArchiveClient::childParameters(unsigned run, const std::string& parent)
{
    // call C+ method
    std::vector<ParamInfo> children;
    m_instance->childParameters(children, run, parent);

    // convert vector into list
    boopy::list res;
    for (const auto& child: children) {
        res.append(PyDqmParamInfo(child));
    }
    return res;
}

boopy::list
PyDqmArchiveClient::results(unsigned run, const std::string& param)
{
    // call C+ method
    DqmArchiveClient::ResultList results;
    m_instance->results(results, run, param);

    // convert vector into list
    boopy::list res;
    for (const auto& rptr: results) {
        PyDqmResult pyres(rptr);
        res.append(pyres);
    }
    return res;
}

boopy::dict
PyDqmArchiveClient::childResults(unsigned run, const std::string& param)
{
    // call C+ method
    std::map<std::string, DqmArchiveClient::ResultList> resmap;
    m_instance->childResults(resmap, run, param);

    // convert vector into list
    boopy::dict res;
    for (const auto& pair: resmap) {
        boopy::list list;
        for (const auto& res: pair.second) {
            PyDqmResult pyres(res);
            list.append(pyres);
        }
        res[pair.first] = list;
    }
    return res;
}

boopy::list
PyDqmArchiveClient::histograms(unsigned run, const std::string& param, double time)
{
    // need to be careful with floating point precision of python time
    // NOTE: we know that archive works with microsecond precision, this is why we can do this
    uint64_t usec = uint64_t(time * 1e6 + 0.5);
    std::chrono::system_clock::time_point ts = std::chrono::system_clock::time_point() + std::chrono::microseconds(usec);

    TObjArray histos;
    m_instance->histograms(histos, run, param, ts);

    // need to take over ownership from an array
    histos.SetOwner(kFALSE);

    boopy::list res;
    TObjArray::Iterator_t iter(&histos);
    while (TObject* tobj = iter.Next()) {
        ERS_DEBUG(2, "PyDqmArchiveClient::histograms: class=" << tobj->ClassName());
        if (TH1* th1 = dynamic_cast<TH1*>(tobj)) {
#if ROOT_VERSION_CODE >= ROOT_VERSION(6,22,0)
            PyObject* pyth1 = TPython::CPPInstance_FromVoidPtr(static_cast<void*>(th1), th1->ClassName(), true);
#else
            PyObject* pyth1 = TPython::ObjectProxy_FromVoidPtr(static_cast<void*>(th1), th1->ClassName(), true);
#endif
            res.append(boopy::object(handle<>(pyth1)));

            // The lifetime of the C++ ROOT object is controlled by Python wrapper object from here on.
            // To avoid problems in case this same object is placed into some other list (like when it
            // is drawn on canvas) we reset kCanDelete for this object.
            tobj->ResetBit(kCanDelete);

        } else {
            // non-histogram, should destroy it to avoid mem leak
            delete tobj;
        }
    }
    return res;
}


void exception_translator(const std::exception& exc)
{
  PyErr_SetString(PyExc_UserWarning, exc.what());
}

template <typename T>
std::string toString(const T& r)
{
    std::ostringstream str;
    r.print(str);
    return str.str();
}
} // namespace

BOOST_PYTHON_MODULE(libpydqm_archive)
{
    // PyROOT needs to have Python module imported first, trouble here is that TDAQ does not set PYTHONPATH
    // to include location of PyROOT, we need to do something fancy here
    if (const char* rootsys = getenv("ROOTSYS")) {
        boofs::path path(rootsys);
        path /= "lib";
        PyObject* rootsyslib = PY3MIG_STRING_FROM_STRING(path.string().c_str());

        PyObject* sys = PyImport_ImportModule("sys");
        if (not sys) return;
        PyObject* sys_path = PyObject_GetAttrString(sys, "path");
        if (not sys_path) return;
        PyList_Append(sys_path, rootsyslib);

        Py_CLEAR(sys_path);
        Py_CLEAR(sys);
        Py_CLEAR(rootsyslib);

        // can try to import ROOT now
        PyObject* ROOT = PyImport_ImportModule("ROOT");
        if (not ROOT) return;
        Py_CLEAR(ROOT);
    }
  
    // handle exceptions
    boopy::register_exception_translator<std::exception>(exception_translator);


    // wrapper for PyDqmArchiveClient class
    class_<PyDqmArchiveClient, boost::noncopyable>("DqmArchiveClient", init<optional<std::string>>(arg("param")="rootcoca"))
        .def("runCount", &PyDqmArchiveClient::runCount, "Return total number of known runs.")
        .def("runs", &PyDqmArchiveClient::runs,
                runs_overloads((arg("count"), arg("skip")=0),
                "This method returns the list of run numbers known to the archive."
                " It returns up to count of latest run numbers (optionally skipping"
                " few latest ones). Runs are returned in the reverse time order which"
                " is not necessary the same as a run number order. Returned value is a"
                " tuple whose first item is a list of runs and second item is total count"
                " of runs stored in the database."))
        .def("parameters", &PyDqmArchiveClient::parameters, args("run"),
                "Return the list of all parameter names for a given run. Returned list is likely"
                " to be large as it includes name of every defined parameter. ")
        .def("childParameters", &PyDqmArchiveClient::childParameters, args("run", "parent"),
                "Return the list of parameters for a given run and a parent region name."
                " If parent region name is empty then the list of top-level regions is returned."
                " Returns the list of ParamInfo objects.")
        .def("results", &PyDqmArchiveClient::results, args("run", "param"),
                "Return the list of all DQM results for a given run and parameter name. Returns"
                " list of result instances.")
        .def("childResults", &PyDqmArchiveClient::childResults, args("run", "param"),
                "Return the all DQM results for a given run and parameter name plus children parameters."
                " This method returns a map from parameter names to result lists. Parameter names should"
                " include parameter specified as an argument and all its children (if there are any).")
        .def("histograms", &PyDqmArchiveClient::histograms, args("run", "param", "time"),
                "Returns the list (possibly empty) of TH1 type instances or its sub-classes. "
                "On input one has to specify run number, parameter name, and timestamp of the result. "
                "Timestamp should be the value obtained from Result object returned from one of the above methods.")
        ;

    // wrapper for PyDqmParamInfo class
    class_<PyDqmParamInfo>("ParamInfo", no_init)
        .def("name", &PyDqmParamInfo::name, "Returns parameter name.")
        .def("leaf", &PyDqmParamInfo::leaf, "Returns True if parameter is a leaf parameter.")
        .def("statusSet", &PyDqmParamInfo::statusSet,
                "Returns set of status values for this parameter, result is returned as a list of enum Status values.")
        .def("__str__", &::toString<PyDqmParamInfo>)
        .def("__repr__", &::toString<PyDqmParamInfo>)
        ;


    // wrapper for PyDqmRunInfo class
    class_<PyDqmRunInfo>("RunInfo", no_init)
        .def("run", &PyDqmRunInfo::run, "Returns run number.")
        .def("partition", &PyDqmRunInfo::partition, "Returns partition name.")
        .def("__str__", &::toString<PyDqmRunInfo>)
        .def("__repr__", &::toString<PyDqmRunInfo>)
        ;


    {
        // wrapper for PyDqmResult class
        scope in_PyDqmResult = class_<PyDqmResult>("Result", no_init)
            .def("status", &PyDqmResult::status,
                    "Returns DQM result status value as Status enum value")
            .def("time", &PyDqmResult::time,
                    "Returns result timestamp as double value (seconds since UTC epoch time)")
            .def("tags", &PyDqmResult::tags,
                    "Returns dictionary of tag names and their values (string->double)")
            .def("hasHistograms", &PyDqmResult::hasHistograms,
                    "Returns true if result has associated histograms")
            .def("__str__", &::toString<PyDqmResult>)
            .def("__repr__", &::toString<PyDqmResult>)
            ;

        // define enum in PyDqmResult scope
        enum_<PyDqmResult::Status>("Status")
            .value("Disabled", PyDqmResult::Disabled)
            .value("Undefined", PyDqmResult::Undefined)
            .value("Red", PyDqmResult::Red)
            .value("Yellow", PyDqmResult::Yellow)
            .value("Green", PyDqmResult::Green)
            .export_values()
            ;
    }
}
