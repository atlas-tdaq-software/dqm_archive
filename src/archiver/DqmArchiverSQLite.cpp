//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "dqm_archive/archiver/DqmArchiverSQLite.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dqm_config/dal/DQParameter.h"
#include "dqm_config/dal/DQRegion.h"
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace std::chrono;

namespace {

    ERS_DECLARE_ISSUE(errors, SQLiteError, message << ", SQLite error: " << sqlerror, ((std::string) message) ((std::string) sqlerror))


    const char* table_ddl[] = {

            "CREATE TABLE Parameters ("
                "parm_id INTEGER PRIMARY KEY, "
                "name TEXT UNIQUE NOT NULL, "
                "parent_id INTEGER NOT NULL REFERENCES Parameters (parm_id) "
            ")",

            "CREATE TABLE Results ("
                "res_id INTEGER PRIMARY KEY, "
                "time_usec INTEGER NOT NULL, "
                "parm_id INTEGER NOT NULL REFERENCES Parameters (parm_id), "
                "status INTEGER NOT NULL"
            ")",

            "CREATE INDEX Results_parm_id ON Results (parm_id)",
            "CREATE INDEX Results_parm_id_status ON Results (parm_id, status)",

            "CREATE TABLE TagNames ("
                "tag_id INTEGER PRIMARY KEY, "
                "name TEXT UNIQUE NOT NULL"
            ")",

            "CREATE TABLE Tags ("
                "tagval_id INTEGER PRIMARY KEY, "
                "res_id INTEGER NOT NULL REFERENCES Results (res_id), "
                "tag_id INTEGER NOT NULL REFERENCES TagNames (tag_id), "
                "value FLOAT NOT NULL"
            ")",

    };


    class Stmt {
    public:
        Stmt(sqlite3* db, const std::string& q) : m_db(db) {
            if (sqlite3_prepare_v2(m_db, q.c_str(), -1, &m_stmt, 0) != SQLITE_OK) {
                throw ::errors::SQLiteError(ERS_HERE, "failed to compile statement", sqlite3_errmsg(m_db));
            }
        }

        ~Stmt() { sqlite3_finalize(m_stmt); }

        void bind_int(int idx, int value) {
            if (sqlite3_bind_int(m_stmt, idx, value) != SQLITE_OK) {
                throw ::errors::SQLiteError(ERS_HERE, "bind failed", sqlite3_errmsg(m_db));
            }
        }

        void bind_int64(int idx, sqlite3_int64 value) {
            if (sqlite3_bind_int64(m_stmt, idx, value) != SQLITE_OK) {
                throw ::errors::SQLiteError(ERS_HERE, "bind failed", sqlite3_errmsg(m_db));
            }
        }

        void bind_double(int idx, double value) {
            if (sqlite3_bind_double(m_stmt, idx, value) != SQLITE_OK) {
                throw ::errors::SQLiteError(ERS_HERE, "bind failed", sqlite3_errmsg(m_db));
            }
        }

        void bind_text(int idx, const std::string& value) {
            if (sqlite3_bind_text(m_stmt, idx, value.c_str(), value.size(), SQLITE_STATIC) != SQLITE_OK) {
                throw ::errors::SQLiteError(ERS_HERE, "bind failed", sqlite3_errmsg(m_db));
            }
        }

        int step() { return sqlite3_step(m_stmt); }

        void reset() {
            if (sqlite3_reset(m_stmt) != SQLITE_OK) {
                throw ::errors::SQLiteError(ERS_HERE, "reset failed", sqlite3_errmsg(m_db));
            }
        }

    private:
        sqlite3* m_db;
        sqlite3_stmt *m_stmt;
    };

    // parameters for buffer size, buffer grows until it reaches maxBufferSize,
    // then initial part of it is sent to file to reach minBufferSize
    unsigned minBufferSize =  1000;
    unsigned maxBufferSize = 11000;

    typedef std::pair<std::string, std::shared_ptr<dqm_core::Result>> ResultPair;

    bool compareResults(const ResultPair& lhs, const ResultPair& rhs) {
       // order by time first and name second
        if (lhs.second->universal_time_ < rhs.second->universal_time_) return true;
        if (lhs.second->universal_time_ > rhs.second->universal_time_) return false;
       return lhs.first < rhs.first;
   }
}


//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace dqm_archive {

//----------------
// Constructors --
//----------------
DqmArchiverSQLite::DqmArchiverSQLite(const std::string& fileName,
                                     const std::map<std::string, std::string>& paramTree)
    : IDqmArchiver()
    , m_ppDb(0)
    , m_parm2id()
    , m_tag2id()
    , m_buffer()
    , m_resCount(0)
    , m_tagCount(0)
    , m_statusCount()
{
    // open the file
    int rc = sqlite3_open_v2(fileName.c_str(), &m_ppDb,
            SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, 0);
    if (rc != SQLITE_OK) {
        throw ::errors::SQLiteError(ERS_HERE, "failed to open file "+fileName,
                sqlite3_errmsg(m_ppDb));
    }

    // create the tables
    for (auto q: table_ddl) {
        char* errmsg = 0;
        if (sqlite3_exec(m_ppDb, q, 0, 0, &errmsg) != SQLITE_OK) {
            throw ::errors::SQLiteError(ERS_HERE, "table definition failed", errmsg);
        }
    }

    // save param tree
    storeConfig(paramTree);
}

//--------------
// Destructor --
//--------------
DqmArchiverSQLite::~DqmArchiverSQLite()
{
    try {
        // try to close but suppress any exceptions
        if (m_ppDb) {
            finish();
        }
    } catch (...) {
    }
}

void
DqmArchiverSQLite::storeConfig(const std::map<std::string, std::string>& paramTree)
{
    // copy all parameter names and add to ordered container
    for (const auto& pair: paramTree) {
        m_parm2id[pair.first] = 0;
    }

    // enumerate them
    unsigned counter = 0;
    for (auto& pair: m_parm2id) {
        pair.second = counter ++;
    }

    // get the ID of a parent for each parameter
    std::vector<int> parents(m_parm2id.size());
    for (const auto& pair: paramTree) {
        int parentId = -1;
        auto nit = m_parm2id.find(pair.second);
        if (nit != m_parm2id.end()) parentId = nit->second;

        parents[m_parm2id[pair.first]] = parentId;
    }

    char* errmsg = 0;
    if (sqlite3_exec(m_ppDb, "BEGIN", 0, 0, &errmsg) != SQLITE_OK) {
        throw ::errors::SQLiteError(ERS_HERE, "start transaction failed", errmsg);
    }

    // store all that now in Parameters table
    ERS_DEBUG(1, "DqmArchiverSQLite::storeConfig: start storing " << m_parm2id.size() << " parameters");
    {
        Stmt stmt(m_ppDb, "INSERT INTO Parameters (parm_id, name, parent_id) VALUES (?1, ?2, ?3)");
        for (auto& pair: m_parm2id) {
            stmt.bind_int(1, pair.second);
            stmt.bind_text(2, pair.first);
            stmt.bind_int(3, parents[pair.second]);
            if (stmt.step() != SQLITE_DONE) {
                throw ::errors::SQLiteError(ERS_HERE, "failed to store data", sqlite3_errmsg(m_ppDb));
            }
            stmt.reset();
        }
    }

    if (sqlite3_exec(m_ppDb, "END", 0, 0, &errmsg) != SQLITE_OK) {
        throw ::errors::SQLiteError(ERS_HERE, "end transaction failed", errmsg);
    }

    ERS_DEBUG(1, "DqmArchiverSQLite::storeConfig: stored " << m_parm2id.size() << " parameters");
}

bool
DqmArchiverSQLite::store(const std::string& name, const std::shared_ptr<dqm_core::Result>& result)
{
    // insert it into a buffer
    ResultPair res(name, result);
    auto it = std::upper_bound(m_buffer.begin(), m_buffer.end(), res, ::compareResults);
    m_buffer.insert(it, res);

    // if buffer is full flush some of it
    if (m_buffer.size() >= ::maxBufferSize) {
        flush(m_buffer.size() - ::minBufferSize);
    }

    return true;
}

// Finish archiving, close all output files, etc.
void
DqmArchiverSQLite::finish()
{
    // store remaining stuff in file
    flush(m_buffer.size());

    if (m_ppDb) {
        if (sqlite3_close(m_ppDb) != SQLITE_OK) {
            ers::error(::errors::SQLiteError(ERS_HERE, "failed to close database", sqlite3_errmsg(m_ppDb)));
        }
        m_ppDb = 0;
    }
}

// write count of results from buffer into file
void
DqmArchiverSQLite::flush(unsigned count)
{
    ERS_DEBUG(2, "DqmArchiverSQLite::flush: count=" << count);

    char* errmsg;
    if (sqlite3_exec(m_ppDb, "BEGIN", 0, 0, &errmsg) != SQLITE_OK) {
        throw ::errors::SQLiteError(ERS_HERE, "start transaction failed", errmsg);
    }

    Stmt tag_names_stmt(m_ppDb, "INSERT INTO TagNames (tag_id, name) VALUES (?1, ?2)");
    Stmt result_stmt(m_ppDb, "INSERT INTO Results (res_id, time_usec, parm_id, status) VALUES (?1, ?2, ?3, ?4)");
    Stmt tags_stmt(m_ppDb, "INSERT INTO Tags (tagval_id, res_id, tag_id, value) VALUES (?1, ?2, ?3, ?4)");

    // save first count results
    for (auto it = m_buffer.begin(); it != m_buffer.begin()+count; ++ it, ++ m_resCount) {

        ERS_DEBUG(3, "DqmArchiverSQLite::flush: store result " << m_resCount);

        const ResultPair& resp = *it;

        // need ID for parameter name
        unsigned parm_id = 0;
        auto nit = m_parm2id.find(resp.first);
        if (nit == m_parm2id.end()) {
            ERS_LOG("DqmArchiverSQLite::flush -- unexpected result name: " << resp.first);
            continue;
        }
        parm_id = nit->second;

        // store result
        const dqm_core::Result& res = *resp.second;

        boost::posix_time::time_duration since_epoch = res.universal_time_ - boost::posix_time::from_time_t(time_t(0));

        result_stmt.bind_int(1, m_resCount);
        result_stmt.bind_int64(2, since_epoch.total_microseconds());
        result_stmt.bind_int(3, parm_id);
        result_stmt.bind_int(4, res.status_);
        if (result_stmt.step() != SQLITE_DONE) {
            throw ::errors::SQLiteError(ERS_HERE, "failed to store data", sqlite3_errmsg(m_ppDb));
        }
        result_stmt.reset();

        // count all statuses
        ++ m_statusCount[std::make_pair(parm_id, res.status_)];

        // store all tags
        for (const auto& tag: res.tags_) {

            // need ID for tag name
            unsigned tag_id = 0;
            auto nit = m_tag2id.find(tag.first);
            if (nit == m_tag2id.end()) {
                tag_id = m_tag2id.size();
                m_tag2id.insert(std::make_pair(tag.first, tag_id));

                tag_names_stmt.bind_int(1, tag_id);
                tag_names_stmt.bind_text(2, tag.first);
                if (tag_names_stmt.step() != SQLITE_DONE) {
                    throw ::errors::SQLiteError(ERS_HERE, "failed to store data", sqlite3_errmsg(m_ppDb));
                }
                tag_names_stmt.reset();

            } else {
                // use existing mapping
                tag_id = nit->second;
            }

            tags_stmt.bind_int(1, m_tagCount++);
            tags_stmt.bind_int(2, m_resCount);
            tags_stmt.bind_int(3, tag_id);
            tags_stmt.bind_int(4, tag.second);
            if (tags_stmt.step() != SQLITE_DONE) {
                throw ::errors::SQLiteError(ERS_HERE, "failed to store data", sqlite3_errmsg(m_ppDb));
            }
            tags_stmt.reset();
        }

    }

    if (sqlite3_exec(m_ppDb, "END", 0, 0, &errmsg) != SQLITE_OK) {
        throw ::errors::SQLiteError(ERS_HERE, "end transaction failed", errmsg);
    }

    // erase stored data from buffer
    m_buffer.erase(m_buffer.begin(), m_buffer.begin()+count);
}

} // namespace dqm_archive
