#ifndef DQM_ARCHIVE_ASYNCFINALIZER_H
#define DQM_ARCHIVE_ASYNCFINALIZER_H

//-----------------
// C/C++ Headers --
//-----------------
#include <chrono>
#include <future>
#include <memory>
#include <string>

//----------------------
// Base Class Headers --
//----------------------
#include "dqm_archive/archiver/AsyncTaskThread.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace dqm_archive {
class ILTSArchiver;
}

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 * @ingroup dqm_archive
 *
 * @brief Class encapsulating finalization process for DQMA ROOT files.
 *
 * Finalization runs in a separate thread and process and this class provides
 * management interface to handle all complexities.
 */

class AsyncFinalizer : public AsyncTaskThread {
public:

    /**
     * Constructor starts finalization process in a separate thread.
     *
     * @param path  Path to the ROOT file to be finalized, file should be closed.
     * @param ltsArchiver  Long term storage archiver.
     * @param indexingTimeout  Timeout for indexing operation.
     */
    AsyncFinalizer(const std::string& path,
                   std::shared_ptr<ILTSArchiver> const& ltsArchiver,
                   std::chrono::milliseconds indexingTimeout,
                   const std::string& name);

    // copy is disabled
    AsyncFinalizer(AsyncFinalizer const&) = delete;
    AsyncFinalizer operator=(AsyncFinalizer const&) = delete;

    /**
     * If finalization has not completed yet then destructor will
     * detach its thread.
     */
    ~AsyncFinalizer() override = default;

protected:

    // Method that runs in a thread.
    // NB: parameters are passed by value because thread can be
    // detached and run even after the controlling object dies.
    static void finalizeTread(std::string path,
                              std::shared_ptr<ILTSArchiver> ltsArchiver,
                              std::chrono::milliseconds indexingTimeout,
                              std::promise<bool> promise);

};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_ASYNCFINALIZER_H
