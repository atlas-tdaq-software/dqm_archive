//-----------------------
// This Class's Header --
//-----------------------
#include "dqm_archive/archiver/FileNameFactoryTemplate.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <regex>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/client/Register.h"
#include "dqm_archive/archiver/Exceptions.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

// all identifiers that we know of, iidentifiers not in
// this list wil cause exception when parsing template string.
const char* known_ids[] = {
    "run", "partition", "seq", "seq1", "timesec"
};

}

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace dqm_archive {

//----------------
// Constructors --
//----------------
FileNameFactoryTemplate::FileNameFactoryTemplate(const std::string& templateString,
                                                 const boost::filesystem::path& dir,
                                                 const std::string& partition,
                                                 unsigned run)
    : m_templateString(templateString)
    , m_dir(dir)
    , m_partition(partition)
    , m_run(run)
{
    // parse template string extract all identifiers
    std::regex const id_re("[{](\\w+)(:(\\d+))?[}]");

    auto ids_begin = 
        std::sregex_iterator(m_templateString.begin(), m_templateString.end(), id_re);
    auto ids_end = std::sregex_iterator();

    for (auto iter = ids_begin; iter != ids_end; ++iter) {
        auto&& match = *iter;

        std::string id = match[1].str();
        // check that id is known
        if (std::find(std::begin(known_ids), std::end(known_ids), id) == std::end(known_ids)) {
            throw errors::FileNameTemplateIdError(ERS_HERE, m_templateString, id);
        }

        unsigned begin = match[0].first - m_templateString.begin();
        unsigned end = match[0].second - m_templateString.begin();
        unsigned width = 0;
        if (match[2].matched) {
            width = std::stoi(match[3].str());
        }
        m_matches.push_back(IdMatch{id, begin, end, width});
    }   
}

/**
 *  @brief Make next file name.
 *
 *  @return File name to use for next DQM archive.
 */
boost::filesystem::path
FileNameFactoryTemplate::makeFileName()
{
    std::string result;
    std::string::size_type pos = 0;
    for (auto const& match: m_matches) {
        // output match prefix
        result.append(m_templateString, pos, match.begin-pos);
        // format id
        result.append(_formatMatch(match));
        pos = match.end;
    }
    // output remaining piece
    result += m_templateString.substr(pos);

    // increment counter
    m_seq ++;

    return m_dir / result;
}

std::string
FileNameFactoryTemplate::_formatMatch(const IdMatch& match) 
{
    std::string result;
    if (match.id == "partition") {
        result = m_partition;
    } else if (match.id == "run") {
        result = std::to_string(m_run);
    } else if (match.id == "seq") {
        result = std::to_string(m_seq);
    } else if (match.id == "seq1") {
        result = std::to_string(m_seq+1);
    } else if (match.id == "timesec") {
        time_t now = ::time(nullptr);
        result = std::to_string(now);
    }

    // pad with zeroes (probably does not make sense for partition but who cares)
    if (result.size() < match.width) {
        result.insert(0, match.width-result.size(), '0');
    }

    return result;
}

} // namespace dqm_archive
