//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "LTSArchiverCoCa.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/client/Register.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

/**
 *  @class RegisterIssues::IPCException
 *  @brief Class for issues generated for communication problems with CoCa IPC server.
 */
ERS_DECLARE_ISSUE_BASE (errors, StoreFailed, dqm_archive::LTSIssues::Exception,
        "file archiving failed for path: " << path, , ((std::string) path))

}

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace dqm_archive {

//----------------
// Constructors --
//----------------
LTSArchiverCoCa::LTSArchiverCoCa(const std::string& server,
        const std::string& partition,
        const std::string& dataset,
        int prio,
        unsigned dtcSec)
    : m_server(server)
    , m_partition(partition)
    , m_dataset(dataset)
    , m_prio(prio)
    , m_dtcSec(dtcSec)
{
}

//--------------
// Destructor --
//--------------
LTSArchiverCoCa::~LTSArchiverCoCa()
{
}

/**
 *  @brief Send one more file to storage
 *
 *  @param[in] path     Full path name of the file to be stored.
 *
 *  @except
 */
void
LTSArchiverCoCa::store(const std::string& path)
try {
    daq::coca::Register coca(m_server, m_partition);
    coca.registerFile(m_dataset, path, m_prio, m_dtcSec, std::string());
} catch (const ers::Issue& ex) {
    ::errors::StoreFailed err(ERS_HERE, path, ex);
    throw err;
}

} // namespace dqm_archive
