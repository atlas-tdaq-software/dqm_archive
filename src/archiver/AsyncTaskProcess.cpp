
//-----------------------
// This Class's Header --
//-----------------------
#include "dqm_archive/archiver/AsyncTaskProcess.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <iostream>
#include <signal.h>
#include <sys/wait.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dqm_archive/archiver/Exceptions.h"
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

/**
 *  Helper class to facilitate the construction of the string arguments
 *  from C++ constructs usable by system calls.
 */
class Vector2Exec {
public:
    Vector2Exec (const std::vector<std::string> &commandLine);
    ~Vector2Exec () throw ();
    const char *executable () const {return m_argv[0];}
    int argc() const {return m_argc;}
    char* const* argv() const {return m_argv;}
private:
    int m_argc;
    char** m_argv;
};

#ifndef ERS_NO_DEBUG

std::ostream&
operator<<(std::ostream& out, const Vector2Exec& v2e) {
    for(int i = 0 ; i != v2e.argc() ; ++ i ) {
        if (i) out << ' ';
        if (strchr(v2e.argv()[i], ' ') != 0) {
            out << "'" << v2e.argv()[i] << "'";
        } else {
            out << v2e.argv()[i];
        }
    }
    return out;
}

#endif // ERS_NO_DEBUG

} // namespace

/*
 * Vector2Exec
 */

::Vector2Exec::Vector2Exec (const std::vector<std::string> &commandLine)
    : m_argc(int(commandLine.size()))
    , m_argv(0)
{
    ERS_PRECONDITION(not commandLine.empty());

    typedef char * str_t;
    m_argv = new str_t[m_argc + 1];

    for (int k = 0; k != m_argc; ++ k) {
        m_argv[k] = new char[commandLine[k].size() + 1];
        strcpy (m_argv[k], commandLine[k].c_str());
    }
    m_argv[m_argc] = 0;
}

::Vector2Exec::~Vector2Exec ()
    throw ()
{
    for (int k = 0; k < m_argc; k ++) {
        delete[] m_argv[k];
    }
    delete[] m_argv;
}


//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------

namespace dqm_archive {

AsyncTaskProcess::AsyncTaskProcess(const std::vector<std::string>& commandLine,
                     const std::chrono::milliseconds& pollTime,
                     const std::string& name,
                     bool background)
    : m_pollTime(pollTime)
    , m_name(name)
    , m_background (background)
{
    ERS_PRECONDITION(not commandLine.empty());

    ::Vector2Exec l2e (commandLine);
    ERS_DEBUG (3, "Going to exec:" << l2e);
    m_childPid = fork();
    if (m_childPid == 0) {
        // this is the child
        /*
          Warning: info libc 'POSIX Threads' 'Threads and Fork'
          Basically: do not use anything, especially any mutex (no ERS!)
          "exec" something as soon as possible :-)
        */
        if (m_background) {
            close (0);
        }
        execvp (l2e.executable(), l2e.argv());
        // Got here? Error!!
        std::string fatal ("echo \"pid[$$]: FATAL: cannot execute: ");
        fatal = fatal + l2e.executable() + "; error: " + strerror (errno)
            + "\"; false";
        execl ("/bin/sh", "sh", "-c", fatal.c_str(), "1>&2", NULL);
        // what? still here??
        abort ();
    } else if (m_childPid == -1) {
        // this is the server; the child was NOT created
        throw errors::ErrnoError(ERS_HERE, "fork");
    }
    // this is the server; the child was created successfully
    ERS_DEBUG (2, "pid [" << m_childPid << "]: " << l2e);
}

AsyncTaskProcess::~AsyncTaskProcess()
{
    if (isRunning() && ! m_background) {
        terminate ();
    }
}

std::string
AsyncTaskProcess::name() const
{
    return m_name;
}

bool 
AsyncTaskProcess::isRunning()
{
    if (m_childPid != 0) {
        // it may have finished by now
        pid_t tmp = waitpid(m_childPid, &m_status, WNOHANG);
        if (tmp == -1) {
            // failure, send a warning but presume it is still running
            ers::warning(errors::ErrnoError(ERS_HERE, "waitpid"));
        } else if (tmp == m_childPid) {
            // yep, it's done
            m_childPid = 0;
        }
    }
    return m_childPid != 0;
}

AsyncTask::TaskStatus
AsyncTaskProcess::wait()
{
    while (m_childPid != 0) {
        // it may have finished by now
        pid_t tmp = waitpid(m_childPid, &m_status, 0);
        if (tmp == -1) {
            if (errno == ECHILD) {
                // child does not exist?
                m_childPid = 0;
            } else if (errno == EINTR) {
                // signal cought, just retry
            } else {
                // failure, send a warning but presume it is still running
                ers::warning(errors::ErrnoError(ERS_HERE, "waitpid"));
            }
        } else if (tmp == m_childPid) {
            // yep, it's done
            m_childPid = 0;
        }
    }
    return m_status == 0 ? TaskStatus::Success : TaskStatus::Failure;
}

AsyncTask::WaitStatus
AsyncTaskProcess::waitFor(std::chrono::milliseconds timeout)
{
    auto timePoint = std::chrono::steady_clock::now() + timeout;
    return waitUntil(timePoint);
}

AsyncTask::WaitStatus
AsyncTaskProcess::waitUntil(std::chrono::steady_clock::time_point timePoint)
{
    if (m_childPid == 0) {
        //aready dead
        return m_status == 0 ? WaitStatus::Success : WaitStatus::Failure;
    }

    while (true) {

        pid_t tmp = waitpid(m_childPid, &m_status, WNOHANG);
        if (tmp == -1) {
            ers::warning(errors::ErrnoError(ERS_HERE, "waitpid"));
        } else if (tmp == m_childPid) {
            // stopped
            m_childPid = 0;
            return m_status == 0 ? WaitStatus::Success : WaitStatus::Failure;
        }

        auto now = std::chrono::steady_clock::now();
        if (now >= timePoint) {
            break;
        }
        tsleep(m_pollTime);
    }
    // still alive here
    return WaitStatus::Timeout;
}

void
AsyncTaskProcess::kill(int signum)
{
    ERS_DEBUG(3, "sending signal " << signum  << " (" << ::strsignal(signum)
              << ") to process: " << m_childPid);
    int tmp = ::kill(m_childPid, signum);
    if (tmp != 0) {
        ers::warning(errors::ErrnoError(ERS_HERE, "kill"));
    }
}

void
AsyncTaskProcess::terminate()
{
    if (m_childPid == 0) return;

    for (int signum: {SIGTERM, SIGTERM, SIGKILL}) {
        pid_t tmp = waitpid(m_childPid, nullptr, WNOHANG);
        if (tmp == m_childPid) {
            // it has stopped
            m_childPid = 0;
            break;
        } else if (tmp == -1) {
            // waitpid failed?
            ers::warning(errors::ErrnoError(ERS_HERE, "waitpid"));
        } else {
            // still running
            this->kill(signum);
            auto res = waitFor(std::chrono::seconds(2));
            if (res != WaitStatus::Timeout) break;
        }
    }
}

// sleep for a PollTime period
void
AsyncTaskProcess::tsleep(const std::chrono::milliseconds& pollTime)
{
    struct timespec tsrem = {pollTime.count()/1000, (pollTime.count()%1000)*1000000};
    while (true) {
        int rc = nanosleep(&tsrem, &tsrem);
        // check if sleep was interrupted by signal
        if (not (rc == -1 and errno == EINTR)) break;
    }
}

} // namespace dqm_archive
