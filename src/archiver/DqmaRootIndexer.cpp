//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "dqm_archive/archiver/DqmaRootIndexer.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <ctime>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "TFile.h"
#include "TTree.h"
#include "dqm_archive/common/RTypes.h"
#include "dqm_archive/common/TTreeSequence.h"
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

ERS_DECLARE_ISSUE(errors, MissingTree,
    "failed to locate tree in a file: " << treename, ((std::string)treename))
ERS_DECLARE_ISSUE(errors, NotATree,
    "named object is not a TTree: " << treename, ((std::string)treename))
ERS_DECLARE_ISSUE(errors, NotEnoughMemory,
    "Not enough memory for indexing " << nResults << " results", ((size_t)nResults))


}

//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------

namespace dqm_archive {

//----------------
// Constructors --
//----------------
DqmaRootIndexer::DqmaRootIndexer(const std::string& resultTreeName,
                                 const std::string& indexTreeName,
                                 const std::string& statusTreeName)
    : m_resultTreeName(resultTreeName),
      m_indexTreeName(indexTreeName),
      m_statusTreeName(statusTreeName)
{
}

void
DqmaRootIndexer::index(TFile* file)
{
    // just enough to be in the file top directory
    file->cd();

    // find an input tree
    auto obj = file->Get(m_resultTreeName.c_str());
    if (not obj) throw ::errors::MissingTree(ERS_HERE, m_resultTreeName);
    auto resultTree = dynamic_cast<TTree*>(obj);
    if (not resultTree) throw ::errors::NotATree(ERS_HERE, m_resultTreeName);

    // make a sequence out of tree
    TTreeSequence<RTypes::ResultRepr> resultSeq(resultTree, "results");

    // counter for each <result ID, status> pair
    std::map<std::pair<RTypes::ParmId, RTypes::Status>, std::uint32_t> statusCount;

    struct timespec tp0;
    clock_gettime(CLOCK_MONOTONIC, &tp0);

    // scan results and build name index
    const size_t nResults = resultSeq.size();
    ERS_LOG("Reading " << nResults << " results for indexing");
    std::vector<RTypes::ResultIndex> resultIndex;
    try {
        resultIndex.resize(nResults);
    } catch (const std::bad_alloc& exc) {
        throw ::errors::NotEnoughMemory(ERS_HERE, nResults);
    }
    for (unsigned r = 0; r != nResults; ++ r) {
        const RTypes::ResultRepr& res = resultSeq[r];
        resultIndex[r].time_usec = res.time_usec;
        resultIndex[r].parm_id = res.parm_id;
        resultIndex[r].status = res.status;
        resultIndex[r].tag_index = res.tag_index;
        resultIndex[r].histo_index = res.histo_index;
        resultIndex[r].result_id = r;

        ++ statusCount[std::make_pair(res.parm_id, res.status)];
    }
    ERS_DEBUG(1, "DqmArchiverRoot::storeIndices: sorting results");
    std::sort(resultIndex.begin(), resultIndex.end());

    ERS_DEBUG(1, "DqmArchiverRoot::storeIndices: storing results");
    TTree* tree = new TTree(m_indexTreeName.c_str(), "Result index sorted by parm_id and timestamp");
    auto resultIndexSeq = TTreeSequenceFactory<RTypes::ResultIndex>::create(tree, "index", RTypes::ResultIndex::leafspec());
    std::copy(resultIndex.begin(), resultIndex.end(), std::back_inserter(resultIndexSeq));
    // update entry count in a tree and save
    tree->SetEntries();
    tree->Write();
    delete tree;

    // Store counters
    tree = new TTree(m_statusTreeName.c_str(), "List of all results and statuses with corresponding counters");
    auto statCountSeq = TTreeSequenceFactory<RTypes::StatusCounters>::create(tree, "counters", RTypes::StatusCounters::leafspec());
    for (const auto& counter: statusCount) {
        RTypes::StatusCounters counters{counter.first.first, counter.first.second, counter.second};
        statCountSeq.push_back(counters);
    }
    // update entry count in a tree and save
    tree->SetEntries();
    tree->Write();
    delete tree;

    struct timespec tp1;
    clock_gettime(CLOCK_MONOTONIC, &tp1);
    ERS_LOG("Done indexing results in " << (tp1.tv_sec - tp0.tv_sec) + (tp1.tv_nsec - tp0.tv_nsec) / 1e9
            << " seconds");
}

} // namespace dqm_archive
