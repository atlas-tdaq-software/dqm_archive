//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "dqm_archive/archiver/AsyncTaskThread.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

ERS_DECLARE_ISSUE(warnings, ThreadError, "Exception in std::thread",)

}

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace dqm_archive {

AsyncTaskThread::AsyncTaskThread(std::thread&& thread, std::future<bool>&& future, const std::string& name)
    : m_thread(std::move(thread)),
      m_future(std::move(future)),
      m_name(name)
{
}

AsyncTaskThread::~AsyncTaskThread()
{
    // if thread is still executing then detach it and let it finish (maybe)
    try {
        if (m_thread.joinable()) {
            m_thread.detach();
        }
    } catch (const std::exception& exc) {
        ers::warning(::warnings::ThreadError(ERS_HERE, exc));
    } catch (...) {
        ers::warning(::warnings::ThreadError(ERS_HERE));
    }
}

std::string
AsyncTaskThread::name() const
{
    return m_name;
}

bool
AsyncTaskThread::isRunning()
{
    if (m_status == WaitStatus::Timeout) {
        waitFor(std::chrono::milliseconds::zero());
    }
    return m_status == WaitStatus::Timeout;
}

AsyncTask::TaskStatus
AsyncTaskThread::wait()
{
    if (m_status == WaitStatus::Timeout) {
        bool ok = m_future.get();
        m_status = ok ? WaitStatus::Success : WaitStatus::Failure;
    }
    return m_status == WaitStatus::Success ? TaskStatus::Success : TaskStatus::Failure;
}

AsyncTask::WaitStatus
AsyncTaskThread::waitFor(std::chrono::milliseconds timeout)
{
    if (m_status == WaitStatus::Timeout) {
        auto status = m_future.wait_for(timeout);
        if (status == std::future_status::ready) {
            bool ok = m_future.get();
            m_status = ok ? WaitStatus::Success : WaitStatus::Failure;
        }
    }
    return m_status;
}

AsyncTask::WaitStatus
AsyncTaskThread::waitUntil(std::chrono::steady_clock::time_point timePoint)
{
    if (m_status == WaitStatus::Timeout) {
        auto status = m_future.wait_until(timePoint);
        if (status == std::future_status::ready) {
            bool ok = m_future.get();
            m_status = ok ? WaitStatus::Success : WaitStatus::Failure;
        }
    }
    return m_status;
}

void
AsyncTaskThread::setThread(std::thread&& thread, std::future<bool>&& future)
{
    m_thread = std::move(thread);
    m_future = std::move(future);
}

} // namespace dqm_archive
