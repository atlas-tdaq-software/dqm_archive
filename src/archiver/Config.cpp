//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "dqm_archive/archiver/Config.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <boost/filesystem.hpp>
#include <mutex>
#include <sstream>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "LTSArchiverCoCa.h"
#include "coca/common/FileSystem.h"
#include "coca/common/System.h"
#include "coca_dal/CoCaServer.h"
#include "dal/util.h"
#include "dqm_archive/archiver/DqmArchiverRoot.h"
#include "dqm_archive/archiver/FileNameFactoryTemplate.h"
#include "dqm_archive/dal/DQMAApplication.h"
#include "dqm_config/dal/DQParameter.h"
#include "dqm_config/dal/DQRegion.h"
#include "RunControl/Common/OnlineServices.h"
#include "TThread.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace fs = boost::filesystem;

namespace {

// This mutex will be used by everybody who needs to work with ROOT
// to synchronize access to thread-unsafe ROOT. It has to be a global
// object because writer thread may become detached.
std::mutex g_rootMutex;

ERS_DECLARE_ISSUE(errors, CannotMakeDir,
    "failed to create directory for output files: " << dirname, ((fs::path)dirname))
ERS_DECLARE_ISSUE(errors, ParamTreeError,
    "Failed to extract DQM parameter tree",)

typedef dqm_archive::Config::ParamHistMap ParamHistMap;

void
scanTree(std::map<std::string, std::string>& paramTree, const std::shared_ptr<ParamHistMap>& paramHist,
        const dqm_config::dal::DQRegion* region)
{

    // do the same for all sub-regions
    std::vector<const dqm_config::dal::DQRegion *> regions;
    region->get_all_regions(regions);
    for (const auto& subreg: regions) {
        paramTree[subreg->UID()] = region->UID();
        scanTree(paramTree, paramHist, subreg);
    }

    // and for sub-parameters
    std::vector<const dqm_config::dal::DQParameter *> parameters;
    region->get_all_parameters(parameters);
    for (const auto& param: parameters) {
        paramTree[param->UID()] = region->UID();
        if (paramHist) (*paramHist)[param->UID()] = param->get_InputDataSource();
    }
}

}

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace dqm_archive {

//----------------
// Constructors --
//----------------
Config::Config()
    : m_serverName()
    , m_queueSize(0)
    , m_archiveHist(false)
    , m_storageDir()
    , m_dataset()
    , m_cocaPartition()
    , m_cocaServerName()
    , m_cocaPriority(0)
    , m_cocaDTC(0)
    , m_updateInterval(0)
    , m_checkpointMinutes(0)
    , m_isWorkerThreads(0)
    , m_paramTree()
    , m_paramHist()
{
    auto& os = daq::rc::OnlineServices::instance();
    Configuration& config = os.getConfiguration();

    // get configuration parameters for application
    auto& app_base = os.getApplication();
    const dal::DQMAApplication *app_config = config.cast<dal::DQMAApplication>(&app_base);
    m_serverName = app_config->get_ServerName();
    m_queueSize = app_config->get_QueueSize();
    m_archiveHist = app_config->get_ArchiveHist();
    m_storageDir = app_config->get_StorageDir();
    m_dataset = app_config->get_CoCaDataset();
    m_cocaPartition = app_config->get_CoCaPartition();
    if (const daq::coca_dal::CoCaServer* cocasrv = app_config->get_CoCaServer()) {
        m_cocaServerName = cocasrv->UID();
    }
    m_cocaPriority = app_config->get_CoCaPriority();
    m_cocaDTC = app_config->get_CoCaDTC();
    m_updateInterval = app_config->get_UpdateInterval();
    m_checkpointMinutes = app_config->get_CheckpointMinutes();
    m_maxFileTimeIntervalSec = app_config->get_MaxFileTimeIntervalSec();
    m_maxFileResultCount = app_config->get_MaxFileResultCount();
    m_finalizeTimeoutSec = app_config->get_FinalizeTimeoutSec();
    m_fileNameTemplate = app_config->get_FileNameTemplate();
    m_isWorkerThreads = app_config->get_ISWorkerThreads();
    if (app_config->get_AddConfigHist()) m_paramHist = std::make_shared<ParamHistMap>();

    // Try to set unix properties, if it does not succeed (e.g. unix
    // group name is wrong) it means most likely configuration error.
    // In case of error just complain about it so that configuration
    // get fixed eventually, but continue. We still should be able to
    // produce output files in this case, though they may have
    // incorrect permissions, etc.
    try {
        const daq::coca_dal::UnixProperties* prop = app_config->get_UnixProperties();
        daq::coca::System::initialize(*prop);
    } catch (const daq::coca::SystemIssues::GroupNotFound &ex) {
        ers::error (ex);
    }

    // get DQM parameters configuration tree
    try {
        std::string dqmConfigName = app_config->get_DQMConfig();
        if (dqmConfigName.empty()) {
            readParamTree(config, os.getPartition());
        } else {
            // get it from separate config (which also likely has separate partition object)
            Configuration dqmConfig(dqmConfigName);
            auto& partition = os.getIPCPartition();
            const daq::core::Partition* partConfig = daq::core::get_partition(dqmConfig, partition.name(), 32);
            dqmConfig.register_converter(new daq::core::SubstituteVariables(*partConfig));
            readParamTree(dqmConfig, *partConfig);
        }
    } catch (const ers::Issue& ex) {
        ::errors::ParamTreeError err(ERS_HERE, ex);
        ers::warning(err);
    }
}

//--------------
// Destructor --
//--------------
Config::~Config()
{
}

void
Config::readParamTree(Configuration& config, const daq::core::Partition& partition)
{
    ERS_LOG("Reading DQM configuration from " << config.get_impl_spec());
    std::vector<const dqm_config::dal::DQRegion*> regions;
    dqm_config::dal::DQRegion::get_root_regions(config, partition, regions);
    // make a mapping from parameter name to its parent name
    for (const auto& region: regions) {
        m_paramTree[region->UID()] = std::string();
        scanTree(m_paramTree, m_paramHist, region);
    }
    ERS_LOG("Read configuration with " << regions.size() << " root regions and " << m_paramTree.size() << " parameters");
}

//  Create and return archiver manager instance for given run number
std::unique_ptr<DqmArchiveMgr>
Config::makeArchiver(unsigned run)
{
    // for now we only do ROOT

    // if could not find any parameters do not need to archive
    if (m_paramTree.empty()) {
        ERS_LOG("No DQM result archiving due to problems with DQM parameters configuration");
        return std::unique_ptr<DqmArchiveMgr>();
    }

    // create directory for output files if needed
    try {
        daq::coca::FileSystem::create_dirs(m_storageDir.string());
    } catch (const fs::filesystem_error &ex) {
        throw errors::CannotMakeDir(ERS_HERE, m_storageDir, ex);
    }

    // make LTS archiver instance
    std::shared_ptr<LTSArchiverCoCa> ltsArchiver;
    if (m_dataset.empty() or m_cocaServerName.empty() or m_cocaPartition.empty()) {
        ERS_LOG("No archiving to CoCa server");
    } else {
        ltsArchiver = std::make_shared<LTSArchiverCoCa>(m_cocaServerName, m_cocaPartition, m_dataset, m_cocaPriority, m_cocaDTC);
    }

    // Need to tell ROOT that we are running with more than one thread
    TThread::Initialize();

    auto& os = daq::rc::OnlineServices::instance();
    auto& partition = os.getIPCPartition();

    // make factory of output file names
    auto fnameFactory = std::make_unique<FileNameFactoryTemplate>(m_fileNameTemplate,
                                                                  m_storageDir, partition.name(), run);

    // instantiate archiver
    std::unique_ptr<IDqmArchiver> store(new DqmArchiverRoot(std::move(fnameFactory), ltsArchiver, m_paramTree,
                                                            m_checkpointMinutes, m_maxFileTimeIntervalSec,
                                                            m_maxFileResultCount, std::chrono::seconds(m_finalizeTimeoutSec),
                                                            g_rootMutex));

    // make instance of archiver manager
    std::unique_ptr<DqmArchiveMgr> archiver(new DqmArchiveMgr(partition, std::move(store), m_serverName,
            m_queueSize, m_archiveHist, m_updateInterval, m_isWorkerThreads, m_paramHist, g_rootMutex));
    return archiver;
}

} // namespace dqm_archive
