//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "dqm_archive/archiver/DqmaResultConverter.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <chrono>
#include <TObjArray.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "oh/OHRootReceiver.h"
#include "owl/semaphore.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace boost::posix_time;

namespace {

ERS_DECLARE_ISSUE(error, UnhandledException,
        "DqmaResultConverter: Unhandled '" << name << "' exception was thrown",
        ((const char *)name) )

ERS_DECLARE_ISSUE(warnings, SkipHistogramsWarning,
        "DqmaResultConverter: input result queue filling fast, will ignore some histograms.", )


}

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace dqm_archive {

//----------------
// Constructors --
//----------------
DqmaResultConverter::DqmaResultConverter(
    const std::shared_ptr<IsResultQueue>& inQueue,
    const std::shared_ptr<CoreResultQueue>& outQueue,
    const std::shared_ptr<DqmaRootReceiver>& rootReceiver
)
    : m_inQueue(inQueue)
    , m_outQueue(outQueue)
    , m_rootReceiver(rootReceiver)
{
}

// operator called by thread
void
DqmaResultConverter::operator()()
try {
    bool stop = false;
    IsResultQueue::cont_type input_results;
    while (not stop) {

        // get more data from a queue
        input_results.clear();
        m_inQueue->pop(input_results);

        ERS_DEBUG(2, "Received: " << input_results.size() << " results" );

        // check for EOD first
        auto it = std::remove_if(input_results.begin(), input_results.end(),
                [](const IsResultQueue::value_type& res) {return not res.second;});
        if (it != input_results.end()) {
            ERS_DEBUG(1, "Received: EOD");
            stop = true;
            input_results.erase(it, input_results.end());
        }

        _process_buffer(input_results);

    }

    // once we are done signal downstream guy to stop it too
    m_outQueue->push(std::make_pair(std::string(), std::shared_ptr<dqm_core::Result>()), true);

    ERS_LOG("Receive loop finished, finalizing archiving");

} catch (const ers::Issue& ex) {
    ers::error(ex);
} catch (const std::exception &ex) {
    ers::error(::error::UnhandledException(ERS_HERE, "standard", ex));
} catch (...) {
    ers::fatal(::error::UnhandledException(ERS_HERE, "unknown"));
}

// operator called by thread
void
DqmaResultConverter::_process_buffer(IsResultQueue::cont_type const& input_results)
{
    bool skip_histograms = false;
    for (auto& pair: input_results) {

        const auto& is_result = pair.second;

        ptime time = from_time_t(is_result->time().c_time()) + microseconds(is_result->time().mksec());
        TObject* tobj = nullptr;
        if (not is_result->objects.empty()) {
            // check the status of input queue, if it gets too full then stop reading histograms.
            if (not skip_histograms and m_inQueue->usage() > 0.95) {
                ers::warning(::warnings::SkipHistogramsWarning(ERS_HERE));
                skip_histograms = true;
            }
            if (not skip_histograms) {
                tobj = m_rootReceiver->get_histograms(is_result->objects).release();
            }
        }

        // make dqm_core::Result out of all this stuff
        auto result = std::make_shared<dqm_core::Result>(dqm_core::Result::Status(is_result->status),
                tobj, time);
        for (const auto& tag: is_result->tags) {
            result->tags_.insert(std::make_pair(tag.name, tag.value));
        }
        m_outQueue->push(std::make_pair(pair.first, result));

    }
}

} // namespace dqm_archive
