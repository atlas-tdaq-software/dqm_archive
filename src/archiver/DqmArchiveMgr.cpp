//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "dqm_archive/archiver/DqmArchiveMgr.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dqm_archive/archiver/ResultWriterThread.h"
#include "dqm_archive/archiver/DqmaResultConverter.h"
#include "dqm_archive/archiver/DqmaRootReceiver.h"
#include "ers/ers.h"
#include "owl/semaphore.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

    ERS_DECLARE_ISSUE(error, ReceiverStopError, "exception when trying to stop receiver", )

    OWLSemaphore g_semaphore;

}

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace dqm_archive {

//----------------
// Constructors --
//----------------
DqmArchiveMgr::DqmArchiveMgr(const IPCPartition& partition,
        std::unique_ptr<IDqmArchiver> archiver,
        const std::string& serverName,
        unsigned queueSize,
        bool archiveHist,
        unsigned updateInterval,
        unsigned isWorkerThreads,
        const std::shared_ptr<ParamHistMap>& paramHist,
        std::mutex& rootMutex)
    : m_partition(partition)
    , m_archiver(std::move(archiver))
    , m_serverName(serverName)
    , m_queueSize(queueSize)
    , m_archiveHist(archiveHist)
    , m_updateInterval(updateInterval)
    , m_isWorkerThreads(isWorkerThreads)
    , m_paramHist(paramHist)
    , m_rootMutex(rootMutex)
    , m_isQueue()
    , m_coreQueue()
    , m_writerThread()
    , m_cvtThread()
    , m_recv()
{
}

//--------------
// Destructor --
//--------------
DqmArchiveMgr::~DqmArchiveMgr()
{
}

/**
 *  Start archiving, can only be called once
 */
void
DqmArchiveMgr::start()
{
    // make a queue
    m_isQueue = std::make_shared<IsResultQueue>(m_queueSize);
    // make output queue slightly bigger to avoid potential weird aliasing
    m_coreQueue = std::make_shared<CoreResultQueue>(m_queueSize + m_queueSize/10);

    // start writer thread
    ERS_LOG("Starting writer thread...");
    m_writerThread = std::thread(ResultWriterThread(std::move(m_archiver), m_coreQueue, g_semaphore));

    // start converter thread
    ERS_LOG("Starting converter thread...");
    std::shared_ptr<DqmaRootReceiver> rootReceiver = 
        std::make_shared<DqmaDefaultRootReceiver>(m_partition, m_rootMutex);
    m_cvtThread = std::thread(DqmaResultConverter(m_isQueue, m_coreQueue, rootReceiver));

    // instantiate receiver and tell it to start running in background
    ERS_LOG("Starting receiver...");
    m_recv.reset(new DqmaResultReceiver(m_partition, m_serverName, m_isQueue, m_archiveHist,
            m_updateInterval, m_isWorkerThreads, m_paramHist));
    m_recv->start();
}

/**
 *  Stop archiving, can only be called once (after start())
 */
void
DqmArchiveMgr::stop(bool detach)
{
    // stop receiver, guaranteed no new data after this method finishes
    ERS_LOG("Stopping receiver...");
    try {
        // have to catch any exception because threads need to be either
        // joined or detached
        m_recv->stop();
    } catch (const std::exception &ex) {
        ers::error(::error::ReceiverStopError(ERS_HERE, ex));
    }
    ERS_LOG("Receiver has stopped");

    // send special beacon down the line to say that we are done
    m_isQueue->push(std::make_pair(std::string(), std::shared_ptr<dqmf::is::Result>()), true);

    // get rid of queues and receiver
    m_isQueue.reset();
    m_coreQueue.reset();
    m_recv.reset();

    if (detach) {
        // detach the threads an let it finish its work
        ERS_LOG("Detaching threads and let them finish their work in background");
        m_writerThread.detach();
        m_cvtThread.detach();
    } else {
        ERS_LOG("Joining threads and waiting until they complete job...");
        m_cvtThread.join();
        ERS_LOG("Converter thread has completed");
        m_writerThread.join();
        ERS_LOG("Writer thread has completed");
    }
}

} // namespace dqm_archive
