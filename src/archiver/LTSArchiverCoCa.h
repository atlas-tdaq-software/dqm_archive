#ifndef DQM_ARCHIVE_LTSARCHIVERCOCA_H
#define DQM_ARCHIVE_LTSARCHIVERCOCA_H

//-----------------
// C/C++ Headers --
//-----------------
#include <string>

//----------------------
// Base Class Headers --
//----------------------
#include "dqm_archive/archiver/ILTSArchiver.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace dqm_archive {

/// @addtogroup dqm_archive

/**
 *  @ingroup dqm_archive
 *
 *  @brief Class implementing Long-Term Storage archiver for storage in CoCa.
 */

class LTSArchiverCoCa : public ILTSArchiver {
public:

    /**
     *  Constructor takes a bunch of parameters controlling where the files should go.
     *
     *  @param[in] server     CoCa server instance name
     *  @param[in] partition  Partition name where CoCa server runs
     *  @param[in] dataset    Name of the CoCa dataset to store the file in
     *  @param[in] prio       Priority value, accepted range is 0-100
     *  @param[in] dtcSec     Desired Time in Cache in seconds.
     */
    LTSArchiverCoCa(const std::string& server,
            const std::string& partition,
            const std::string& dataset,
            int prio = 0,
            unsigned dtcSec = 600);
    
    // Destructor
    ~LTSArchiverCoCa() override;

    /**
     *  @brief Send one more file to storage
     *
     *  @param[in] path     Full path name of the file to be stored.
     *
     *  @except LTSIssues::Exception if archiving failed
     */
    void store(const std::string& path) override;

protected:

private:

    const std::string m_server;
    const std::string m_partition;
    const std::string m_dataset;
    const int m_prio;
    const unsigned m_dtcSec;
};

} // namespace dqm_archive

#endif // DQM_ARCHIVE_LTSARCHIVERCOCA_H
