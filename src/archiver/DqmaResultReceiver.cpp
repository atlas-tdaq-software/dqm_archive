//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "dqm_archive/archiver/DqmaResultReceiver.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>
#include <set>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dqm_core/Result.h"
#include "dqmf/is/Result.h"
#include "ipc/partition.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace boost::posix_time;

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace dqm_archive {

//----------------
// Constructors --
//----------------
DqmaResultReceiver::DqmaResultReceiver(const IPCPartition& partition,
    const std::string& server,
    const std::shared_ptr<IsResultQueue>& queue,
    bool archiveHist,
    unsigned updateInterval,
    unsigned isWorkerThreads,
    const std::shared_ptr<ParamHistMap>& paramHist)
    : ISInfoReceiver(partition, isWorkerThreads)
    , m_server(server)
    , m_queue(queue)
    , m_archiveHist(archiveHist)
    , m_updateInterval(updateInterval)
    , m_paramHist(paramHist)
    , m_criteria(".*", dqmf::is::Result::type())
    , m_subscribed(false)
    , m_prevResult()
    , m_counter(0)
    , m_skipped(0)
{
}

//--------------
// Destructor --
//--------------
DqmaResultReceiver::~DqmaResultReceiver()
{
    if (m_subscribed) {
        try {
            unsubscribe(m_server, m_criteria, true);
        } catch (std::exception const& exc) {
            ERS_LOG("DqmaResultReceiver: unsubscribe failed: " << exc.what());
        }
    }
}

// subscribe to data from a server
void
DqmaResultReceiver::start()
{
    ERS_LOG("DqmaResultReceiver: subscribing to server " << m_server);
    // subscribe to all DQMF results for that server
    scheduleSubscription(m_server, m_criteria, &DqmaResultReceiver::callback, this);
    m_subscribed = true;
    m_counter = 0;
    m_skipped = 0;
}

// stop subscription
void
DqmaResultReceiver::stop()
{
    // unsubscribe, wait for completion, so that callback is not
    // called anymore after we return from this method
    unsubscribe(m_server, m_criteria, true);
    m_subscribed = false;
    m_prevResult.clear();
    ERS_LOG("DqmaResultReceiver: unsubscribed from server " << m_server);
    ERS_LOG("Totals: results=" << m_counter << " skipped=" << m_skipped);
}

// callback method called on every update of IS objects,
// pay attention, this may be called from many threads simultaneously
void
DqmaResultReceiver::callback(ISCallbackInfo* cb)
{
    if (cb->reason() != ISInfo::Deleted) {
        try {
            ++ m_counter;

            auto is_result = std::make_shared<dqmf::is::Result>();
            cb->value(*is_result);

            const std::string name = cb->objectName();

            ERS_DEBUG(3, "Received: " << name << " info.time=" << is_result->time());

            // stuff below needs to be synchronized
            std::unique_lock<std::mutex> qlock(m_callbackMutex);

            // if has not changed then ignore
            LastResult newres(*is_result);
            auto it = m_prevResult.find(name);
            if (it != m_prevResult.end()) {
                if (not it->second.changed(newres, m_updateInterval)) {
                    ++ m_skipped;
                    return;
                }
            }

            // check if we need to load histograms for this result
            bool archiveHistos = m_archiveHist and needHistos(name, is_result->status);
            if (archiveHistos) {
                std::set<std::string> objects(is_result->objects.begin(), is_result->objects.end());
                if (m_paramHist) {
                    // extend result objects list with config object list
                    const std::vector<std::string>& cfgObjects = (*m_paramHist)[name];
                    objects.insert(cfgObjects.begin(), cfgObjects.end());
                }
                is_result->objects.assign(objects.begin(), objects.end());
            } else {
                // clear all objects so that converter does not try to read them
                is_result->objects.clear();
            }

            m_queue->push(std::make_pair(name, is_result));

            // remember it
            m_prevResult[name] = newres;

        } catch ( daq::is::Exception & ex ) {
            ERS_LOG( "Parsing " << cb->objectName() << " IS Info failed: " << ex );
        }
    }
}

// return true if histograms need to be archived for the result
bool
DqmaResultReceiver::needHistos(const std::string& param, dqmf::is::Result::Status newStatus)
{
    // We want to store histograms for all transitions except for transition to Disabled or from Disabled to
    // Undefined or Green. Additionally if very first histogram is Undefined or Green do not record it as well.

    dqmf::is::Result::Status oldStatus = dqmf::is::Result::Disabled;
    auto it = m_prevResult.find(param);
    if (it != m_prevResult.end()) {
        oldStatus = it->second.status();
    }

    // archive only when status changes and when new status is not Disabled
    if (newStatus == oldStatus) return false;
    if (newStatus == dqmf::is::Result::Disabled) return false;
    if (oldStatus == dqmf::is::Result::Disabled) {
        if (newStatus == dqmf::is::Result::Undefined or newStatus == dqmf::is::Result::Green) return false;
    }
    return true;
}

DqmaResultReceiver::LastResult::LastResult(const dqmf::is::Result& isResult)
    : m_time()
    , m_status(isResult.status)
    , m_tags()
{
    // convert time to boost
    m_time = from_time_t(isResult.time().c_time()) + microseconds(isResult.time().mksec());

    // copy and sort tags
    m_tags.reserve(isResult.tags.size());
    for (const auto& tag: isResult.tags) {
        m_tags.push_back(std::make_pair(tag.name, tag.value));
    }
    std::sort(m_tags.begin(), m_tags.end());
}

bool
DqmaResultReceiver::LastResult::changed(const LastResult& other, unsigned delta) const
{
    // compare time
    boost::posix_time::time_duration d = other.m_time - m_time;
    if (d.total_seconds() >= long(delta)) return true;

    // compare status and tags
    return other.m_status != m_status or other.m_tags != m_tags;
}

} // namespace dqm_archive
