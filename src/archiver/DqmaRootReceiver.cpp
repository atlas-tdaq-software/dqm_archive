//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "dqm_archive/archiver/DqmaRootReceiver.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <TObjArray.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "oh/OHRootReceiver.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------


//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace dqm_archive {

//----------------
// Constructors --
//----------------
DqmaDefaultRootReceiver::DqmaDefaultRootReceiver(const IPCPartition& partition, std::mutex& rootMutex)
    : m_partition(partition)
    , m_rootMutex(rootMutex)
{
}

std::unique_ptr<TObjArray>
DqmaDefaultRootReceiver::get_histograms(std::vector<std::string> const& objects)
{
    // We are going to mess with ROOT so acquire the mutex.
    std::lock_guard<std::mutex> lock(m_rootMutex);

    std::unique_ptr<TObjArray> array(new TObjArray(objects.size()));
    array->SetOwner(kTRUE);

    unsigned pos = 0;
    for (const auto& name: objects) {
        try {
            OHRootObject object(OHRootReceiver::getRootObject(m_partition, name));
            array->AddAt(object.object.release(), pos++);
            ERS_DEBUG(2, "Received histogram: " << name);
        } catch(daq::oh::Exception& ex) {
            ers::log( ex );
        }
    }

    // may need to resize it
    if (pos == 0) {
        return nullptr;
    } else if (pos != objects.size()) {
        array->SetLast(pos - 1);
    }
    return array;
}

} // namespace dqm_archive
