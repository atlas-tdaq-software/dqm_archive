//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "dqm_archive/archiver/DqmArchiverRoot.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <algorithm>
#include <boost/lexical_cast.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "TFile.h"
#include "TH1.h"
#include "TTree.h"
#include "AsyncFinalizer.h"
#include "dqm_archive/archiver/AsyncTaskList.h"
#include "dqm_archive/archiver/DqmaRootIndexer.h"
#include "dqm_archive/archiver/IFileNameFactory.h"
#include "dqm_archive/common/RootSerializer.h"
#include "dqm_config/dal/DQParameter.h"
#include "dqm_config/dal/DQRegion.h"
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace std::chrono;

namespace {

// parameters for buffer size, buffer grows until it reaches maxBufferSize,
// then initial part of it is sent to file to reach minBufferSize
const unsigned minBufferSize = 10000;
const unsigned maxBufferSize = 11000;

typedef std::pair<std::string, std::shared_ptr<dqm_core::Result>> ResultPair;

bool compareResults(const ResultPair& lhs, const ResultPair& rhs) {
    // order by time first and name second
    return std::tie(lhs.second->universal_time_, lhs.first) < std::tie(rhs.second->universal_time_, rhs.first);
}

// Send specified file to long term storage
bool sendToLTS(std::string const& path, std::shared_ptr<dqm_archive::ILTSArchiver> const& ltsArchiver)
{
    try {
        // send it to coca
        if (ltsArchiver != nullptr) {
            ERS_LOG("Sending output file to long-term storage");
            ltsArchiver->store(path);
        }
    } catch (const ers::Issue& ex) {
        // report an error but continue, someone needs to take care later
        ERS_LOG(ex);
        ers::error(ex);
        return false;
    }
    return true;
}

}


//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace dqm_archive {

// Opens a file and creates all trees
DqmArchiverRoot::FileContext::FileContext(boost::filesystem::path const& path_) :
    path(path_)
{
    // create a file
    file.reset(TFile::Open(path.string().c_str(), "RECREATE", "DQM Results Archive"));

    // create all trees/branches
    resultTree = new TTree("Results", "List of all results ordered by time");
    resultSeq = TTreeSequenceFactory<RTypes::ResultRepr>::create(resultTree, "results", RTypes::ResultRepr::leafspec(), 128*1024);

    tagNameTree = new TTree("TagNames", "List of all tag names");
    tagNameSeq = TTreeSequenceFactory<TString*>::create(tagNameTree, "name");

    tagTree = new TTree("Tags", "List of all tag values");
    tagValueSeq = TTreeSequenceFactory<RTypes::TagValueRepr>::create(tagTree, "tags", RTypes::TagValueRepr::leafspec(), 128*1024);

    histoTree = new TTree("Histograms", "Serialized histograms");
    histoIdSeq = TTreeSequenceFactory<RTypes::HistoIdRepr>::create(histoTree, "histo_id", RTypes::HistoIdRepr::leafspec());
    histoSeq = TTreeSequenceFactory<TString*>::create(histoTree, "histo_blob", 128*1024);
}

// set trees' entries and flush them to the file
void DqmArchiverRoot::FileContext::flush()
{
    // make sure we are in the right ROOT directory, this is not very thread-safe
    file->cd();

    // order is important in case we crash between Write()s
    TTree* trees[] = {tagNameTree, tagTree, histoTree, resultTree};
    for (TTree* tree: trees) {
        // update entry counts in trees, not really necessary but
        // to avoid possible misinterpretation
        tree->SetEntries();
        // save existing trees in case we get killed before closing the file
        tree->Write(nullptr, TObject::kWriteDelete);
    }
}


//----------------
// Constructors --
//----------------
DqmArchiverRoot::DqmArchiverRoot(std::unique_ptr<IFileNameFactory> fileNameFactory,
                std::shared_ptr<ILTSArchiver> ltsArchiver,
                const std::map<std::string, std::string>& paramTree,
                unsigned checkpointMinutes,
                unsigned maxFileTimeIntervalSec,
                unsigned maxFileResultCount,
                std::chrono::milliseconds finalizeTimeout,
                std::mutex& rootMutex)
    : IDqmArchiver()
    , m_fileNameFactory(std::move(fileNameFactory))
    , m_ltsArchiver(std::move(ltsArchiver))
    , m_checkpointMinutes(checkpointMinutes)
    , m_maxFileTimeIntervalSec(maxFileTimeIntervalSec)
    , m_maxFileResultCount(maxFileResultCount)
    , m_finalizeTimeout(finalizeTimeout)
    , m_rootMutex(rootMutex)
    , m_lastStored(boost::posix_time::from_time_t(time_t(0)))
    , m_finalizers(std::make_shared<AsyncTaskList>("finalizers"))
{
    // assign ID to each parameter
    unsigned counter = 0;
    for (const auto& pair: paramTree) {
        m_parm2id[pair.first] = counter ++;
    }

    // get the ID of a parent for each parameter
    m_parm2parentId.resize(m_parm2id.size());
    for (const auto& pair: paramTree) {
        int parentId = -1;
        auto nit = m_parm2id.find(pair.second);
        if (nit != m_parm2id.end()) parentId = nit->second;

        m_parm2parentId[m_parm2id[pair.first]] = parentId;
    }

    m_fileContext = openNextFile();
}

//--------------
// Destructor --
//--------------
DqmArchiverRoot::~DqmArchiverRoot()
{
    try {
        // try to close but suppress any exceptions
        // with background finalization condition check will need update
        if (m_fileContext != nullptr) {
            finish();
        }
    } catch (...) {
    }
}

std::unique_ptr<DqmArchiverRoot::FileContext>
DqmArchiverRoot::openNextFile()
{
    auto path = m_fileNameFactory->makeFileName();
    ERS_LOG("Opening output file " << path);

    auto fcontext = std::make_unique<FileContext>(path);

    // save parameter tree
    storeConfig(*fcontext);

    return fcontext;
}

// Store parameters configuration.
void
DqmArchiverRoot::storeConfig(FileContext& fcontext)
{
    // protect from other threads
    std::lock_guard<std::mutex> lock(m_rootMutex);

    // make sure we are in the right ROOT directory
    fcontext.file->cd();

    TTree* parmTree = new TTree("Parameters", "List of DQParameters, their names and parent relations.");
    auto parmSeq = TTreeSequenceFactory<TString*>::create(parmTree, "name", 128*1024);

    // store parameter names in a tree ordered by name
    for (auto& pair: m_parm2id) {
        TString str(pair.first.c_str());
        parmSeq.push_back(&str);
    }

    // store parents in different branch
    auto parentSeq = TTreeSequenceFactory<std::int32_t>::create(parmTree, "parent", "parent_id/I", 128*1024);
    for (const auto& parent_id: m_parm2parentId) {
        parentSeq.push_back(parent_id);
    }

    parmTree->SetEntries();
    parmTree->Write();
    delete parmTree;

    // we also want to produce set of pairs (parent, child) sorted by parent Id
    std::vector<RTypes::ParamTreeRepr> paren2child;
    paren2child.reserve(m_parm2parentId.size());
    unsigned counter = 0;
    for (const auto& parent_id: m_parm2parentId) {
        paren2child.push_back(RTypes::ParamTreeRepr{parent_id, counter++});
    }
    std::sort(paren2child.begin(), paren2child.end());

    // new tree for tree
    parmTree = new TTree("ParmTree", "Parameter ID pairs (parent, child) ordered by (parent, child).");
    auto parmTreeSeq = TTreeSequenceFactory<RTypes::ParamTreeRepr>::create(parmTree, "param_tree",
                                                                           RTypes::ParamTreeRepr::leafspec(),
                                                                           128*1024);
    std::copy(paren2child.begin(), paren2child.end(), std::back_inserter(parmTreeSeq));

    parmTree->SetEntries();
    parmTree->Write();
    delete parmTree;
}


bool
DqmArchiverRoot::store(const std::string& name, const std::shared_ptr<dqm_core::Result>& result)
{
    ++ m_counter;
    if (m_counter % 100000 == 0) {
        ERS_LOG("Results received=" << m_counter << " dropped=" << m_dropped);
    }

    // check the timestamp
    if (result->universal_time_ < m_lastStored) {
        // this is earlier that the data in a file already
        ++ m_dropped;
        return false;
    }

    // insert it into a buffer
    ResultPair res(name, result);
    auto it = std::upper_bound(m_buffer.begin(), m_buffer.end(), res, ::compareResults);
    m_buffer.insert(it, res);

    // if buffer is full flush some of it
    if (m_buffer.size() >= ::maxBufferSize) {
        {
            // protect from other threads
            std::lock_guard<std::mutex> lock(m_rootMutex);
            flush(m_buffer.size() - ::minBufferSize);
        }

        // maybe it's time to switch to a new file
        if (decideSwitch()) {
            {
                // protect from other threads
                std::lock_guard<std::mutex> lock(m_rootMutex);
                // save existing data to file in case we crash while indexing
                m_fileContext->flush();
            }

            // start finalization for this file in async mode
            finalize(true);

            // open new file
            m_fileContext = openNextFile();
        }
    }

    return true;
}

// Finish archiving, close all output files, etc.
void
DqmArchiverRoot::finish()
{
    ERS_LOG("Start finalizing output");
    ERS_LOG("Total results received=" << m_counter << " dropped=" << m_dropped);

    {
        // protect from other threads
        std::lock_guard<std::mutex> lock(m_rootMutex);

        // store remaining stuff in file
        flush(m_buffer.size());
        ERS_LOG("Stored total " << m_statResults << " results, " << m_statHistos << " histos, "
                << m_statTags << " tags");

        // save existing data to file in case we crash while indexing
        m_fileContext->flush();
    }

    finalize(true);

    // TODO: Here we wait indefinitely until finalization stops, it would be better
    // to use timed wait and return before finalization is done.
    if (m_finalizeTimeout.count() == 0) {
        ERS_DEBUG(0, "Wait until finalization completes");
        m_finalizers->wait();
    } else {
        ERS_DEBUG(0, "Wait until finalization completes, max " << m_finalizeTimeout.count() << " msec");
        m_finalizers->waitFor(m_finalizeTimeout);
    }
}

void
DqmArchiverRoot::finalize(bool async) {
    auto fcontext = std::move(m_fileContext);
    m_fileContext.reset();

    if (async) {

        // run finalization in a thread, finalization is done by separate process
        // to avoid locking for extended period

        ERS_LOG("Closing output file " << fcontext->path);
        fcontext->file->Close();
        fcontext->file.reset();

        std::chrono::milliseconds indexingTimeout = std::chrono::minutes(5);
        auto finalizer = std::make_shared<AsyncFinalizer>(fcontext->path.string(), m_ltsArchiver, indexingTimeout, "last_finalizer");
        m_finalizers->addTask(finalizer);

    } else {

        // access to ROOT has to be protected
        std::lock_guard<std::mutex> lock(m_rootMutex);

        dqm_archive::DqmaRootIndexer indexer("Results", "ResultIndex", "StatusCounters");
        try {
            indexer.index(fcontext->file.get());
        } catch (const ers::Issue& ex) {
            ERS_LOG(ex);
            ers::error(ex);
            return;
        }

        ERS_LOG("Closing output file " << fcontext->path);
        fcontext->file->Close();
        fcontext->file.reset();

        sendToLTS(fcontext->path.string(), m_ltsArchiver);
    }
}


// write count of results from buffer into file
void
DqmArchiverRoot::flush(unsigned count)
{
    ERS_DEBUG(1, "DqmArchiverRoot::flush: count=" << count);

    unsigned nResults = 0;
    unsigned nHistos = 0;
    unsigned nTags = 0;
    struct timespec tp0;
    clock_gettime(CLOCK_MONOTONIC, &tp0);

    // save first count results
    for (auto it = m_buffer.begin(); it != m_buffer.begin()+count; ++ it) {

        const ResultPair& resp = *it;

        // need ID for parameter name
        unsigned parm_id = 0;
        auto nit = m_parm2id.find(resp.first);
        if (nit == m_parm2id.end()) {
            ERS_LOG("DqmArchiverRoot::flush -- unexpected result name: " << resp.first);
            continue;
        }
        parm_id = nit->second;

        // store result
        const dqm_core::Result& res = *resp.second;
        boost::posix_time::time_duration since_epoch = res.universal_time_ -
                boost::posix_time::from_time_t(time_t(0));

        // store result
        unsigned res_id = m_fileContext->resultSeq.size();

        // store histograms if there are any
        std::int32_t histoIndex = -1;
        if (TObject* tobj = res.getObject()) {

            RootSerializer serializer;

            // find all TH1 objects, the object itself may be a TH1 or TObjArray
            if (tobj->InheritsFrom(TObjArray::Class())) {
                TObjArrayIter it(static_cast<const TObjArray*>(tobj));
                while (TObject* obj = it.Next()) {
                    if (obj->InheritsFrom(TH1::Class())) {
                        if (histoIndex < 0) histoIndex = m_fileContext->histoIdSeq.size();
                        TString histdata = serializer.serialize(obj);
                        m_fileContext->histoSeq.push_back(&histdata);
                        m_fileContext->histoIdSeq.push_back(RTypes::HistoIdRepr{since_epoch.total_microseconds(), parm_id, res_id});
                        ++ nHistos;
                        ++ m_statHistos;
                    }
                }
            } else if (tobj->InheritsFrom(TH1::Class())) {
                if (histoIndex < 0) histoIndex = m_fileContext->histoIdSeq.size();
                TString histdata = serializer.serialize(tobj);
                m_fileContext->histoSeq.push_back(&histdata);
                m_fileContext->histoIdSeq.push_back(RTypes::HistoIdRepr{since_epoch.total_microseconds(), parm_id, res_id});
                ++ nHistos;
                ++ m_statHistos;
            }

        }

        RTypes::Timestamp timestamp = since_epoch.total_microseconds();
        int tagIndex = res.tags_.empty() ? -1 : int(m_fileContext->tagValueSeq.size());
        m_fileContext->resultSeq.push_back(RTypes::ResultRepr{timestamp, parm_id, res.status_, tagIndex, histoIndex});
        ++ nResults;

        // update per-file stats
        if (m_fileContext->firstTimeStamp == 0) m_fileContext->firstTimeStamp = timestamp;
        m_fileContext->lastTimeStamp = timestamp;
        ++ m_fileContext->resultCount;

        // store all tags
        for (const auto& tag: res.tags_) {

            // need ID for tag name
            unsigned tag_id = 0;
            auto&& tag2id = m_fileContext->tag2id;
            auto nit = tag2id.find(tag.first);
            if (nit == tag2id.end()) {
                tag_id = tag2id.size();
                tag2id.insert(std::make_pair(tag.first, tag_id));

                // Store it in the tree
                TString str(tag.first.c_str());
                m_fileContext->tagNameSeq.push_back(&str);
            } else {
                // use existing mapping
                tag_id = nit->second;
            }

            m_fileContext->tagValueSeq.push_back(RTypes::TagValueRepr{res_id, tag_id, tag.second});
            ++ nTags;
            ++ m_statTags;

        }

        ++ m_statResults;
        if (m_statResults % 100000 == 0) {
            ERS_LOG("Stored " << m_statResults << " results, " << m_statHistos << " histos, "
                    << m_statTags << " tags");
        }

        // remember last timestamp
        m_lastStored = res.universal_time_;

    }

    struct timespec tp1;
    clock_gettime(CLOCK_MONOTONIC, &tp1);
    ERS_DEBUG(0, "Stored " << nResults << " results, " << nHistos << " histos, "
            << nTags << " tags in " << (tp1.tv_sec - tp0.tv_sec) + (tp1.tv_nsec - tp0.tv_nsec) / 1e9
            << " seconds");

    // erase stored data from buffer
    m_buffer.erase(m_buffer.begin(), m_buffer.begin()+count);

    // periodic checkpoints
    if (m_checkpointMinutes) {
        auto now = time(nullptr);
        if (m_fileContext->lastCheckpoint == 0) {
            m_fileContext->lastCheckpoint = now;
        } else if (now > m_fileContext->lastCheckpoint + m_checkpointMinutes*60) {
            // checkpoint again
            ERS_LOG("Checkpoint: write all trees to a file");

            // save trees to file
            m_fileContext->flush();

            ERS_LOG("Checkpoint: done");

            m_fileContext->lastCheckpoint = now;
        }
    }

}

bool
DqmArchiverRoot::decideSwitch()
{
    if (m_maxFileTimeIntervalSec > 0) {
        // if the time different between last and first results in file
        // is larger than limit then it's OK to switch.
        if (m_fileContext->lastTimeStamp - m_fileContext->firstTimeStamp >= m_maxFileTimeIntervalSec * 1000000) {
            ERS_DEBUG(1, "Exceeded per-file time limit: firstTimeStamp=" << m_fileContext->firstTimeStamp
                      << " usec, lastTimeStamp=" << m_fileContext->lastTimeStamp << " usec, limit="
                      << m_maxFileTimeIntervalSec << " sec");
            return true;
        }
    }
    if (m_maxFileResultCount > 0) {
        // if number of results stores in file is larger than limit then it's OK to switch.
        if (m_fileContext->resultCount >= m_maxFileResultCount) {
            ERS_DEBUG(1, "Exceeded per-file result count: resultCount=" << m_fileContext->resultCount
                      << " maxFileResultCount=" << m_maxFileResultCount);
            return true;
        }
    }
    return false;
}

} // namespace dqm_archive
