//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "dqm_archive/archiver/DqmaControllable.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <time.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "is/infodictionary.h"
#include "rc/RunParams.h"
#include "ers/ers.h"
#include "RunControl/Common/OnlineServices.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace dqm_archive {

//----------------
// Constructors --
//----------------
DqmaControllable::DqmaControllable()
    : daq::rc::Controllable()
    , m_config()
    , m_archiver()
{
}

//--------------
// Destructor --
//--------------
DqmaControllable::~DqmaControllable() throw()
{
    // If we are get killed by signal then stopArchiving is skipped and we have a mess
    // hanging around. Try to cleanup and save the data and hope it does not take
    // very long, otherwise bad guys will kill me anyways.
    if (m_archiver) {
        ERS_LOG("We are shutdown in the middle of running, will try to save data but it may fail");
        m_archiver->stop(false);
        m_archiver.reset();
    }

    m_config.reset();
}

void DqmaControllable::configure(const daq::rc::TransitionCmd&)
{
    // re-read configuration
    m_config.reset();
    try {
        m_config.reset(new Config());
    } catch (ers::Issue& ex) {
        ERS_LOG(ex);
        throw; // propagate back to framework
    }
}

void DqmaControllable::prepareForRun(const daq::rc::TransitionCmd&)
{
    unsigned run = 0;
    try {
        // need a run number, can only get it from IS
        auto& os = daq::rc::OnlineServices::instance();
        ISInfoDictionary dict(os.getIPCPartition());
        RunParams run_params;
        dict.getValue("RunParams.RunParams", run_params);
        run = run_params.run_number;
    } catch (const daq::is::Exception &ex) {
        // trouble with IS, try to continue by generating pseudo run number
        run = time(0);
        ERS_LOG("Failed to obtain run number from IS, will use fake run #" << run << ", error: " << ex.what());
    }

    try {
        if (m_config) m_archiver = m_config->makeArchiver(run);
    } catch (ers::Issue& ex) {
        ERS_LOG(ex);
        throw; // propagate back to framework
    }
    if (m_archiver) m_archiver->start();
}

void DqmaControllable::stopArchiving(const daq::rc::TransitionCmd&)
{
    if (m_archiver) m_archiver->stop(true);
    m_archiver.reset();
}

void DqmaControllable::unconfigure(const daq::rc::TransitionCmd&)
{
    // could it happen that unconfigure is called without stopArchiving?
    // I don't think so but try to be paranoid w.r.t. possible cleanups.
    if (m_archiver) {
        m_archiver->stop(false);
        m_archiver.reset();
    }

    m_config.reset();
}

} // namespace dqm_archive
