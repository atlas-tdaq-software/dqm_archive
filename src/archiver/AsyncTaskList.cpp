//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "dqm_archive/archiver/AsyncTaskList.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <vector>
#include <exception>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace dqm_archive {

void
AsyncTaskList::addTask(std::shared_ptr<AsyncTask> const& task)
{
    m_tasks.push_back(task);
}

void
AsyncTaskList::cleanup()
{
    for (auto iter = std::begin(m_tasks); iter != std::end(m_tasks); ) {
        auto taskptr = *iter;
        ERS_DEBUG(1, name() << " checking task " << taskptr->name());
        if (taskptr->waitFor(std::chrono::milliseconds::zero()) != WaitStatus::Timeout) {
            // completed, can be removed
            iter = m_tasks.erase(iter);
            ERS_DEBUG(1, name() << " removed task " << taskptr->name());
        } else {
            ++ iter;
        }
    }
}

std::string
AsyncTaskList::name() const
{
    return m_name;
}

bool
AsyncTaskList::isRunning()
{
    for (auto iter = std::begin(m_tasks); iter != std::end(m_tasks); ) {
        auto taskptr = *iter;
        ERS_DEBUG(1, name() << " checking task " << taskptr->name());
        if (taskptr->isRunning()) {
            ERS_DEBUG(1, name() << " task " << taskptr->name() << " is running");
            return true;
        } else {
            // also cleanup
            iter = m_tasks.erase(iter);
            ERS_DEBUG(1, name() << " removed task " << taskptr->name());
        }
    }
    return false;
}

AsyncTask::TaskStatus
AsyncTaskList::wait()
{
    for (auto iter = std::begin(m_tasks); iter != std::end(m_tasks); ) {
        auto taskptr = *iter;
        ERS_DEBUG(1, name() << " waiting for task " << taskptr->name());
        taskptr->wait();
        ERS_DEBUG(1, name() << " done waiting for task " << taskptr->name());
        // also cleanup
        iter = m_tasks.erase(iter);
        ERS_DEBUG(1, name() << " removed task " << taskptr->name());
    }
    // always return success even if some tasks have failed
    return TaskStatus::Success;
}

AsyncTask::WaitStatus
AsyncTaskList::waitFor(std::chrono::milliseconds timeout)
{
    auto until = std::chrono::steady_clock::now() + timeout;
    return waitUntil(until);
}

AsyncTask::WaitStatus
AsyncTaskList::waitUntil(std::chrono::steady_clock::time_point timePoint)
{
    for (auto iter = std::begin(m_tasks); iter != std::end(m_tasks); ) {
        auto taskptr = *iter;
        ERS_DEBUG(1, name() << " waiting for task " << taskptr->name());
        if (taskptr->waitUntil(timePoint) == WaitStatus::Timeout) {
            ERS_DEBUG(1, name() << " task " << taskptr->name() << " is still running");
            return WaitStatus::Timeout;
        } else {
            // also cleanup
            iter = m_tasks.erase(iter);
            ERS_DEBUG(1, name() << " removed task " << taskptr->name());
        }
    }
    // always return success even if some tasks have failed
    return WaitStatus::Success;
}

} // namespace dqm_archive
