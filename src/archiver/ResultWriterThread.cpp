//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "dqm_archive/archiver/ResultWriterThread.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <algorithm>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dqm_archive/archiver/IDqmArchiver.h"
#include "dqm_archive/archiver/ResultQueue.h"
#include "ers/ers.h"
#include "owl/semaphore.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

    ERS_DECLARE_ISSUE(error, UnhandledException,
            "ResultWriterThread: Unhandled '" << name << "' exception was thrown",
            ((const char *)name) )

     bool compareResults(const dqm_archive::ResultWriterThread::CoreResultQueue::value_type& lhs,
             const dqm_archive::ResultWriterThread::CoreResultQueue::value_type& rhs) {
        // order by time first and name second
        if (lhs.second->universal_time_ < rhs.second->universal_time_) return true;
        if (lhs.second->universal_time_ > rhs.second->universal_time_) return false;
        return lhs.first < rhs.first;
    }
}

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace dqm_archive {

//----------------
// Constructors --
//----------------
ResultWriterThread::ResultWriterThread(std::unique_ptr<IDqmArchiver> arch,
    const std::shared_ptr<CoreResultQueue>& queue,
    OWLSemaphore& semaphore)
    : m_arch(std::move(arch))
    , m_queue(queue)
    , m_semaphore(semaphore)
{
}

// operator called by thread
void
ResultWriterThread::operator()()
try {

    bool stop = false;
    CoreResultQueue::cont_type input_results;
    while (not stop) {

        // get more data from a queue
        input_results.clear();
        m_queue->pop(input_results);

        ERS_DEBUG(2, "Received: " << input_results.size() << " results" );

        // check for EOD first, remove all of them
        auto it = std::remove_if(input_results.begin(), input_results.end(),
                [](const CoreResultQueue::value_type& res) {return not res.second;});
        if (it != input_results.end()) {
            ERS_DEBUG(1, "Received: EOD");
            stop = true;
            input_results.erase(it, input_results.end());
        }

        // store() likes ordered data, sort new data first
        std::sort(input_results.begin(), input_results.end(), ::compareResults);

        for (const auto& res: input_results) {
            if(not m_arch->store(res.first, res.second)) {
                ERS_LOG("DQM result was not stored, likely out of order: " << res.first);
            }
        }

    }

    ERS_LOG("Receive loop finished, finalizing archiving");

    // to finalize we need to destroy archiver
    m_arch->finish();
    m_arch.reset();

    // say that we are done
    m_semaphore.post();

} catch (const ers::Issue& ex) {
    ers::error(ex);
    m_semaphore.post();
} catch (const std::exception &ex) {
    ers::error(::error::UnhandledException(ERS_HERE, "standard", ex));
    m_semaphore.post();
} catch (...) {
    ers::fatal(::error::UnhandledException(ERS_HERE, "unknown"));
    m_semaphore.post();
}

} // namespace dqm_archive
