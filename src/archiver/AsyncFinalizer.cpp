//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "AsyncFinalizer.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <vector>
#include <exception>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dqm_archive/archiver/AsyncTaskProcess.h"
#include "dqm_archive/archiver/ILTSArchiver.h"
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

ERS_DECLARE_ISSUE(warnings, IndexingIssue,
        "dqma_indexer failed on file " << path, ((std::string) path))
ERS_DECLARE_ISSUE(warnings, IndexingTimeout,
        "dqma_indexer takes too long to index file " << path, ((std::string) path))
ERS_DECLARE_ISSUE(warnings, ThreadError, "Exception in std::thread",)

// Send specified file to long term storage
bool sendToLTS(std::string const& path, std::shared_ptr<dqm_archive::ILTSArchiver> const& ltsArchiver)
{
    try {
        // send it to coca
        if (ltsArchiver != nullptr) {
            ERS_LOG("Sending output file to long-term storage");
            ltsArchiver->store(path);
        }
    } catch (const ers::Issue& ex) {
        // report an error but continue, someone needs to take care later
        ERS_LOG(ex);
        ers::error(ex);
        return false;
    }
    return true;
}

}

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace dqm_archive {


AsyncFinalizer::AsyncFinalizer(const std::string& path,
                               std::shared_ptr<ILTSArchiver> const& ltsArchiver,
                               std::chrono::milliseconds indexingTimeout,
                               const std::string& name)
    : AsyncTaskThread(name)
{
    std::promise<bool> promise;
    auto future = promise.get_future();
    auto thread = std::thread(finalizeTread, path, ltsArchiver, indexingTimeout, std::move(promise));
    setThread(std::move(thread), std::move(future));
}

void
AsyncFinalizer::finalizeTread(std::string path,
                              std::shared_ptr<ILTSArchiver> ltsArchiver,
                              std::chrono::milliseconds indexingTimeout,
                              std::promise<bool> promise)
{
    // Indexing is performed by dqma_indexer app, it can take some time to run it
    // and if application does not finish in time we want to keep it running,
    // so set `background` option to true.
    try {
        ERS_LOG("Start dqma_indexer process on " << path);
        std::vector<std::string> commandLine{"dqma_indexer", path};
        bool background = true;
        AsyncTaskProcess process(commandLine, std::chrono::milliseconds(100), "dqma_indexer", background);

        // wait until it finishes (or not)
        auto const status = process.waitFor(indexingTimeout);
        if (status == AsyncTask::WaitStatus::Timeout) {
            // still running
            ERS_LOG("dqma_indexer is still running on " << path << ", will not wait longer."
                    " File has to be registered in CoCa manually later.");
            ers::warning(::warnings::IndexingTimeout(ERS_HERE, path));
            promise.set_value(false);
            return;
        } else if (status == AsyncTask::WaitStatus::Failure) {
            // finished but with bad status
            ers::warning(::warnings::IndexingIssue(ERS_HERE, path));
            promise.set_value(false);
            return;
        }
        ERS_LOG("dqma_indexer process finished on " << path);
    } catch (const std::exception& exc) {
        ers::warning(::warnings::IndexingIssue(ERS_HERE, path, exc));
        promise.set_value(false);
        return;
    }

    // Looks like indexing finished OK, send it to coca
    bool stat = ::sendToLTS(path, ltsArchiver);
    promise.set_value(stat);
}

} // namespace dqm_archive
