//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "dqm_archive/common/RootSerializer.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <stdexcept>
#include <string_view>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "TBufferFile.h"
#include "TClass.h"
#include "TROOT.h"
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

const int latestVersion = 0;

// Must include latestVersion, naturally, and all version numbers supported.
const int supportedVersions[] = {0};


void serialize_v0(TObject* tobj, TString* buf);
TObject* deserialize_v0(TBufferFile& tbuf);

ERS_DECLARE_ISSUE(errors, InvalidVersionException, "Invalid version number " << version, ((int) version))

}

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace dqm_archive {

RootSerializer::RootSerializer(int version) :
        m_version(version)
{
    if (m_version < 0) {
        m_version = ::latestVersion;
    } else {
        auto iter = std::find(std::begin(::supportedVersions), std::end(::supportedVersions), m_version);
        if (iter == std::end(::supportedVersions)) {
            throw errors::InvalidVersionException(ERS_HERE, m_version);
        }
    }
}

TString RootSerializer::serialize(TObject* tobj) const
{
    TString buf;
    switch (m_version) {
    case 0:
        ::serialize_v0(tobj, &buf);
        break;
    }

    return buf;
}

TObject*
RootSerializer::deserialize(const TString& data) const
{
    // make TBufferFile
    TBufferFile tbuf(TBufferFile::kRead, data.Length(), (void*)data.Data(), false);

    // get the version back
    unsigned char version;
    tbuf.ReadUChar(version);

    ERS_DEBUG(2, "RootSerializer::deserialize: version = " << int(version));

    switch (version) {
    case 0:
        return ::deserialize_v0(tbuf);
    }

    return nullptr;
}

} // namespace dqm_archive

namespace {

void serialize_v0(TObject* tobj, TString* buf)
{
    // make TBufferFile
    TBufferFile tbuf(TBufferFile::kWrite);

    // write serializer version
    unsigned char version = 0;
    tbuf << version;

    // store object class name
    TString className = tobj->ClassName();
    tbuf << className;

    // store object
    tobj->Streamer(tbuf);

    // Need to use string_view here, as TString(char*, size_t) was changed to
    // issue a warning when buffer contains NUL character >:(
    *buf = TString(std::string_view(tbuf.Buffer(), tbuf.Length()));
}

TObject* deserialize_v0(TBufferFile& tbuf)
{
    // get class name back
    TString className;
    tbuf.ReadTString(className);

    ERS_DEBUG(2, "RootSerializer::deserialize: class = '" << className << "'");

    // get class instance from ROOT
    TClass* tclass = gROOT->GetClass(className);
    if (not tclass) return nullptr;

    ERS_DEBUG(2, "RootSerializer::deserialize: found class");

    // make an object
    TObject* tobj = (TObject*) tclass->New();
    if (tobj) {
        ERS_DEBUG(2, "RootSerializer::deserialize: made new instance");
        // read its stuff from buffer
        tobj->Streamer(tbuf);
    }

    return tobj;
}

} // namespace
