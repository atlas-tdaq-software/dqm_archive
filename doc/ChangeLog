#--------------------------------------------------------------------------
# File and Version Information:
#  $Id: ChangeLog 115213 2013-08-02 23:50:38Z salnikov $
#
# Description:
#  ChangeLog file for package dqm_archive
#------------------------------------------------------------------------

Package author: Andy Salnikov

Please describe any modifications that you made to the package in the
reverse time order.

Tag: dqm_archive-00-11-00
2015-11-05 Andy Salnikov
- add file cache to client to save multiple lookups

Tag: dqm_archive-00-10-01
2015-11-03 Andy Salnikov
- install dqma_indexer

Tag: dqm_archive-00-10-00
2015-11-03 Andy Salnikov
- improve robustness against crashes
- implement periodic chekpointing for trees filled during run
- add separate application for post-mortem indexing

Tag: dqm_archive-00-09-03
2015-10-28 Andy Salnikov
- make data saving more robust against crashes at EoR

Tag: dqm_archive-00-09-02
2015-10-13 Andy Salnikov
- fix for missing semicolon after ERS macro

Tag: dqm_archive-00-09-01
2015-08-24 Andy Salnikov
- improve performance of some data retrieving methods (ADHI-3945)
- takes into account ordering of the stored data

Tag: dqm_archive-00-09-00
2015-04-04 Andy Salnikov
- previous patch was incomplete, some ROOT calls were not protected
- now correctly protect all ROOT calls
- moved histogram retrieving and result format conversion to a separate
  thread to avoid delays in IS receiver threads
- in DqmArchiveMgr::stop() add try block around call to receiver->stop()
  to avoid crashes when unsubscribe() throws (may happen when IS server is
  missing during subscribe())
- in receiver use scheduleSubscription() instead of subscribe() to handle
  missing IS server on startup

Tag: dqm_archive-00-08-00 is mistake, same as dqm_archive-00-07-00

Tag: dqm_archive-00-07-00
2015-03-24 Andy Salnikov
- synchronize all ROOT operations to avoid races
- this is supposed to fix ADHI-3652

Tag: dqm_archive-00-06-00
2014-12-09 Andy Salnikov
- DqmArchiveClient: add new method runCount(), runs() now does not return
  total run count, use runCount() instead

Tag: dqm_archive-00-05-02
2014-11-30 Andy Salnikov
- pydqm_archive.cpp: fixing ADHI-3464 (double-delete of histogram object)

Tag: dqm_archive-00-05-01
2014-11-26 Andy Salnikov
- merged dqm_archive-00-04-07 onto trunk

Tag: dqm_archive-00-04-07 (from branch tdaq-05-04-00_patches)
2014-11-26 Andy Salnikov
- DqmaResultReceiver.cpp: fixing TObjArray capacity (ADHI-3450)

2014-11-26 Andy Salnikov
- made branch tdaq-05-04-00_patches from tag dqm_archive-00-04-04
- merged changes in tags dqm_archive-00-04-05 and dqm_archive-00-04-06 onto
  new branch

Tag: dqm_archive-00-05-00
2014-10-30 Andy Salnikov
- introduce RunInfo class returned from client runs() method instead of 
  run number

Tag: dqm_archive-00-04-06
2014-10-29 Andy Salnikov
- remove unfinished pbeast client implementation

Tag: dqm_archive-00-04-05
2014-09-30 Andy Salnikov
- requirements: remove -lCint which is missing in ROOT6, should build OK
  even with ROOT5

Tag: dqm_archive-00-04-04
2014-07-10 Andy Salnikov
- improved building of region/param tree from configuration

Tag: dqm_archive-00-04-03
2014-06-27 Andy Salnikov
- merged dqm_archive-00-03-06 onto trunk

Tag: dqm_archive-00-03-06 (from branch tdaq-05-03-00_patches)
2014-06-27 Andy Salnikov
- dqma_result_generator.cpp: add support for templates in DQMF OKS config
- DqmArchiveMgr::stop(): add option to detach or join writer thread, if process
  is killed by a signal then detaching does not work, have to join
- DqmaControllable: in destructor stop DqmArchiveMgr with join option, this
  is to handle shutdowns from RunControl

Tag: dqm_archive-00-04-02
2014-06-24 Andy Salnikov
- merged dqm_archive-00-03-05 onto trunk

Tag: dqm_archive-00-04-01
2014-06-24 Andy Salnikov
- DqmArchiveClientRoot.cpp: add in-memory cache to reduce tree search time 
  for tag names
- DqmArchiveClientRootCoca.cpp: changed default name for coca dataset

Tag: dqm_archive-00-03-05 (from branch tdaq-05-03-00_patches)
2014-06-24 Andy Salnikov
- add support for templates in DQMF OKS config

Tag: dqm_archive-00-04-00
2014-02-04 Andy Salnikov
- add implementation to support AddConfigHist option 

Tag: dqm_archive-00-03-04
2014-02-04 Andy Salnikov
- added OKS config option AddConfigHist to the schema

Tag: dqm_archive-00-03-03
2014-02-04 Andy Salnikov
- update tags in release notes

Tag: dqm_archive-00-03-02
2014-01-26 Andy Salnikov
- forgot to implement PyDqmResult::hasHistograms

Tag: dqm_archive-00-03-01
2014-01-24 Andy Salnikov
- adding release notes

Tag: dqm_archive-00-03-00
2014-01-22 Andy Salnikov
- adding rootcoca client implementation, tested with test coca database only
- cleanup, move some headers to src/ directory

Tag: dqm_archive-00-02-00
2014-01-22 Andy Salnikov
- added histograms() method to client interface including python wrapper

Tag: dqm_archive-00-01-01
2014-01-21 Andy Salnikov
- fixing requirements

Tag: dqm_archive-00-01-00
2014-01-20 Andy Salnikov
- re-implemented archiving of histograms, now histos are saved in a separate 
  tree as serialized blobs
- implemented filtering of the results
- make client interface more consistent

Tag: dqm_archive-00-00-04
2014-01-14 Andy Salnikov
- some updates for pbeast client, mostly for testing
- added new configuration parameter UpdateInterval to limit update rate
  if nothing is changing

Tag: dqm_archive-00-00-03
2013-12-21 Andy Salnikov
- dqma_result_generator: DQParameter method names changed

Tag: dqm_archive-00-00-02
2013-10-03 Andy Salnikov
- additional protection for the case when DQM agents are not defined 
  in partition

Tag: dqm_archive-00-00-01
2013-10-03 Andy Salnikov
- a bit of refactoring and renaming
- more or less complete implementation of the archiver including saving files
  to coca
- schema for server app is ready as well

2013-09-30 Andy Salnikov
- progress on RC-based app

2013-09-30 Andy Salnikov
- move development from git to svn
- previous history is in  ~salnikov/repo/dqm_archive.git (could have moved
  whole history to svn but it's not very useful and I never done it before)
- standalone archiver works, work on RC-based app started
- priminary implementation of client API is there
