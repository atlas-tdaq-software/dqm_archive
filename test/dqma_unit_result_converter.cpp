/**
 *  Unit test for DqmaResultConverter.
 */

#include <memory>
#include <string>

#include <TObjArray.h>

#include "dqm_archive/archiver/DqmaResultConverter.h"
#include "dqm_archive/archiver/DqmaRootReceiver.h"
#include "dqm_core/Result.h"
#include "dqmf/is/Result.h"

#define BOOST_TEST_MODULE dqma_unit_result_converter
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

namespace tt = boost::test_tools;
using namespace dqm_archive;

// ==============================================================

using IsResultQueue = ResultQueue<std::shared_ptr<dqmf::is::Result>>;
using CoreResultQueue = ResultQueue<std::shared_ptr<dqm_core::Result>>;

namespace {

class TestDqmaRootReceiver : public DqmaRootReceiver {
public:

    virtual ~TestDqmaRootReceiver() = default;

    // Retrieve histograms with given names
    virtual std::unique_ptr<TObjArray> get_histograms(std::vector<std::string> const& objects) {
        count ++;
        return nullptr;
    }

    unsigned count = 0;
};

// generate a bunch of result and add to the queue
void _fill_queue(IsResultQueue& queue, unsigned count)
{
    std::string name = "X";
    for (unsigned i = 0 ; i < count; ++ i) {
        auto result = std::make_shared<dqmf::is::Result>();
        result->objects.push_back("/object");
        queue.push(std::make_pair(name, result));
    }
}

}

BOOST_AUTO_TEST_CASE(test_skip_histograms)
{
    unsigned const queueSize = 1000;
    auto inputQueue = std::make_shared<IsResultQueue>(queueSize);
    auto outputQueue = std::make_shared<CoreResultQueue>(queueSize + queueSize/10);

    std::shared_ptr<TestDqmaRootReceiver> rootReceiver(new TestDqmaRootReceiver);
    DqmaResultConverter converter(inputQueue, outputQueue, rootReceiver);

    IsResultQueue::cont_type input_results;

    _fill_queue(*inputQueue, 100);
    inputQueue->pop(input_results);
    BOOST_TEST(input_results.size() == 100);

    converter._process_buffer(input_results);
    BOOST_TEST(rootReceiver->count == 100);
    {
        CoreResultQueue::cont_type output_results;
        outputQueue->pop(output_results);
        BOOST_TEST(output_results.size() == 100);
    }

    input_results.clear();
    _fill_queue(*inputQueue, 100);
    inputQueue->pop(input_results);
    BOOST_TEST(input_results.size() == 100);

    // fill the queue above 0.95 threshold
    _fill_queue(*inputQueue, 951);
    converter._process_buffer(input_results);
    BOOST_TEST(rootReceiver->count == 100);
    {
        CoreResultQueue::cont_type output_results;
        outputQueue->pop(output_results);
        BOOST_TEST(output_results.size() == 100);
    }

    input_results.clear();
    inputQueue->pop(input_results);
    BOOST_TEST(input_results.size() == 951);

    converter._process_buffer(input_results);
    BOOST_TEST(rootReceiver->count == 1051);
    {
        CoreResultQueue::cont_type output_results;
        outputQueue->pop(output_results);
        BOOST_TEST(output_results.size() == 951);
    }

    input_results.clear();
    _fill_queue(*inputQueue, 100);
    inputQueue->pop(input_results);
    BOOST_TEST(input_results.size() == 100);

    // fill the queue just below 0.95 threshold
    _fill_queue(*inputQueue, 949);
    converter._process_buffer(input_results);
    BOOST_TEST(rootReceiver->count == 1151);
    {
        CoreResultQueue::cont_type output_results;
        outputQueue->pop(output_results);
        BOOST_TEST(output_results.size() == 100);
    }

}
