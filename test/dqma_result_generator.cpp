//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <map>
#include <set>
#include <fstream>
#include <algorithm>
#include <memory>
#include <random>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <unistd.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "config/Configuration.h"
#include "dal/util.h"
#include "dqm_config/dal/DQAlgorithm.h"
#include "dqm_config/dal/DQParameter.h"
#include "dqm_config/dal/DQRegion.h"
#include "dqm_config/dal/DQSummaryMaker.h"
#include "dqm_core/Result.h"
#include "dqmf/Output.h"
#include "ers/ers.h"
#include "ipc/core.h"
#include "ipc/partition.h"
#include "is/infodictionary.h"
#include "is/infoT.h"
#include "oh/OHRootProvider.h"
#include "TH1.h"
#include "TObjArray.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------
namespace po = boost::program_options;

namespace {

    ERS_DECLARE_ISSUE (Issues, CommandLineSyntax,
            "bad command line syntax: " << error, ((std::string) error))
    ERS_DECLARE_ISSUE(Issues, UncaughtException, "Uncaught exception, terminating.", )
    ERS_DECLARE_ISSUE (Issues, ConfigFileError,
            "error in config file: " << file << ":" << line << ": " << error, ((std::string) file)((unsigned) line)((std::string) error))
    ERS_DECLARE_ISSUE (Issues, UnknownStatusName,
            "Status name unrecognized: " << status, ((std::string) status))


    struct AlgoConfig {
        AlgoConfig() : weightSum(0) {}

        void addConfig(dqm_core::Result::Status status, double weight, const std::vector<std::string>& tags) {
            weights[status] = weight;
            weightSum += weight;
            this->tags[status] = tags;
        }

        std::shared_ptr<dqm_core::Result> makeResult(TObject* hist) const;

        std::map<dqm_core::Result::Status, double> weights;
        double weightSum;
        mutable std::map<dqm_core::Result::Status, std::vector<std::string> > tags;
    };

    // pair of parameter name and algorithm name
    typedef std::pair<std::string, std::string> ParamAlgoPair;
    typedef std::map<std::string, std::string> ParamTypeMap;
    typedef std::map<std::string, std::string> ParamTree;     // child -> parent
    typedef std::vector<ParamAlgoPair> Parameters;
    typedef std::vector<std::string> ConfigHistos;
    typedef std::map<std::string, AlgoConfig> Algos;
    typedef std::map<std::string, std::shared_ptr<dqm_core::Result>> CachedResults;

    // read parameter map, fill mapping parameter name -> algorithm name and mapping for parameter types
    void readParameters(Configuration& configDb, const daq::core::Partition& partition, Parameters& params,
            ParamTypeMap& paramTypes, ConfigHistos& configHistos);

    // read algorithms configurations
    void readAlgoFile(const std::string& fileName, Algos& algos);

    // read parameter tree from config
    void readTree(Configuration& configDb, const daq::core::Partition& partition, ParamTree& paramTree);

    // read parameter tree from config
    void publishTree(ParamTree& paramTree, const std::string& partition, const std::string& server, const std::string& isname);

    // method that publishes results
    bool publish(dqmf::Output& output, Parameters& params, const Algos& algos, ParamTypeMap& paramTypes, CachedResults& resCache,
        bool dohisto, unsigned updateInterval);

    void publishExtraHistos(const std::string& partName, const std::string& serverName, ::ConfigHistos const& configHistos);

    // histogram params
    struct HistoMaker {

        // Factory method for new histo makers
        static HistoMaker factory(const std::string& param, const std::string& paramType);

        // generator for histograms
        TObject* make();

        // parameter of histograms
        int nbins;
        double xmin;
        double xmax;
        std::string param;

    };

    // map from parameter name to histogram maker
    std::map<std::string, HistoMaker> hmakers;

}

// ------------
// --- Main ---
// ------------
int
main (int argc, char **argv)
try {

    // seed random engine
    srand((unsigned)time(NULL));

    // init IPC stuff
    IPCCore::init (argc, argv);

    // command line parsing
    po::options_description cmdline_options("Available options");
    cmdline_options.add_options()
            ("help,h", "print usage and exit")
            ("partition,p", po::value<std::string>()->required(), "TDAQ partition name, required.")
            ("server,s", po::value<std::string>()->required(), "IS server name for publishing results, required.")
            ("interval,i", po::value<unsigned>()->default_value(60U), "Interval for publishing in seconds")
            ("update-interval,u", po::value<unsigned>()->default_value(0U), "Result update rate in seconds")
            ("once,1", "Publish once and exit")
            ("histo,H", "Make histograms")
            ("add-cfg-histo", "Add histograms from DQParameter configuration.")
            ("tree,t", "Publish parameter tree in IS object with name DQM-Parameter-Tree")
            ;
    po::options_description pos_options("Hidden options");
    pos_options.add_options()
            ("config-db", po::value<std::string>()->required(), "Configuration database to use, will cause publishing of parameter tree")
            ("algo-list-file", po::value<std::string>()->default_value(std::string()), "Input file with algorithm names, optional")
            ;

    po::options_description all_options("All options");
    all_options.add(cmdline_options).add(pos_options);

    po::positional_options_description positional;
    positional.add("config-db", 1);
    positional.add("algo-list-file", 1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(all_options).positional(positional).run(), vm);

    if (vm.count("help")) {
        std::cout << "Generator for DQMF results.\n\n"
              << "Usage: " << argv[0] << " [options] config-db [algo-list-file]\n\n"
              << cmdline_options << "\n";
        return 0;
    }

    po::notify(vm);

    const std::string partName = vm["partition"].as<std::string>();
    const std::string serverName = vm["server"].as<std::string>();
    const unsigned interval = vm["interval"].as<unsigned>();
    const unsigned updateInterval = vm["update-interval"].as<unsigned>();
    const bool once = vm.count("once") > 0;
    const bool dohisto = vm.count("histo") > 0;
    const bool addCfgHist = vm.count("add-cfg-histo");
    const bool dotree = vm.count("tree") > 0;
    const std::string configDb = vm["config-db"].as<std::string>();
    const std::string algoListFile = vm["algo-list-file"].as<std::string>();

    // read input files
    ::Parameters params;
    ::Algos algos;
    ::ParamTypeMap paramTypes;
    ::ParamTree paramTree;
    ::ConfigHistos configHistos;
    try {
        // Get configuration
        Configuration config(configDb);

        const daq::core::Partition* partConfig = daq::core::get_partition(config, partName, 0);
        config.register_converter(new daq::core::SubstituteVariables(*partConfig));

        ::readParameters(config, *partConfig, params, paramTypes, configHistos);
        ::readAlgoFile(algoListFile, algos);
        if (dotree) ::readTree(config, *partConfig, paramTree);
    } catch (const std::exception& ex) {
        std::cerr << ex.what();
        return 1;
    }

    // instantiate Output "stream"
    std::vector<std::string> parameters;
    parameters.push_back(partName);
    parameters.push_back(serverName);
    parameters.push_back("PPProvider");
    dqmf::Output output(parameters);
    output.activate();

    // publish tree
    if (not paramTree.empty()) ::publishTree(paramTree, partName, serverName, "DQM-Parameter-Tree");

    CachedResults resCache;
    while (true) {

        // publish extra histos
        if (dohisto and addCfgHist) {
            publishExtraHistos(partName, serverName, configHistos);
        }

        // publish everything
        if (not publish(output, params, algos, paramTypes, resCache, dohisto, updateInterval)) break;

        // may need to stop here
        if (once) break;

        // sleep
        sleep(interval);

    }


} catch (const po::error& ex) {
    std::cerr << ex.what() << "\nUse --help option for usage information.\n";
    return 1;
} catch (const std::exception& ex) {
    ers::error(Issues::UncaughtException(ERS_HERE, ex));
    return 1;
} catch (...) {
    ers::error(Issues::UncaughtException(ERS_HERE));
    return 1;
}


namespace {

void
publishExtraHistos(const std::string& partName, const std::string& serverName, ::ConfigHistos const& configHistos)
{
    IPCPartition part(partName);

    for (auto hname: configHistos) {
        auto p = hname.find('.');
        if (p == std::string::npos) continue;
        hname = hname.substr(p+1);

        p = hname.find('.');
        if (p == std::string::npos) continue;

        std::string provName(hname, 0, p);
        hname = hname.substr(p+1);

        // make histo
        TH1::AddDirectory(kFALSE);

        std::string title = "Histogram " + hname;
        int nbins = 20 + rand() % 100;
        double xmin = rand() * 200. / RAND_MAX - 100;
        double xmax = xmin + 1. + rand() * 200. / RAND_MAX;

        TH1I histo(hname.c_str(), title.c_str(), nbins, xmin, xmax);

        for (int i = 0; i != 40; ++ i) {
            double x = rand() * (xmax-xmin) / RAND_MAX + xmin;
            histo.Fill(x);
        }

        // this is a horrible hack to fix ROOT multi-threading trouble,
        // InheritsFrom is not thread safe and it is called from Output
        if ( histo.InheritsFrom( TObjArray::Class() ) ) {
            histo.Fill(1.);
        }

        // make provider
        OHRootProvider prov(part, serverName, provName);
        prov.publish(histo, hname);

    }

}

void getParam(const dqm_config::dal::DQRegion* region, Parameters& params, ParamTypeMap& paramTypes,
        ConfigHistos& configHistos)
{
    const std::string& pname = region->UID();
    const std::string& algo = region->get_DQSummaryMaker()->UID();
    params.push_back(std::make_pair(pname, algo));
    paramTypes.insert(std::make_pair(pname, region->class_name()));

    // do the same for all sub-regions
    std::vector<const dqm_config::dal::DQRegion *> regions;
    region->get_all_regions(regions);
    for (const auto& subreg: regions) {
        getParam(subreg, params, paramTypes, configHistos);
    }

    // and for sub-parameters
    std::vector<const dqm_config::dal::DQParameter *> parameters;
    region->get_all_parameters(parameters);
    for (const auto& param: parameters) {
        const std::string& pname = param->UID();
        const std::string& algo = param->get_DQAlgorithm()->UID();
        const std::vector<std::string>& inputs = param->get_InputDataSource();

        params.push_back(std::make_pair(pname, algo));
        paramTypes.insert(std::make_pair(pname, param->class_name()));
        configHistos.insert(configHistos.end(), inputs.begin(), inputs.end());
    }
}

// read parameter map, fill mapping parameter name -> algorithm name and mapping for parameter types
void readParameters(Configuration& config, const daq::core::Partition& partition, Parameters& params,
        ParamTypeMap& paramTypes, ConfigHistos& configHistos)
{
    // get all top DQ regions
    std::vector<const dqm_config::dal::DQRegion*> regions;
    dqm_config::dal::DQRegion::get_root_regions(config, partition, regions);
    ERS_DEBUG(1, "readParameters: found " << regions.size() << " root regions in config file");

    // get the ID of a parent for each parameter
    for (const auto& region: regions) {
        getParam(region, params, paramTypes, configHistos);
    }
}

void
findParents(ParamTree& paramTree, const dqm_config::dal::DQRegion* region)
{

    // do the same for all sub-regions
    std::vector<const dqm_config::dal::DQRegion *> regions;
    region->get_all_regions(regions);
    for (const auto& subreg: regions) {
        paramTree[subreg->UID()] = region->UID();
        findParents(paramTree, subreg);
    }

    // and for sub-parameters
    std::vector<const dqm_config::dal::DQParameter *> parameters;
    region->get_all_parameters(parameters);
    for (const auto& param: parameters) {
        paramTree[param->UID()] = region->UID();
    }
}

// read parameter tree from config
void readTree(Configuration& config, const daq::core::Partition& partition, ParamTree& paramTree)
{
    // get all top DQ regions
    std::vector<const dqm_config::dal::DQRegion*> regions;
    dqm_config::dal::DQRegion::get_root_regions(config, partition, regions);
    ERS_DEBUG(1, "Config: found " << regions.size() << " root regions in configuration");

    // get the ID of a parent for each parameter
    for (const auto& region: regions) {
        paramTree[region->UID()] = std::string();
        findParents(paramTree, region);
    }

    ERS_DEBUG(1, "read " << paramTree.size() << " into tree");
}

dqm_core::Result::Status
name2status(const std::string& name)
{
    if (name == "Green") {
        return dqm_core::Result::Green;
    } else if (name == "Yellow") {
        return dqm_core::Result::Yellow;
    } else if (name == "Red") {
        return dqm_core::Result::Red;
    } else if (name == "Undefined") {
        return dqm_core::Result::Undefined;
    } else if (name == "Disabled") {
        return dqm_core::Result::Disabled;
    } else {
        throw Issues::UnknownStatusName(ERS_HERE, name);
    }
}

// read algorithms configurations
void
readAlgoFile(const std::string& fileName, Algos& algos)
{

    // create one no-name algorithm to be used as default
    AlgoConfig& algo = algos[std::string()];
    std::vector<std::string> tags;
    algo.addConfig(dqm_core::Result::Undefined, 1.0, tags);
    algo.addConfig(dqm_core::Result::Yellow, 3.0, tags);
    algo.addConfig(dqm_core::Result::Green, 95.0, tags);
    tags.push_back("NBinsRed");
    algo.addConfig(dqm_core::Result::Red, 1.0, tags);

    if (not fileName.empty()) {

        ERS_DEBUG(2, "reading algorithms from file " << fileName);
        std::ifstream in(fileName.c_str());
        std::string line;
        std::string algoName;
        unsigned lineNo = 0;
        while (std::getline(in, line)) {

            ++ lineNo;

            boost::algorithm::trim(line);
            if (line.empty()) continue;
            if (line[0] == '#') continue;

            std::vector<std::string> words;
            boost::algorithm::split(words, line, boost::algorithm::is_any_of(":"));

            if (not words[0].empty()) algoName = words[0];
            AlgoConfig& algo = algos[algoName];

            ERS_DEBUG(3, "  algo name = " << algoName);

            if (words.size() == 1) continue;
            if (words.size() < 3) {
                throw Issues::ConfigFileError(ERS_HERE, fileName, lineNo, "too few specifications");
            }

            dqm_core::Result::Status status =  name2status(words[1]);
            double weight = 0;
            try {
                weight = boost::lexical_cast<double>(words[2]);
            } catch (const boost::bad_lexical_cast& ex) {
                throw Issues::ConfigFileError(ERS_HERE, fileName, lineNo, ex.what());
            }

            ERS_DEBUG(3, "  algo config = " << algoName << " " << words[1] << " " << weight);

            words.erase(words.begin(), words.begin()+3);


            algo.addConfig(status, weight, words);
        }
        ERS_DEBUG(1, "read " << algos.size() << " configurations");

    }
}


std::shared_ptr<dqm_core::Result>
AlgoConfig::makeResult(TObject* hist) const
{

    double r = rand() * this->weightSum / RAND_MAX;

    double wsum = 0;
    dqm_core::Result::Status status = dqm_core::Result::Green;
    for (std::map<dqm_core::Result::Status, double>::const_iterator it = weights.begin(); it != weights.end(); ++ it) {
        wsum += it->second;
        if (wsum >= r) {
            status = it->first;
            break;
        }
    }

    std::shared_ptr<dqm_core::Result> res = std::make_shared<dqm_core::Result>(status, hist);
    const std::vector<std::string>& tagNames = this->tags[status];
    for (const std::string& tagName: tagNames) {
        double value = 0;
        if (tagName.find("Fraction") != std::string::npos) {
            value = double(rand()) / RAND_MAX;
        } else if (tagName.find("Num") != std::string::npos or tagName.find("NBins") != std::string::npos
                or tagName.find("Entries") != std::string::npos) {
            value = rand() % 1000;
        } else {
            value = rand() * 1000. / RAND_MAX;
        }

        res->tags_.insert(std::make_pair(tagName, value));
    }

    return res;
}

// Factory method for new histo makers
HistoMaker
HistoMaker::factory(const std::string& param, const std::string& paramType)
{
    // find/make histo maker
    HistoMaker hmaker;
    auto hmit = hmakers.find(param);
    if (hmit != hmakers.end()) {

        hmaker = hmit->second;

    } else {

        if (paramType == "DQRegion") {

            // number of bins is negative, means do not make any histograms
            int nbins = -1;
            hmaker = HistoMaker{nbins, 0., 0., param};

        } else {

            // number of bins is between 20 and 120
            int nbins = 20 + rand() % 100;

            // range
            double xmin = rand() * 200. / RAND_MAX - 100;
            double xmax = xmin + 1. + rand() * 200. / RAND_MAX;

            hmaker = HistoMaker{nbins, xmin, xmax, param};
        }
        hmakers.insert(std::make_pair(param, hmaker));
    }

    return hmaker;
}

// generator for histograms
TObject* HistoMaker::make()
{
    if (nbins < 0) return 0;

    TH1::AddDirectory(kFALSE);

    std::string name = "HISTO_" + param;
    std::string title = "Histogram for param " + param;

    TH1* histo = new TH1I(name.c_str(), title.c_str(), nbins, xmin, xmax);

    for (int i = 0; i != 10; ++ i) {
        double x = rand() * (xmax-xmin) / RAND_MAX + xmin;
        histo->Fill(x);
    }

    // this is a horrible hack to fix ROOT multi-threading trouble,
    // InheritsFrom is not thread safe and it is called from Output
    if ( histo->InheritsFrom( TObjArray::Class() ) ) {
        delete histo;
        histo = 0;
    }

    return histo;
}


// read parameter tree from config
void publishTree(ParamTree& paramTree, const std::string& partition, const std::string& server, const std::string& isname)
{
    std::string treeStr;
    for (const auto& pair: paramTree) {
        if (not treeStr.empty()) treeStr += '\n';
        treeStr += pair.first;
        treeStr += '\t';
        treeStr += pair.second;
    }

    ISInfoT<std::string> isTree(treeStr);

    IPCPartition part(partition);
    ISInfoDictionary isdict(part);
    isdict.checkin(server+"."+isname, isTree);
}

// method that publishes results
bool
publish(dqmf::Output& output, Parameters& params, const Algos& algos, ParamTypeMap& paramTypes, CachedResults& resCache,
    bool dohisto, unsigned updateInterval)
{
    ERS_DEBUG(1, "starting publishing of the results");

    // reshuffle parameters to avoid identical order of updates
    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(params.begin(), params.end(), g);

    unsigned repub = 0;
    for (const auto& parm_alg: params) {
        const std::string& param = parm_alg.first;
        const std::string& algo = parm_alg.second;

        TObject* hist = 0;
        if (dohisto) {

            // find/make histo maker
            HistoMaker hmaker = HistoMaker::factory(param, paramTypes[param]);

            // make new histogram
            hist = hmaker.make();
        }

        std::shared_ptr<dqm_core::Result> res = resCache[param];
        if (res) {
            // check if it is time to update;
            boost::posix_time::ptime now = boost::posix_time::microsec_clock::universal_time();
            boost::posix_time::time_duration d = now - res->universal_time_;
            if (d.total_seconds() >= long(updateInterval)) {
                res.reset();
            }
        }

        // generate result
        if (not res) {
            auto it = algos.find(algo);
            if (it == algos.end()) it = algos.find(std::string());
            if (it == algos.end()) {
                ERS_LOG("Could not find algorithm " << algo << " to create result");
            } else {
                res = it->second.makeResult(hist);
                resCache[param] = res;
                ERS_DEBUG(2, "publishing new result " << param << " algo=" << algo << " ntags=" << res->tags_.size() << " hname=" << (hist ? hist->GetName() : ""));
            }
        } else {
            ERS_DEBUG(2, "re-publishing result " << param);
            ++ repub;
        }

        // publish it
        output.publishResult(param, *res);
    }

    ERS_DEBUG(1, "published " << params.size() << " results, re-published: " << repub);

    return true;
}

}
