
#include <iostream>
#include <string>

#include "dqm_archive/common/RootSerializer.h"

#define BOOST_TEST_MODULE dqma_unit_root_serializer
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include "ers/ers.h"
#include "TH1.h"

using namespace dqm_archive;

// ==============================================================

BOOST_AUTO_TEST_CASE(test_instantiate)
{
    // unit test for instantiating RootSerializer

    {
        // Default version.
        RootSerializer ser;
    }

    // Default or known versions.
    for (int version: {-1, -100, 0}) {
        RootSerializer ser(version);
    }

    // Invalid versions.
    for (int version: {1, 2, 100}) {
        // need parentheses, otherwise it is same as `RootSerializer version`
        BOOST_CHECK_THROW((RootSerializer(version)), ers::Issue);
    }
}

BOOST_AUTO_TEST_CASE(test_v0)
{
    // unit test for instantiating RootSerializer
    RootSerializer ser(0);

    TH1* h = new TH1I("th1i", "Title", 100, 0., 100.);
    h->SetDirectory(nullptr);
    h->SetBit(kCanDelete);
    h->Fill(10.);
    h->Fill(20.);

    BOOST_TEST(h->GetEntries() == 2);

    TString serialized = ser.serialize(h);
    BOOST_TEST(serialized.Length() > 0);
    // first byte is a version number.
    BOOST_TEST(serialized[0] == 0);
    // encoded class name
    BOOST_TEST(serialized(1, 5) == "\04TH1I");

    TObject* tobj = ser.deserialize(serialized);
    BOOST_TEST(tobj != nullptr);
    TH1* h2 = dynamic_cast<TH1*>(tobj);
    BOOST_TEST(h2 != nullptr);
    h2->SetDirectory(0);
    h2->SetBit(kCanDelete);
    BOOST_TEST(h2->GetEntries() == 2);

}
