
// Unit tests for AsyncTask subclasses

#include <string>
#include <chrono>

#include "dqm_archive/archiver/AsyncTaskList.h"
#include "dqm_archive/archiver/AsyncTaskThread.h"
#include "dqm_archive/archiver/AsyncTaskProcess.h"
#include "dqm_archive/archiver/Exceptions.h"

#define BOOST_TEST_MODULE dqma_unit_async_task
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/mpl/vector.hpp>


using namespace dqm_archive;


namespace {

void sleep_thread(const std::chrono::milliseconds timeout, std::promise<bool> promise, bool value=true)
{
    AsyncTaskProcess::tsleep(timeout);
    promise.set_value(value);
}

// test sub-class for testing setThread() method
class TestSubclassAsyncTaskThread : public AsyncTaskThread {
public:
    TestSubclassAsyncTaskThread(bool value, std::chrono::milliseconds sleepTime)
        : AsyncTaskThread("TestSubclassAsyncTaskThread")
    {
        std::promise<bool> promise;
        auto future = promise.get_future();
        std::thread thread(sleep_thread, sleepTime, std::move(promise), value);
        setThread(std::move(thread), std::move(future));
    }
};

struct FixtureThread {

    std::unique_ptr<AsyncTask>
    makeSleepTask(bool value=true)
    {
        BOOST_TEST_MESSAGE("FixtureThread: will make new AsyncTaskThread sleeping for " << sleepTime.count() << " msec");
        std::promise<bool> promise;
        auto future = promise.get_future();
        std::thread thread(sleep_thread, sleepTime, std::move(promise), value);
        return std::make_unique<AsyncTaskThread>(std::move(thread), std::move(future), "sleep_thread");
    }

    const std::chrono::milliseconds sleepTime{100};
    const std::chrono::milliseconds shortDelay{1};
    const std::chrono::milliseconds regularDelay{120};
    const std::chrono::milliseconds longDelay{1000};
    const bool alwaysSuccess = false;
};

struct FixtureThreadSubclass {

    std::unique_ptr<AsyncTask>
    makeSleepTask(bool value=true)
    {
        BOOST_TEST_MESSAGE("FixtureThreadSubclass: will make new AsyncTaskThread sleeping for " << sleepTime.count() << " msec");
        return std::make_unique<TestSubclassAsyncTaskThread>(value, sleepTime);
    }

    const std::chrono::milliseconds sleepTime{100};
    const std::chrono::milliseconds shortDelay{1};
    const std::chrono::milliseconds regularDelay{120};
    const std::chrono::milliseconds longDelay{1000};
    const bool alwaysSuccess = false;
};

struct FixtureProcess {

    std::unique_ptr<AsyncTask>
    makeSleepTask(bool value=true)
    {
        std::vector<std::string> cmd;
        BOOST_TEST_MESSAGE("FixtureProcess: will make new AsyncTaskProcess sleeping for " << sleepTime.count() << " msec");
        if (value) {
            cmd.push_back("/usr/bin/sleep");
            cmd.push_back(std::to_string(sleepTime.count() / 1000.));
        } else {
            cmd.push_back("/bin/sh");
            cmd.push_back("-c");
            cmd.push_back("/usr/bin/sleep " + std::to_string(sleepTime.count() / 1000.) + "; false");
        }
        return std::make_unique<AsyncTaskProcess>(cmd, pollTime, "sleep_process");
    }

    const std::chrono::milliseconds sleepTime{200};
    const std::chrono::milliseconds shortDelay{10};
    const std::chrono::milliseconds regularDelay{800};
    const std::chrono::milliseconds longDelay{1000};
    const std::chrono::milliseconds pollTime{20};
    const bool alwaysSuccess = false;
};

struct FixtureThreadList : FixtureThread {

    std::unique_ptr<AsyncTask>
    makeSleepTask(bool value=true)
    {
        BOOST_TEST_MESSAGE("FixtureThreadList: will make new AsyncTaskThreadList sleeping for " << sleepTime.count() << " msec");
        auto taskList = std::make_unique<AsyncTaskList>("async_task_list");
        BOOST_TEST_MESSAGE("Add few tasks to the list");
        for (int i = 0; i != 4; ++ i) {
            taskList->addTask(FixtureThread::makeSleepTask(value));
        }
        return taskList;
    }
    const bool alwaysSuccess = true;
};


typedef boost::mpl::vector<FixtureThread, FixtureThreadSubclass, FixtureProcess, FixtureThreadList> Fixtures;

} // namespace

// ==============================================================

BOOST_FIXTURE_TEST_CASE_TEMPLATE(test_async_task, F, Fixtures, F)
{
    // unit test for AsyncTaskThread class

    for (auto retValue: {true, false}) {
        auto task = F::makeSleepTask(retValue);
        BOOST_CHECK(task->isRunning() == true);

        BOOST_TEST_MESSAGE("Short wait should return Timeout");
        auto status = task->waitFor(this->shortDelay);
        BOOST_CHECK(status == AsyncTask::WaitStatus::Timeout);

        BOOST_TEST_MESSAGE("Another short wait should return Timeout");
        status = task->waitFor(this->shortDelay);
        BOOST_CHECK(status == AsyncTask::WaitStatus::Timeout);

        auto expectStatus = retValue or this->alwaysSuccess ? AsyncTask::WaitStatus::Success : AsyncTask::WaitStatus::Failure;
        BOOST_TEST_MESSAGE("Long wait should return non-Timeout");
        status = task->waitFor(this->regularDelay);
        BOOST_CHECK(status == expectStatus);
        BOOST_CHECK(task->isRunning() == false);

        BOOST_TEST_MESSAGE("Long wait should return immediately");
        status = task->waitFor(this->longDelay);
        BOOST_CHECK(status == expectStatus);
    };

    for (auto retValue: {true, false}) {
        auto task = F::makeSleepTask(retValue);
        BOOST_CHECK(task->isRunning() == true);

        auto tp = std::chrono::steady_clock::now() + this->shortDelay;
        BOOST_TEST_MESSAGE("Short wait should return Timeout");
        auto status = task->waitUntil(tp);
        BOOST_CHECK(status == AsyncTask::WaitStatus::Timeout);

        BOOST_TEST_MESSAGE("Another short wait should return Timeout");
        tp = std::chrono::steady_clock::now() + this->shortDelay;
        status = task->waitUntil(tp);
        BOOST_CHECK(status == AsyncTask::WaitStatus::Timeout);

        auto expectStatus = retValue or this->alwaysSuccess ? AsyncTask::WaitStatus::Success : AsyncTask::WaitStatus::Failure;
        BOOST_TEST_MESSAGE("Long wait should return non-Timeout");
        tp = std::chrono::steady_clock::now() + this->regularDelay;
        status = task->waitUntil(tp);
        BOOST_CHECK(status == expectStatus);
        BOOST_CHECK(task->isRunning() == false);

        BOOST_TEST_MESSAGE("Long wait should return immediately");
        tp = std::chrono::steady_clock::now() + this->longDelay;
        status = task->waitUntil(tp);
        BOOST_CHECK(status == expectStatus);
    };

    for (auto retValue: {true, false}) {
        auto task = F::makeSleepTask(retValue);

        BOOST_TEST_MESSAGE("Taks should be running");
        BOOST_CHECK(task->isRunning() == true);

        auto expectStatus = retValue or this->alwaysSuccess ? AsyncTask::TaskStatus::Success : AsyncTask::TaskStatus::Failure;
        BOOST_TEST_MESSAGE("Wait until taks finishes");
        auto status = task->wait();
        BOOST_CHECK(status == expectStatus);

        BOOST_TEST_MESSAGE("Taks should be finished");
        BOOST_CHECK(task->isRunning() == false);
    }
}
