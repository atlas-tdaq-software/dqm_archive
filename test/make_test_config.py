#!/usr/bin/env tdaq_python

"""Script which generates simple partition config including DQM tree.
"""

import argparse
import os
import sys

import pm
import pm.project
from config.dal import module as dal_module

# Tag name
_tag_name = os.environ['CMTCONFIG']


def main():

    parser = argparse.ArgumentParser(description='Generate segment or partition for OLC2HLT')
    parser.add_argument('-p', '--partition', default='part_dqm_test', metavar='STRING',
                        help='The name of partition, def: %(default)s')
    parser.add_argument('-H', '--host', default='', metavar='HOST',
                        help='optional host name where to run applications, def: <empty>=localhost')
    parser.add_argument('-t', '--tag', default=_tag_name, metavar='STRING',
                        help='tag name, def: %(default)s')
    args = parser.parse_args()

    dal = dal_module('dal', 'daq/schema/core.schema.xml')
    dqm_dal = dal_module('dqm_dal', 'daq/schema/dqm.schema.xml')
    dqma_dal = dal_module('dqm_dal', 'daq/schema/dqm_archive.schema.xml')
    coca_dal = dal_module('dqm_dal', 'daq/schema/coca.schema.xml')
    args.tag_dal = dal.Tag(args.tag)

    repo = pm.project.Project("daq/sw/repository.data.xml")
    setup = pm.project.Project("daq/segments/setup.data.xml")
    dqm_algos = pm.project.Project("daq/sw/dqm-algorithms.data.xml")
    dqm_streams = pm.project.Project("daq/sw/dqm-default-streams.data.xml")
    common_env = pm.project.Project("daq/segments/common-environment.data.xml")

    # Computer
    args.comp = None
    if args.host:
        hw_tag = '-'.join(args.tag.split('-')[:2])
        args.comp = dal.Computer(args.host, HW_Tag=hw_tag)

    regions = _makeRegions(dqm_dal, dqm_algos)

    # DQM agent
    dqm_input = dqm_streams.getObject("DQInput", "DefaultInput")
    dqm_output = dqm_streams.getObject("DQOutput", "DefaultOutput")
    dqm_agent = dqm_dal.DQAgent("DQM_Agent",
                                Program=repo.getObject("Binary", "dqmf_agent"),
                                InterfaceName="rc/commander",
                                DQRegions=regions,
                                DQInputs=[dqm_input],
                                DQOutputs=[dqm_output])

    # archiver
    uprop = coca_dal.UnixProperties("dqm-uprop", ProcessUmask=2, NewDirGroup="zp", NewDirHasSetGid=True)
    dqma = dqma_dal.DQMAApplication("DQM_Archiver",
                                    Program=repo.getObject("Binary", "dqm_archiver"),
                                    InterfaceName="rc/commander",
                                    Lifetime="Boot_Shutdown",
                                    UpdateInterval=10,
                                    ArchiveHist=True,
                                    AddConfigHist=True,
                                    ServerName="DQMis",
                                    UnixProperties=uprop)

    # IS server
    iss = dal.InfrastructureApplication('DQMis',
                                        Parameters = '-s ',
                                        RestartParameters = '-s ',
                                        Program = repo.getObject("Binary", "is_server"),
                                        RestartableDuringRun = 1)

    # make DQM segment
    rcapp = dal.RunControlApplication('dqm_seqment_ctrl',
                                      Program=repo.getObject("Binary", "rc_controller"),
                                      InterfaceName="rc/controllable")
    dqm_seg = dal.Segment('DQM_Segment',
                          IsControlledBy=rcapp,
                          Applications=[dqm_agent],
                          Resources=[dqma],
                          Infrastructure=[iss])

    # make partition object
    part = dal.Partition(args.partition,
                         OnlineInfrastructure=setup.getObject("OnlineSegment", "setup"),
                         Segments=[dqm_seg],
                         DefaultHost=args.comp,
                         DefaultTags=[args.tag_dal],
                         Parameters=[common_env.getObject('VariableSet', 'CommonParameters')])

    includes = ['daq/schema/core.schema.xml',
                'daq/schema/dqm.schema.xml',
                'daq/schema/dqm_archive.schema.xml',
                "daq/segments/setup.data.xml",
                "daq/sw/dqm-algorithms.data.xml",
                "daq/sw/dqm-default-streams.data.xml",
                "daq/sw/tags.data.xml",
                "daq/sw/repository.data.xml",
                "daq/segments/common-environment.data.xml"]
    save_db = pm.project.Project(part.id + '.data.xml', includes)
    save_db.addObjects([part])


def _makeRegions(dqm_dal, dqm_algos):

    sys_regions = []
    for system in 'ABCDEFG':
        sub_regions = []
        for subsys in "IJKLMN":

            parameters = []
            for param in 'UVWXYZ':
                pname = '_'.join([system, subsys, param])
                param = dqm_dal.DQParameter(pname,
                                            DQAlgorithm=dqm_algos.getObject("DQAlgorithm", "Histogram_Empty"))
                parameters += [param]

            region = dqm_dal.DQRegion("SubSystem_" + system + "_" + subsys,
                                      DQParameters=parameters,
                                      DQSummaryMaker=dqm_algos.getObject("DQSummaryMaker", "WorstCaseSummary"))
            sub_regions += [region]

        region = dqm_dal.DQRegion("System" + system,
                                  DQRegions=sub_regions,
                                  DQSummaryMaker=dqm_algos.getObject("DQSummaryMaker", "WorstCaseSummary"))
        sys_regions += [region]

    top_region = dqm_dal.DQRegion("Top",
                                  DQRegions=sys_regions,
                                  DQSummaryMaker=dqm_algos.getObject("DQSummaryMaker", "WorstCaseSummary"))

    return [top_region]


if __name__ == "__main__":
    main()
