/**
 *  Unit test for DqmArchiverRoot.
 */

#include <algorithm>
#include <cstdlib>
#include <memory>
#include <string>
#include <sys/stat.h>

#include "dqm_archive/archiver/DqmArchiverRoot.h"
#include "dqm_archive/archiver/AsyncTaskProcess.h"
#include "dqm_archive/archiver/FileNameFactoryTemplate.h"
#include "dqm_archive/archiver/ILTSArchiver.h"
#include "dqma_test_utils.h"
#include "../src/client/DqmArchiveClientRootCoca.h"

#define BOOST_TEST_MODULE dqma_unit_archive_root
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

namespace tt = boost::test_tools;
using namespace dqm_archive;

// ==============================================================

namespace {

RTypes::Timestamp startTime_usec(int run) {
    return run*10000000;
}
RTypes::Timestamp endTime_usec(int run) {
    return run*10000000+9000000;
}

// special LTS archiver that generates delays
class SlowLTSArchiver : public ILTSArchiver {
public:
    SlowLTSArchiver(std::chrono::milliseconds delay) : m_delay(delay) {}

    void store(const std::string& path) override {
        BOOST_TEST_MESSAGE("SlowLTSArchiver: sleep for " << m_delay.count() << " msec");
        AsyncTaskProcess::tsleep(m_delay);
        BOOST_TEST_MESSAGE("SlowLTSArchiver: done sleeping");
    }

private:
    std::chrono::milliseconds m_delay;
};

struct Fixture {

    Fixture() {
        char dir_template[] = "/tmp/dqmauXXXXXX";
        tmpdir = mkdtemp(dir_template);
        BOOST_TEST_MESSAGE("created directory " << tmpdir);
        coca = std::make_unique<test::TestDBClient>();

        // make parameter tree and a list of parameter names
        paramTree = gen.makeParamTree();
        for (auto&& pair: paramTree) {
            paramNames.push_back(pair.first);
        }
    }

    ~Fixture() {
        // delete all files and tmp folder
        for (auto&& path: files) {
            BOOST_TEST_MESSAGE("removing file " << path);
            ::unlink(path.c_str());
        }
        BOOST_TEST_MESSAGE("removing directory " << tmpdir);
        ::rmdir(tmpdir.c_str());
    }

    // make archiver instance
    std::unique_ptr<DqmArchiverRoot> makeArchiver(int run, std::string const& tmpl);

    void storeResults(DqmArchiverRoot& archiver, int numResults,
                      RTypes::Timestamp startTime_usec, RTypes::Timestamp endTime_usec) {
        auto results = gen.dqm_results(numResults, startTime_usec, endTime_usec);
        for (unsigned i = 0; i != results.size(); ++ i) {
            auto paramName = paramNames[i % paramNames.size()];
            archiver.store(paramName, results[i]);
        }
    }

    // add new file name to the list of files created in a test
    void addFile(boost::filesystem::path const& path) {
        files.push_back(path.string());
    }

    unsigned maxFileTimeIntervalSec = 0;
    unsigned maxFileResultCount = 0;
    std::chrono::milliseconds finalizeTimeout{0};
    std::chrono::milliseconds finalizeDelay{0};
    std::string tmpdir;    // folder for ROOT files to be removed
    std::vector<std::string> files;  // files to remove
    std::unique_ptr<test::TestDBClient> coca;
    test::ResultGenerator gen;
    std::map<std::string, std::string> paramTree;
    std::vector<std::string> paramNames;
    std::mutex rootMutex;
};

// extenstion of FileNameFactoryTemplate which records names of the files generated
class TestFileNameFactory : public FileNameFactoryTemplate {
public:
    TestFileNameFactory(const std::string& templateString,
                        const boost::filesystem::path& dir,
                        const std::string& partition,
                        unsigned run,
                        Fixture* fixture) :
        FileNameFactoryTemplate(templateString, dir, partition, run),
        m_fixture(fixture)
    {}

    boost::filesystem::path makeFileName() override {
        auto path = FileNameFactoryTemplate::makeFileName();
        m_fixture->addFile(path);
        return path;
    }

private:
    Fixture* m_fixture;
};

std::unique_ptr<DqmArchiverRoot> Fixture::makeArchiver(int run, std::string const& tmpl)
{
    auto fnameFactory = std::make_unique<TestFileNameFactory>(tmpl, tmpdir, "PART", run, this);
    const unsigned checkpointMinutes = 60;
    std::unique_ptr<ILTSArchiver> ltsArchiver;
    if (finalizeDelay.count() != 0) {
        ltsArchiver = std::make_unique<SlowLTSArchiver>(finalizeDelay);
    }
    return std::make_unique<DqmArchiverRoot>(std::move(fnameFactory), std::move(ltsArchiver),
                                             paramTree, checkpointMinutes, maxFileTimeIntervalSec,
                                             maxFileResultCount, finalizeTimeout, rootMutex);
}

}

BOOST_FIXTURE_TEST_CASE(test_legacy, Fixture)
{
    // test case for legacy data files with single archive file for the whole run

    int const numResults = 100;

    int run = 999;
    std::string tmpl = "pfx-{partition}-{run}.root";
    auto archiver = makeArchiver(run, tmpl);

    BOOST_TEST_MESSAGE("store results");
    storeResults(*archiver, numResults, ::startTime_usec(run), ::endTime_usec(run));

    BOOST_TEST_MESSAGE("finalize");
    archiver->finish();

    // should have made a single file
    BOOST_TEST(files.size() == 1);

    for (auto&& path: files) {
        coca->registerFile(path);
    }

    // instantiate DQMA client
    DqmArchiveClientRootCoca client(std::move(coca));
    BOOST_TEST(client.runCount() == 1);

    // check that we can retrieve parameter names
    std::vector<std::string> params;
    client.parameters(params, run);
    // NB: potentially order may differ, though it is the same for now
    BOOST_TEST(params == paramNames, tt::per_element());

    // get list of children for each parameter
    for (auto&& paramName: params) {
        BOOST_TEST_MESSAGE("check children for parameter " << paramName);
        std::vector<ParamInfo> children;
        client.childParameters(children, run, paramName);
        std::vector<ParamInfo> expect_children = gen.childParameters(paramName);
        BOOST_TEST(children.size() == expect_children.size());
    }
}

BOOST_FIXTURE_TEST_CASE(test_multifile_finaltimeout, Fixture)
{
    // test case for new multi-file implementation with the limit on time.

    // test for finalization delay, this should not crash

    // timing is sensitive to how fast sub-process can start, it can take
    // couple of seconds or longer depending on wheather
    this->finalizeTimeout = std::chrono::milliseconds(3000);
    this->finalizeDelay = std::chrono::milliseconds(4000);

    int const numResults = 100;

    int run = 999;
    std::string tmpl = "pfx-{partition}-{run}-{seq}.root";
    auto archiver = makeArchiver(run, tmpl);

    BOOST_TEST_MESSAGE("store results");
    storeResults(*archiver, numResults, ::startTime_usec(run), ::endTime_usec(run));

    BOOST_TEST_MESSAGE("finalize");
    archiver->finish();
    archiver.reset();

    // should have made three files
    std::vector<std::string> expectedFiles = {tmpdir+"/pfx-PART-999-0.root"};
    BOOST_TEST(files == expectedFiles, tt::per_element());
}

BOOST_FIXTURE_TEST_CASE(test_multifile_numresults, Fixture)
{
    // test case for new multi-file implementation with the limit on number of results.

    // The issue here is that DqmArchiverRoot only writes/flushes data by 1k results at
    // once (after reaching 11k result in buffer), and limits are checked at flush time,
    // so we need larger number of results.

    // with this settings first two files should have 2k results, third file - 11k
    int const numResults = 14500;
    this->maxFileResultCount = 1500;

    int run = 999;
    std::string tmpl = "pfx-{partition}-{run}-{seq}.root";
    auto archiver = makeArchiver(run, tmpl);

    BOOST_TEST_MESSAGE("store results");
    storeResults(*archiver, numResults, ::startTime_usec(run), ::endTime_usec(run));

    BOOST_TEST_MESSAGE("finalize");
    archiver->finish();

    // should have made three files
    std::vector<std::string> expectedFiles = {tmpdir+"/pfx-PART-999-0.root",
                                              tmpdir+"/pfx-PART-999-1.root",
                                              tmpdir+"/pfx-PART-999-2.root"};
    BOOST_TEST(files == expectedFiles, tt::per_element());

    for (auto&& path: files) {
        coca->registerFile(path);
    }

    // instantiate DQMA client
    DqmArchiveClientRootCoca client(std::move(coca));
    BOOST_TEST(client.runCount() == 1);

    // the rest of multi-file reading is tested in dqma_unit_client_coca
}

BOOST_FIXTURE_TEST_CASE(test_multifile_timelimit, Fixture)
{
    // test case for new multi-file implementation with the limit on time.

    // The issue here is that DqmArchiverRoot only writes/flushes data by 1k results at
    // once (after reaching 11k result in buffer), and limits are checked at flush time,
    // so we need larger number of results.

    // with this settings we should get one file per flush (depends also on
    // startTime_usec/endTime_usec)
    int const numResults = 14000;
    this->maxFileTimeIntervalSec = 2;

    int run = 999;
    std::string tmpl = "pfx-{partition}-{run}-{seq}.root";
    auto archiver = makeArchiver(run, tmpl);

    BOOST_TEST_MESSAGE("store results");
    storeResults(*archiver, numResults, ::startTime_usec(run), ::endTime_usec(run));

    BOOST_TEST_MESSAGE("finalize");
    archiver->finish();

    // should have made three files
    std::vector<std::string> expectedFiles = {tmpdir+"/pfx-PART-999-0.root",
                                              tmpdir+"/pfx-PART-999-1.root"};
    BOOST_TEST(files == expectedFiles, tt::per_element());

    for (auto&& path: files) {
        coca->registerFile(path);
    }

    // instantiate DQMA client
    DqmArchiveClientRootCoca client(std::move(coca));
    BOOST_TEST(client.runCount() == 1);

    // the rest of multi-file reading is tested in dqma_unit_client_coca
}
