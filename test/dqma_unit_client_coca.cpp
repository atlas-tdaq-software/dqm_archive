/**
 *  Unit test for DqmArchiveClientRootCoca.
 */

#include <algorithm>
#include <cstdlib>
#include <memory>
#include <string>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>

#include "../src/client/DqmArchiveClientRootCoca.h"
#include "dqma_test_utils.h"
#include "TFile.h"

#define BOOST_TEST_MODULE dqma_unit_client_coca
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

namespace tt = boost::test_tools;
using namespace dqm_archive;

// ==============================================================

namespace {

RTypes::Timestamp startTime_usec(int run) {
    return run*10000;
}
RTypes::Timestamp endTime_usec(int run) {
    return run*10000+9000;
}

RTypes::Timestamp startTime_usec(int run, int chunk) {
    return (run*1000ULL + chunk) * 1000000ULL;
}
RTypes::Timestamp endTime_usec(int run, int chunk) {
    return (run*1000ULL + chunk + 1) * 1000000ULL - 1;
}


struct Fixture {

    Fixture() {
        char dir_template[] = "/tmp/dqmauXXXXXX";
        tmpdir = mkdtemp(dir_template);
        BOOST_TEST_MESSAGE("created directory " << tmpdir);
        coca = std::make_unique<test::TestDBClient>();
    }

    ~Fixture() {
        for (auto&& path: files) {
            BOOST_TEST_MESSAGE("removing file " << path);
            ::unlink(path.c_str());
        }
        BOOST_TEST_MESSAGE("removing directory " << tmpdir);
        ::rmdir(tmpdir.c_str());
    }

    void makeFile(std::string path, RTypes::Timestamp startTime_usec, RTypes::Timestamp endTime_usec) {
        path = tmpdir + "/" + path;
        BOOST_TEST_MESSAGE("making new file " << path);
        auto file = gen.makeTestRootFile(path, numResults, startTime_usec, endTime_usec);
        file->Close();
        files.push_back(path);
        coca->registerFile(path);
    }

    std::vector<Result>
    expect_results_one(std::string paramName, int run) {
        return gen.results(paramName, numResults, ::startTime_usec(run), ::endTime_usec(run));
    }

    std::vector<Result>
    expect_results_mf(std::string paramName, int run, int numChunks) {
        std::vector<Result> results;
        for (int chunk = 0; chunk != numChunks; ++ chunk) {
            auto chunk_results = gen.results(paramName, numResults, ::startTime_usec(run, chunk), ::endTime_usec(run, chunk));
            results.insert(results.end(), chunk_results.begin(), chunk_results.end());
        }
        return results;
    }


    int const numResults = 100;
    std::string tmpdir;    // folder for ROOT files to be removed
    std::vector<std::string> files;  // files to remove
    std::unique_ptr<test::TestDBClient> coca;
    test::ResultGenerator gen;
};

}

BOOST_FIXTURE_TEST_CASE(test_legacy, Fixture)
{
    // test case for legacy data files with single archive file for the whole run,
    // file names look like dqm_results-PARTITION-0000001234.root

    // make few ROOT files, run numbers are not always ordered
    std::vector<int> runNumbers = {123, 124, 1500000000, 345};
    for (int run: runNumbers) {
        char buf[64];
        snprintf(buf, sizeof buf, "dqm_results-PART-%010d.root", run);
        this->makeFile(buf, ::startTime_usec(run), ::endTime_usec(run));
    }

    // instantiate DQMA client
    DqmArchiveClientRootCoca client(std::move(coca));

    BOOST_TEST(client.runCount() == runNumbers.size());

    // pull all runs
    std::vector<RunInfo> runs;
    client.runs(runs, 100);
    BOOST_TEST(runs.size() == runNumbers.size());
    for (unsigned i = 0; i != std::min(runs.size(), runNumbers.size()); ++ i) {
        BOOST_TEST(runs[i].run() == runNumbers[runNumbers.size() - i - 1]);
        BOOST_TEST(runs[i].partition() == "PART");
    }

    // pull subset of runs
    runs.clear();
    client.runs(runs, 2);
    BOOST_TEST(runs.size() == 2);
    if (runs.size() == 2) {
        for (unsigned i = 0; i < 2; ++ i) {
            BOOST_TEST(runs[i].run() == runNumbers[runNumbers.size() - i - 1]);
            BOOST_TEST(runs[i].partition() == "PART");
        }
    }

    // pull individual runs, newer runs are returned first
    for (unsigned i = 0; i < runNumbers.size(); ++ i) {
        runs.clear();
        client.runs(runs, 1, i);
        BOOST_TEST(runs.size() == 1);
        BOOST_TEST(runs[0].run() == runNumbers[runNumbers.size() - i - 1]);
        BOOST_TEST(runs[0].partition() == "PART");
    }

    // check that we can retrieve parameter names
    std::vector<std::string> params;
    client.parameters(params, runNumbers[0]);
    std::vector<std::string> expect_params = gen.parameters();
    BOOST_TEST(params == expect_params, tt::per_element());

    // get list of children for each parameter
    for (auto&& paramName: params) {
        BOOST_TEST_MESSAGE("check children for parameter " << paramName);
        std::vector<ParamInfo> children;
        int run = runNumbers[0];
        client.childParameters(children, run, paramName);
        std::vector<ParamInfo> expect_children = gen.childParameters(paramName);
        BOOST_TEST(children.size() == expect_children.size());
    }

    // get results for each parameter
    for (auto&& paramName: params) {
        BOOST_TEST_MESSAGE("getting results for parameter " << paramName);
        int run = runNumbers[0];
        std::vector<Result> results;
        client.results(results, run, paramName);
        std::vector<Result> expect_results = expect_results_one(paramName, run);
        BOOST_TEST(results.size() == expect_results.size());
        // compare status for each result
        auto cmp_status = [](const Result& lhs, const Result& rhs) -> bool { return lhs.status() == rhs.status(); };
        BOOST_TEST(std::equal(results.begin(), results.end(), expect_results.begin(), cmp_status));
        // compare times for each result
        auto cmp_time = [](const Result& lhs, const Result& rhs) -> bool { return lhs.time() == rhs.time(); };
        BOOST_TEST(std::equal(results.begin(), results.end(), expect_results.begin(), cmp_time));
        // compare tags for each result
        auto cmp_tags = [](const Result& lhs, const Result& rhs) -> bool {
            auto&& lhs_tags = lhs.tags();
            auto&& rhs_tags = rhs.tags();
            return std::equal(lhs_tags.begin(), lhs_tags.end(), rhs_tags.begin(), rhs_tags.end());
        };
        BOOST_TEST(std::equal(results.begin(), results.end(), expect_results.begin(), cmp_tags));
        // compare histo flag for each result
        auto cmp_histo = [](const Result& lhs, const Result& rhs) -> bool { return lhs.hasHistograms() == rhs.hasHistograms(); };
        BOOST_TEST(std::equal(results.begin(), results.end(), expect_results.begin(), cmp_histo));
    }

    // get child results for each parameter
    for (auto&& parentName: params) {
        BOOST_TEST_MESSAGE("getting child results for parameter " << parentName);
        int run = runNumbers[0];
        std::map<std::string, std::vector<Result>> resultMap;
        client.childResults(resultMap, run, parentName);
        for (auto&& pair: resultMap) {
            auto&& paramName = pair.first;
            auto&& results = pair.second;
            BOOST_TEST_MESSAGE("chicking child results for parameter " << paramName);
            std::vector<Result> expect_results = expect_results_one(paramName, run);
            BOOST_TEST(results.size() == expect_results.size());
            // compare status for each result
            auto cmp_status = [](const Result& lhs, const Result& rhs) -> bool { return lhs.status() == rhs.status(); };
            BOOST_TEST(std::equal(results.begin(), results.end(), expect_results.begin(), cmp_status));
            // compare times for each result
            auto cmp_time = [](const Result& lhs, const Result& rhs) -> bool { return lhs.time() == rhs.time(); };
            BOOST_TEST(std::equal(results.begin(), results.end(), expect_results.begin(), cmp_time));
            if (paramName == parentName) {
                // tags are only returned for parent, not for children
                auto cmp_tags = [](const Result& lhs, const Result& rhs) -> bool {
                    auto&& lhs_tags = lhs.tags();
                    auto&& rhs_tags = rhs.tags();
                    return std::equal(lhs_tags.begin(), lhs_tags.end(), rhs_tags.begin(), rhs_tags.end());
                };
                BOOST_TEST(std::equal(results.begin(), results.end(), expect_results.begin(), cmp_tags));
            } else {
                for (auto&& result: results) {
                    BOOST_TEST(result.tags().empty());
                }
            }
            // compare histo flag for each result
            auto cmp_histo = [](const Result& lhs, const Result& rhs) -> bool { return lhs.hasHistograms() == rhs.hasHistograms(); };
            BOOST_TEST(std::equal(results.begin(), results.end(), expect_results.begin(), cmp_histo));
        }
    }
}

BOOST_FIXTURE_TEST_CASE(test_multi_file, Fixture)
{
    // test case for implementation that supports multiple files per run,
    // file names look like dqm_results-PARTITION-0000001234-1534217654.root
    // with first number being a run number and second number a timestamp.

    int const numChunksPerRun = 3;

    // make few ROOT files, run numbers are not always ordered
    std::vector<int> runNumbers = {123, 124, 1500000000, 345};
    std::vector<std::string> files;
    for (int run: runNumbers) {
        for (int chunk = 0; chunk != numChunksPerRun; ++ chunk) {
            char buf[64];
            unsigned long ts = ::startTime_usec(run, chunk) / 1000000;
            snprintf(buf, sizeof buf, "dqm_results-PART-%010d-%010lu.root", run, ts);
            this->makeFile(buf, ::startTime_usec(run, chunk), ::endTime_usec(run, chunk));
        }
    }

    // instantiate DQMA client
    DqmArchiveClientRootCoca client(std::move(coca));

    BOOST_TEST(client.runCount() == runNumbers.size());

    // pull all runs
    std::vector<RunInfo> runs;
    client.runs(runs, 100);
    BOOST_TEST(runs.size() == runNumbers.size());
    for (unsigned i = 0; i != std::min(runs.size(), runNumbers.size()); ++ i) {
        BOOST_TEST(runs[i].run() == runNumbers[runNumbers.size() - i - 1]);
        BOOST_TEST(runs[i].partition() == "PART");
    }

    // pull subset of runs
    runs.clear();
    client.runs(runs, 2);
    BOOST_TEST(runs.size() == 2);
    if (runs.size() == 2) {
        for (unsigned i = 0; i < 2; ++ i) {
            BOOST_TEST(runs[i].run() == runNumbers[runNumbers.size() - i - 1]);
            BOOST_TEST(runs[i].partition() == "PART");
        }
    }

    // pull individual runs, newer runs are returned first
    for (unsigned i = 0; i < runNumbers.size(); ++ i) {
        runs.clear();
        client.runs(runs, 1, i);
        BOOST_TEST(runs.size() == 1);
        BOOST_TEST(runs[0].run() == runNumbers[runNumbers.size() - i - 1]);
        BOOST_TEST(runs[0].partition() == "PART");
    }

    // check that we can retrieve parameter names
    std::vector<std::string> params;
    client.parameters(params, runNumbers[0]);
    std::vector<std::string> expect_params = gen.parameters();
    BOOST_TEST(params == expect_params, tt::per_element());

    // get list of children for each parameter
    for (auto&& paramName: params) {
        BOOST_TEST_MESSAGE("check children for parameter " << paramName);
        std::vector<ParamInfo> children;
        int run = runNumbers[0];
        client.childParameters(children, run, paramName);
        std::vector<ParamInfo> expect_children = gen.childParameters(paramName);
        BOOST_TEST(children.size() == expect_children.size());
    }

    // get results for each parameter
    for (auto&& paramName: params) {
        BOOST_TEST_MESSAGE("getting results for parameter " << paramName);
        int run = runNumbers[0];
        std::vector<Result> results;
        client.results(results, run, paramName);
        std::vector<Result> expect_results = expect_results_mf(paramName, run, numChunksPerRun);
        BOOST_TEST(results.size() == expect_results.size());
        // compare status for each result
        auto cmp_status = [](const Result& lhs, const Result& rhs) -> bool { return lhs.status() == rhs.status(); };
        BOOST_TEST(std::equal(results.begin(), results.end(), expect_results.begin(), cmp_status));
        // compare times for each result
        auto cmp_time = [](const Result& lhs, const Result& rhs) -> bool { return lhs.time() == rhs.time(); };
        BOOST_TEST(std::equal(results.begin(), results.end(), expect_results.begin(), cmp_time));
        // compare tags for each result
        auto cmp_tags = [](const Result& lhs, const Result& rhs) -> bool {
            auto&& lhs_tags = lhs.tags();
            auto&& rhs_tags = rhs.tags();
            return std::equal(lhs_tags.begin(), lhs_tags.end(), rhs_tags.begin(), rhs_tags.end());
        };
        BOOST_TEST(std::equal(results.begin(), results.end(), expect_results.begin(), cmp_tags));
        // compare histo flag for each result
        auto cmp_histo = [](const Result& lhs, const Result& rhs) -> bool { return lhs.hasHistograms() == rhs.hasHistograms(); };
        BOOST_TEST(std::equal(results.begin(), results.end(), expect_results.begin(), cmp_histo));
    }

    // get child results for each parameter
    for (auto&& parentName: params) {
        BOOST_TEST_MESSAGE("getting child results for parameter " << parentName);
        int run = runNumbers[0];
        std::map<std::string, std::vector<Result>> resultMap;
        client.childResults(resultMap, run, parentName);
        for (auto&& pair: resultMap) {
            auto&& paramName = pair.first;
            auto&& results = pair.second;
            BOOST_TEST_MESSAGE("chicking child results for parameter " << paramName);
            std::vector<Result> expect_results = expect_results_mf(paramName, run, numChunksPerRun);
            BOOST_TEST(results.size() == expect_results.size());
            // compare status for each result
            auto cmp_status = [](const Result& lhs, const Result& rhs) -> bool { return lhs.status() == rhs.status(); };
            BOOST_TEST(std::equal(results.begin(), results.end(), expect_results.begin(), cmp_status));
            // compare times for each result
            auto cmp_time = [](const Result& lhs, const Result& rhs) -> bool { return lhs.time() == rhs.time(); };
            BOOST_TEST(std::equal(results.begin(), results.end(), expect_results.begin(), cmp_time));
            if (paramName == parentName) {
                // tags are only returned for parent, not for children
                auto cmp_tags = [](const Result& lhs, const Result& rhs) -> bool {
                    auto&& lhs_tags = lhs.tags();
                    auto&& rhs_tags = rhs.tags();
                    return std::equal(lhs_tags.begin(), lhs_tags.end(), rhs_tags.begin(), rhs_tags.end());
                };
                BOOST_TEST(std::equal(results.begin(), results.end(), expect_results.begin(), cmp_tags));
            } else {
                for (auto&& result: results) {
                    BOOST_TEST(result.tags().empty());
                }
            }
            // compare histo flag for each result
            auto cmp_histo = [](const Result& lhs, const Result& rhs) -> bool { return lhs.hasHistograms() == rhs.hasHistograms(); };
            BOOST_TEST(std::equal(results.begin(), results.end(), expect_results.begin(), cmp_histo));
        }
    }
}
