
#include <string>

#include "dqm_archive/archiver/FileNameFactoryTemplate.h"
#include "dqm_archive/archiver/Exceptions.h"

#define BOOST_TEST_MODULE dqma_unit_fname_factory
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>


using namespace dqm_archive;

// ==============================================================

BOOST_AUTO_TEST_CASE(test_factory_template)
{
    // unit test for FileNameFactoryTemplate class

    boost::filesystem::path folder = "/tmp";

    {
        // no ids in template
        std::string tmpl = "no-ids.here";
        FileNameFactoryTemplate factory(tmpl, folder, "PART", 0);
        BOOST_TEST(factory.makeFileName() == "/tmp/" + tmpl);
    }
    {
        // one id
        std::string tmpl = "pfx-{run}.ext";
        FileNameFactoryTemplate factory(tmpl, folder, "PART", 999);
        BOOST_TEST(factory.makeFileName() == "/tmp/pfx-999.ext");
    }
    {
        // one id at the start
        std::string tmpl = "{run}.ext";
        FileNameFactoryTemplate factory(tmpl, folder, "PART", 999);
        BOOST_TEST(factory.makeFileName() == "/tmp/999.ext");
    }
    {
        // one id at the end
        std::string tmpl = "pfx-{run}";
        FileNameFactoryTemplate factory(tmpl, folder, "PART", 999);
        BOOST_TEST(factory.makeFileName() == "/tmp/pfx-999");
    }
    {
        // few ids
        std::string tmpl = "pfx-{partition}-{run}.root";
        FileNameFactoryTemplate factory(tmpl, folder, "PART", 999);
        BOOST_TEST(factory.makeFileName() == "/tmp/pfx-PART-999.root");
    }
    {
        // with counters
        std::string tmpl = "pfx-{partition}-{run}-{seq}.root";
        FileNameFactoryTemplate factory(tmpl, folder, "PART", 999);
        BOOST_TEST(factory.makeFileName() == "/tmp/pfx-PART-999-0.root");
        BOOST_TEST(factory.makeFileName() == "/tmp/pfx-PART-999-1.root");
        BOOST_TEST(factory.makeFileName() == "/tmp/pfx-PART-999-2.root");
    }
    {
        // with two counters
        std::string tmpl = "pfx-{partition}-{run}-{seq}-{seq1}.root";
        FileNameFactoryTemplate factory(tmpl, folder, "PART", 999);
        BOOST_TEST(factory.makeFileName() == "/tmp/pfx-PART-999-0-1.root");
        BOOST_TEST(factory.makeFileName() == "/tmp/pfx-PART-999-1-2.root");
        BOOST_TEST(factory.makeFileName() == "/tmp/pfx-PART-999-2-3.root");
    }
    {
        // with padding
        std::string tmpl = "pfx-{partition:8}-{run:10}-{seq:3}-{seq1:3}.root";
        FileNameFactoryTemplate factory(tmpl, folder, "PART", 999);
        BOOST_TEST(factory.makeFileName() == "/tmp/pfx-0000PART-0000000999-000-001.root");
        BOOST_TEST(factory.makeFileName() == "/tmp/pfx-0000PART-0000000999-001-002.root");
    }
    {
        // timestamp impossible to test exactly, just make sure that length is OK
        std::string tmpl = "pfx-{partition}-{run:10}-{timesec:10}.root";
        FileNameFactoryTemplate factory(tmpl, folder, "PART", 999);
        auto fname = factory.makeFileName().string();
        BOOST_TEST(fname.size() == 40);
        BOOST_TEST(fname.substr(0, 25) == "/tmp/pfx-PART-0000000999-");
        BOOST_TEST(fname.substr(35) == ".root");
    }
    {
        // unknown id should raise an exception
        BOOST_CHECK_THROW(FileNameFactoryTemplate("pfx-{blah}.root", folder, "PART", 999), errors::FileNameTemplateIdError);
        BOOST_CHECK_THROW(FileNameFactoryTemplate("pfx-{blah:8}.root", folder, "PART", 999), errors::FileNameTemplateIdError);
    }
    {
        // some "incomplete" templates, these are not errors
        std::string tmpl = "pfx-run}.root";
        FileNameFactoryTemplate factory(tmpl, folder, "PART", 0);
        BOOST_TEST(factory.makeFileName() == "/tmp/" + tmpl);
    }
    {
        // some "incomplete" templates, these are not errors
        std::string tmpl = "pfx-{run.root";
        FileNameFactoryTemplate factory(tmpl, folder, "PART", 0);
        BOOST_TEST(factory.makeFileName() == "/tmp/" + tmpl);
    }
    {
        // some "incomplete" templates, these are not errors
        std::string tmpl = "pfx-{partition-run}.root";
        FileNameFactoryTemplate factory(tmpl, folder, "PART", 0);
        BOOST_TEST(factory.makeFileName() == "/tmp/" + tmpl);
    }
}
