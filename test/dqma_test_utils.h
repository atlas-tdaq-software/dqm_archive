#ifndef DQM_ARCHIVE_DQMA_TEST_UTILS_H
#define DQM_ARCHIVE_DQMA_TEST_UTILS_H

#include <map>
#include <memory>
#include <string>

#include "coca/client/IDBClient.h"
#include "dqm_archive/client/ParamInfo.h"
#include "dqm_archive/client/Result.h"
#include "dqm_archive/common/RTypes.h"
#include "dqm_core/Result.h"

class TFile;

namespace dqm_archive {
namespace test {

class ResultGenerator {
public:

    /**
     * Create new file with some test data at specified location, optionally make
     * ResultIndex tree.
     *
     * @param path        Location of the file, empty string will make a random file name
     * @param numResults  Number of results to generate
     * @param startTime_usec Time in UNIX microseconds for the first generated result
     * @param endTime_usec Time in UNIX microseconds for the last generated result
     * @param makeIndex   If false then skip generation of result index
     */
    std::unique_ptr<TFile> makeTestRootFile(const std::string& path,
                                            int numResults,
                                            RTypes::Timestamp startTime_usec,
                                            RTypes::Timestamp endTime_usec,
                                            bool makeIndex=true) const;

    /**
     * Make parameter tree as accepted by storeConfig() methods
     */
    std::map<std::string, std::string> makeParamTree() const;

    /**
     * Return list of the parameter names that should exist in the file
     * created by above method. Parameters are ordered by name, same way
     * as they are stored in Parameters tree.
     */
    std::vector<std::string> parameters() const;

    /**
     * Return parameter info for all direct children of a parameter.
     */
    std::vector<ParamInfo> childParameters(const std::string& parent) const;

    /**
     * Return results for given parameter, other parameters are the same
     * as for makeTestRootFile() method.
     */
    std::vector<Result> results(const std::string& param,
                                int numResults,
                                RTypes::Timestamp startTime_usec,
                                RTypes::Timestamp endTime_usec) const;

    /**
     * Return list of dqm_core results.
     */
    std::vector<std::shared_ptr<dqm_core::Result>>
    dqm_results(int numResults,
                RTypes::Timestamp startTime_usec,
                RTypes::Timestamp endTime_usec) const;
};

/**
 * Special implementation of coca::IDBClient used for tests.
 *
 * Note that only the methods used by DqmArchiveClientRootCoca are
 * implemented here - countFiles(), files(), fileLocations(). There are
 * also assumption about parameters that are being passed to those methods
 * that simplify implementation but depend on DqmArchiveClientRootCoca.
 */
class TestDBClient : public daq::coca::IDBClient {
public:

    /**
     * @param fileLocations  List of file locations (URL style or local path)
     * @param dataset        Nae of a dataset for these files
     */
    TestDBClient(std::string const& dataset="DQM-Archive")
        : m_fileLocations(),
          m_dataset(dataset)
    {
    }

    void registerFile(std::string const& fileLocation) {
        // newer files are at the beginning of the list
        m_fileLocations.insert(m_fileLocations.begin(), fileLocation);
    }

    std::vector<std::string> datasets() override;

    std::vector<daq::coca::RemoteArchive> archives(const std::string& dataset = std::string(),
                                                   const std::string& archive = std::string(),
                                                   const time_point& since = time_point(),
                                                   const time_point& until = time_point()) override;

    std::vector<daq::coca::RemoteArchive> archives(const std::string& dataset,
                                                   const std::string& archive,
                                                   unsigned count,
                                                   unsigned skip = 0) override;

    unsigned countFiles(const std::string& dataset = std::string(),
                        const std::string& archive = std::string(),
                        const std::string& file = std::string()) override;

    std::vector<daq::coca::RemoteFile> files(const std::string& dataset = std::string(),
                                             const std::string& archive = std::string(),
                                             const std::string& file = std::string(),
                                             const time_point& since = time_point(),
                                             const time_point& until = time_point()) override;

    std::vector<daq::coca::RemoteFile> files(const std::string& dataset,
                                             const std::string& archive,
                                             const std::string& file,
                                             unsigned count,
                                             unsigned skip = 0) override;

    std::vector<std::string> fileLocations(const std::string& file,
                                           const std::string& dataset,
                                           Location /* location */) override;

    std::vector<std::string> fileLocations(const std::string& file,
                                           const std::string& dataset) override;

    daq::coca::Metadata metadata() override;

private:
    std::vector<std::string> m_fileLocations;
    std::string m_dataset;
};

}} // namespace dqm_archive::test

#endif // DQM_ARCHIVE_DQMA_TEST_UTILS_H
