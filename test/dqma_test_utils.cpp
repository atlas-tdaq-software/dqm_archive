
#include <cstdlib>
#include <stdexcept>
#include <fnmatch.h>
#include <unistd.h>

#include "dqma_test_utils.h"

#include "TFile.h"
#include "dqm_archive/archiver/DqmaRootIndexer.h"
#include "dqm_archive/common/RTypes.h"
#include "dqm_archive/common/TTreeSequence.h"

using namespace dqm_archive;

namespace {

// Parameter tree
struct ParamPair {
    const char* parentName;
    const char* childName;
};

ParamPair g_allParams[] = {
    {"", "RegionA"},
    {"", "RegionB"},
    {"", "RegionC"},

    {"RegionA", "RegionA1"},
    {"RegionA", "RegionA2"},
    {"RegionA", "RegionA3"},

    {"RegionB", "RegionB1"},
    {"RegionB", "RegionB2"},
    {"RegionB", "RegionB3"},

    {"RegionC", "RegionC1"},
    {"RegionC", "RegionC2"},
    {"RegionC", "RegionC3"},

    {"RegionA1", "ParamA1_1"},
    {"RegionA1", "ParamA1_2"},
    {"RegionA1", "ParamA1_3"},

    {"RegionA2", "ParamA2_1"},
    {"RegionA2", "ParamA2_2"},
    {"RegionA2", "ParamA2_3"},

    {"RegionA3", "ParamA3_1"},
    {"RegionA3", "ParamA3_2"},
    {"RegionA3", "ParamA3_3"},

    {"RegionB1", "ParamB1_1"},
    {"RegionB1", "ParamB1_2"},
    {"RegionB1", "ParamB1_3"},

    {"RegionB2", "ParamB2_1"},
    {"RegionB2", "ParamB2_2"},
    {"RegionB2", "ParamB2_3"},

    {"RegionB3", "ParamB3_1"},
    {"RegionB3", "ParamB3_2"},
    {"RegionB3", "ParamB3_3"},

    {"RegionC1", "ParamC1_1"},
    {"RegionC1", "ParamC1_2"},
    {"RegionC1", "ParamC1_3"},

    {"RegionC2", "ParamC2_1"},
    {"RegionC2", "ParamC2_2"},
    {"RegionC2", "ParamC2_3"},

    {"RegionC3", "ParamC3_1"},
    {"RegionC3", "ParamC3_2"},
    {"RegionC3", "ParamC3_3"}
};

unsigned g_numParams = sizeof g_allParams / sizeof g_allParams[0];

std::map<std::string, RTypes::ParmId> _initParm2id() {
    std::map<std::string, RTypes::ParmId> parm2id;
    for (auto&& pair: ::g_allParams) {
        parm2id[pair.childName] = 0;
    }
    unsigned counter = 0;
    for (auto&& pair: parm2id) {
        pair.second = counter ++;
    }
    return parm2id;
}

// maps parameter name to its ID (index of child in g_allParams)
std::map<std::string, RTypes::ParmId> const g_parm2id = _initParm2id();


// returns total number of parameters
int _makeParamTree()
{
    // parameters tree
    auto parmTree = std::make_unique<TTree>("Parameters", "List of DQParameters, their names and parent relations.");
    auto parmSeq = TTreeSequenceFactory<TString*>::create(parmTree.get(), "name", 128*1024);
    auto parentSeq = TTreeSequenceFactory<std::int32_t>::create(parmTree.get(), "parent", "parent_id/I", 128*1024);

    std::map<std::string, std::string> paramTree;   // maps child name to its parent name
    for (auto&& pair: ::g_allParams) {
        paramTree[pair.childName] = pair.parentName;
    }

    // Parameters tree must be ordered by name
    for (auto&& pair: paramTree) {
        TString str(pair.first);
        parmSeq.push_back(&str);

        int parentId = -1;
        auto nit = g_parm2id.find(pair.second);
        if (nit != g_parm2id.end()) parentId = nit->second;
        parentSeq.push_back(parentId);
    }
    parmTree->SetEntries();
    parmTree->Write();

    parmTree = std::make_unique<TTree>("ParmTree", "Parameter ID pairs (parent, child) ordered by (parent, child).");
    auto parmTreeSeq = TTreeSequenceFactory<RTypes::ParamTreeRepr>::create(parmTree.get(), "param_tree",
                                                                           RTypes::ParamTreeRepr::leafspec(),
                                                                           128*1024);

    std::vector<RTypes::ParamTreeRepr> paren2child;
    paren2child.reserve(g_numParams);
    for (auto&& pair: ::g_allParams) {
        int parentId = -1;
        auto nit = g_parm2id.find(pair.parentName);
        if (nit != g_parm2id.end()) parentId = nit->second;
        RTypes::ParmId childId = g_parm2id.at(pair.childName);
        paren2child.push_back(RTypes::ParamTreeRepr{parentId, childId});
    }
    std::sort(paren2child.begin(), paren2child.end());
    for (auto&& p2c: paren2child) {
        parmTreeSeq.push_back(p2c);
    }
    parmTree->SetEntries();
    parmTree->Write();

    return g_numParams;
}

template <typename OutputIter>
void _genResults(OutputIter iter,
                 int numResults,
                 RTypes::Timestamp startTime_usec,
                 RTypes::Timestamp endTime_usec)
{
    for (int i = 0; i < numResults; ++ i) {
        RTypes::ResultRepr result;
        result.time_usec = startTime_usec;
        if (i > 0) {
            result.time_usec += (endTime_usec - startTime_usec) * i / (numResults - 1);
        }
        result.parm_id = i % g_numParams;
        result.status = 0;
        result.tag_index = -1;
        result.histo_index = -1;
        *iter = result;
        ++iter;
    }
}

}


namespace dqm_archive {
namespace test {


std::unique_ptr<TFile>
ResultGenerator::makeTestRootFile(const std::string& path,
                                  int numResults,
                                  RTypes::Timestamp startTime_usec,
                                  RTypes::Timestamp endTime_usec,
                                  bool makeIndex) const
{
    // copied from DqmArchiveRoot
    std::string fpath = path;
    if (fpath.empty()) {
        // It wil be updated in place with actual file name.
        char file_template[] = "/tmp/dqmauXXXXXX";
        int fd = mkstemp(file_template);
        close(fd);
        unlink(file_template);
        fpath = file_template;
    }

    // create a file
    std::unique_ptr<TFile> file(TFile::Open(fpath.c_str(), "RECREATE", "DQM Results Archive"));

    ::_makeParamTree();

    // create all trees/branches
    auto resultTree = std::make_unique<TTree>("Results", "List of all results ordered by time");
    auto resultSeq = TTreeSequenceFactory<RTypes::ResultRepr>::create(resultTree.get(), "results", RTypes::ResultRepr::leafspec(), 128*1024);

    auto tagNameTree = std::make_unique<TTree>("TagNames", "List of all tag names");
    auto tagNameSeq = TTreeSequenceFactory<TString*>::create(tagNameTree.get(), "name");

    auto tagTree = std::make_unique<TTree>("Tags", "List of all tag values");
    auto tagValueSeq = TTreeSequenceFactory<RTypes::TagValueRepr>::create(tagTree.get(), "tags", RTypes::TagValueRepr::leafspec(), 128*1024);

    auto histoTree = std::make_unique<TTree>("Histograms", "Serialized histograms");
    auto histoIdSeq = TTreeSequenceFactory<RTypes::HistoIdRepr>::create(histoTree.get(), "histo_id", RTypes::HistoIdRepr::leafspec());
    auto histoSeq = TTreeSequenceFactory<TString*>::create(histoTree.get(), "histo_blob", 128*1024);

    // generate results
    ::_genResults(std::back_inserter(resultSeq), numResults, startTime_usec, endTime_usec);

    resultTree->SetEntries();
    resultTree->Write();

    if (makeIndex) {
        DqmaRootIndexer indexer("Results", "ResultIndex", "StatusCounters");
        indexer.index(file.get());
    }

    // write all other trees
    tagNameTree->Write();
    tagTree->Write();
    histoTree->Write();

    return file;
}

std::map<std::string, std::string>
ResultGenerator::makeParamTree() const
{
    std::map<std::string, std::string> tree;
    for (auto&& pair: ::g_allParams) {
        tree[pair.childName] = pair.parentName;
    }
    return tree;
}

std::vector<std::string>
ResultGenerator::parameters() const
{
    std::vector<std::string> params;
    for (auto&& pair: ::g_parm2id) {
        params.push_back(pair.first);
    }
    return params;
}

std::vector<ParamInfo>
ResultGenerator::childParameters(const std::string& parent) const
{
    std::vector<ParamInfo> params;
    for (auto&& pair: ::g_allParams) {
        if (pair.parentName != parent) continue;
        std::string childName = pair.childName;
        bool leaf = true;
        for (auto&& pair1: ::g_allParams) {
            if (pair1.parentName == childName) {
                leaf = false;
                break;
            }
        }
        params.emplace_back(childName, leaf);
    }
    return params;
}

std::vector<Result>
ResultGenerator::results(const std::string& param,
                         int numResults,
                         RTypes::Timestamp startTime_usec,
                         RTypes::Timestamp endTime_usec) const
{
    std::vector<Result> results;
    auto it = g_parm2id.find(param);
    if (it == g_parm2id.end()) {
        return results;
    }
    auto parmId = it->second;

    // generate results
    std::vector<RTypes::ResultRepr> reprResults;
    ::_genResults(std::back_inserter(reprResults), numResults, startTime_usec, endTime_usec);

    for (auto&& res: reprResults) {
        if (res.parm_id == parmId) {
            std::chrono::system_clock::time_point tp = std::chrono::system_clock::time_point() + std::chrono::microseconds(res.time_usec);
            results.emplace_back(dqm_core::Result::Status(res.status), tp, Result::TagList(), false);
        }
    }
    return results;
}

std::vector<std::shared_ptr<dqm_core::Result>>
ResultGenerator::dqm_results(int numResults,
                             RTypes::Timestamp startTime_usec,
                             RTypes::Timestamp endTime_usec) const
{
    std::vector<RTypes::ResultRepr> reprResults;
    ::_genResults(std::back_inserter(reprResults), numResults, startTime_usec, endTime_usec);

    std::vector<std::shared_ptr<dqm_core::Result>> results;
    for (auto&& res: reprResults) {
        boost::posix_time::ptime ts = boost::posix_time::from_time_t(0) + boost::posix_time::microseconds(res.time_usec);
        results.push_back(std::make_shared<dqm_core::Result>(dqm_core::Result::Status(res.status), nullptr, ts));
    }
    return results;
}

// ====================== TestDBClient methods ===========================

std::vector<std::string>
TestDBClient::datasets()
{
    // not reaaly used but implementation is trivial
    return std::vector<std::string>(1, m_dataset);
}

std::vector<daq::coca::RemoteArchive>
TestDBClient::archives(const std::string& dataset,
                       const std::string& archive,
                       const time_point& since,
                       const time_point& until)
{
    // this method is not used by DqmArchiveClientRootCoca
    return std::vector<daq::coca::RemoteArchive>();
}

std::vector<daq::coca::RemoteArchive>
TestDBClient::archives(const std::string& dataset,
                       const std::string& archive,
                       unsigned count,
                       unsigned skip)
{
    // this method is not used by DqmArchiveClientRootCoca
    return std::vector<daq::coca::RemoteArchive>();
}

unsigned
TestDBClient::countFiles(const std::string& dataset,
                         const std::string& archive,
                         const std::string& file)
{
    // DqmArchiveClientRootCoca only provides dataset parameter
    if (dataset == m_dataset) {
        return m_fileLocations.size();
    }
    return 0;
}

std::vector<daq::coca::RemoteFile>
TestDBClient::files(const std::string& dataset,
                    const std::string& archive,
                    const std::string& file,
                    const time_point& since,
                    const time_point& until)
{
    // `archive` is always empty but `file` can be a pattern like
    // "dqm_archive-*-0000123456.root"
    std::vector<daq::coca::RemoteFile> files;
    if (dataset == m_dataset) {
        for (const auto& fileLoc: m_fileLocations) {
            auto p = fileLoc.rfind('/');
            std::string const relPath = p == std::string::npos ? fileLoc : fileLoc.substr(p+1);
            if (not file.empty() and fnmatch(file.c_str(), relPath.c_str(), FNM_PATHNAME) != 0) {
                // 0 means match
                continue;
            }
            std::vector<daq::coca::FileLocation> locations;
            locations.emplace_back(std::string(), fileLoc);
            // use 1(MB) as size, it is not used
            files.emplace_back(m_dataset, relPath, locations, fileLoc, daq::coca::Size(10240), "md5:1234567890");
        }
    }
    return files;
}

std::vector<daq::coca::RemoteFile>
TestDBClient::files(const std::string& dataset,
                    const std::string& archive,
                    const std::string& file,
                    unsigned count,
                    unsigned skip)
{
    // `archive` and `file` are always empty
    std::vector<daq::coca::RemoteFile> files;
    if (dataset == m_dataset) {
        for (unsigned i = skip; i < skip + count; ++ i) {
            if (i >= m_fileLocations.size()) {
                break;
            }
            auto&& fileLoc = m_fileLocations[i];
            auto p = fileLoc.rfind('/');
            std::string const relPath = p == std::string::npos ? fileLoc : fileLoc.substr(p+1);
            std::vector<daq::coca::FileLocation> locations;
            locations.emplace_back(std::string(), fileLoc);
            // use 1(MB) as size, it is not used
            files.emplace_back(m_dataset, relPath, locations, fileLoc, daq::coca::Size(10240), "md5:1234567890");
        }
    }
    return files;
}

std::vector<std::string>
TestDBClient::fileLocations(const std::string& file,
                            const std::string& dataset,
                            Location /* location */)
{
    return fileLocations(file, dataset);
}

std::vector<std::string>
TestDBClient::fileLocations(const std::string& file,
                            const std::string& dataset)
{
    std::vector<std::string> locations;
    if (dataset == m_dataset) {
        for (const auto& fileLoc: m_fileLocations) {
            auto p = fileLoc.rfind('/');
            std::string const relPath = p == std::string::npos ? fileLoc : fileLoc.substr(p+1);
            if (relPath == file) {
                locations.push_back(fileLoc);
            }
        }
    }
    return locations;
}

daq::coca::Metadata
TestDBClient::metadata()
{
    // We don't care about this and it not trivial to implement
    throw std::runtime_error("not implemented");
}


}} // namespace dqm_archive::test
