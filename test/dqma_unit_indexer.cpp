
#include <algorithm>
#include <string>
#include <stdio.h>
#include <unistd.h>

#include "dqma_test_utils.h"
#include "dqm_archive/archiver/DqmaRootIndexer.h"
#include "dqm_archive/common/RTypes.h"
#include "dqm_archive/common/TTreeSequence.h"
#include "TFile.h"
#include "TTree.h"

#define BOOST_TEST_MODULE dqma_unit_indexer
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>


using namespace dqm_archive;

// ==============================================================

BOOST_AUTO_TEST_CASE(test_basic)
{
    // make a file with some data
    test::ResultGenerator gen;
    auto file = gen.makeTestRootFile("", 10, 1000, 1900, false);
    BOOST_TEST_MESSAGE("making new file " << file->GetName());

    TTree* resultTree = static_cast<TTree*>(file->Get("Results"));
    BOOST_TEST(resultTree->GetEntries() == 10);
    BOOST_TEST(file->Get("ResultIndex") == nullptr);

    // run indexer on it
    DqmaRootIndexer indexer("Results", "ResultIndex", "StatusCounters");
    BOOST_CHECK_NO_THROW(indexer.index(file.get()));

    // check that index tree exists and has expected number of ordered elements
    TTree* resultIndexTree = static_cast<TTree*>(file->Get("ResultIndex"));
    BOOST_TEST(resultIndexTree != nullptr);
    BOOST_TEST(resultIndexTree->GetEntries() == 10);
    TTreeSequence<RTypes::ResultIndex> resultIndexSeq(resultIndexTree);
    BOOST_TEST(std::is_sorted(resultIndexSeq.begin(), resultIndexSeq.end()));

    // counters tree should exist too
    BOOST_TEST(file->Get("StatusCounters") != nullptr);

    unlink(file->GetName());
    file->Close();
}
