//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <map>
#include <algorithm>
#include <thread>
#include <boost/program_options.hpp>
#include <signal.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "config/Configuration.h"
#include "dal/util.h"
#include "dqm_archive/archiver/DqmArchiverRoot.h"
#include "dqm_archive/archiver/DqmArchiverSQLite.h"
#include "dqm_archive/archiver/DqmaResultConverter.h"
#include "dqm_archive/archiver/DqmaResultReceiver.h"
#include "dqm_archive/archiver/DqmaRootReceiver.h"
#include "dqm_archive/archiver/IFileNameFactory.h"
#include "dqm_archive/archiver/ILTSArchiver.h"
#include "dqm_archive/archiver/ResultQueue.h"
#include "dqm_archive/archiver/ResultWriterThread.h"
#include "dqm_config/dal/DQParameter.h"
#include "dqm_config/dal/DQRegion.h"
#include "dqm_core/Result.h"
#include "dqmf/is/Result.h"
#include "dqmf/is/Tag.h"
#include "ers/ers.h"
#include "ipc/core.h"
#include "ipc/partition.h"
#include "owl/semaphore.h"
#include "TThread.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------
namespace po = boost::program_options;
using namespace dqm_archive;

namespace {

ERS_DECLARE_ISSUE (Issues, CommandLineSyntax,
        "bad command line syntax: " << error, ((std::string) error))
ERS_DECLARE_ISSUE(Issues, UncaughtException, "Uncaught exception, terminating.", )

// Initially this semaphore is at 0 and we pass it to anyone
// who can decide to stop the whole shebang (like signal handler
// or corba server)
OWLSemaphore g_semaphore;

// This mutex will be used by everybody who needs to work with ROOT
// to synchronize access to thread-unsafe ROOT. It has to be a global
// object because writer thread may become detached.
std::mutex g_rootMutex;

// signal handler stuff
int signalFired = 0;
extern "C"
void sighandler(int sig)
{
    g_semaphore.post();
    signalFired = sig;
}

void
scanTree(std::map<std::string, std::string>& paramTree, const std::shared_ptr<DqmaResultReceiver::ParamHistMap>& paramHist,
        const dqm_config::dal::DQRegion* region)
{

    // do the same for all sub-regions
    std::vector<const dqm_config::dal::DQRegion *> regions;
    region->get_all_regions(regions);
    for (const auto& subreg: regions) {
        paramTree[subreg->UID()] = region->UID();
        scanTree(paramTree, paramHist, subreg);
    }

    // and for sub-parameters
    std::vector<const dqm_config::dal::DQParameter *> parameters;
    region->get_all_parameters(parameters);
    for (const auto& param: parameters) {
        paramTree[param->UID()] = region->UID();
        if (paramHist) (*paramHist)[param->UID()] = param->get_InputDataSource();
    }
}

// special implementation of IFileNameFactory for fixed file name
class FixedFileNameFactory : public IFileNameFactory {
public:

    FixedFileNameFactory(const std::string& fname) : m_fname(fname) {}

    boost::filesystem::path makeFileName() override { return m_fname; }

private:
    boost::filesystem::path m_fname;
};

void runShebang(po::variables_map vm)
{
    const std::string partName = vm["partition"].as<std::string>();
    const std::string serverName = vm["server"].as<std::string>();
    const std::string fname = vm["output-file"].as<std::string>();
    const std::string configdb = vm["config-db"].as<std::string>();
    const std::string type = vm["type"].as<std::string>();
    const unsigned queueSize = vm["queue-size"].as<unsigned>();
    const unsigned workers = vm["workers"].as<unsigned>();
    const unsigned updateInterval = vm["update-interval"].as<unsigned>();
    const unsigned checkpointMinutes = vm["checkpoint-minutes"].as<unsigned>();
    const unsigned maxFileTimeIntervalSec = vm["max-file-interval-sec"].as<unsigned>();
    const unsigned maxFileResultCount = vm["max-file-result-count"].as<unsigned>();
    bool archiveHist = vm.count("archive-histo");
    bool addCfgHist = vm.count("add-cfg-histo");

    // Need to tell ROOT that we are running with more than one thread
    TThread::Initialize();

    // setup partition and IS
    IPCPartition partition(partName);

    // instantiate data transport
    typedef ResultQueue<std::shared_ptr<dqmf::is::Result>> IsResultQueue;
    typedef ResultQueue<std::shared_ptr<dqm_core::Result>> CoreResultQueue;
    auto isQueue = std::make_shared<IsResultQueue>(queueSize);
    auto coreQueue = std::make_shared<CoreResultQueue>(queueSize);

    std::shared_ptr<DqmaResultReceiver::ParamHistMap> paramHist;
    if (addCfgHist) paramHist = std::make_shared<DqmaResultReceiver::ParamHistMap>();

    std::map<std::string, std::string> paramTree;
    {
        // Get configuration
        Configuration config(configdb);

        const daq::core::Partition* partConfig = daq::core::get_partition(config, partName, 0);
        config.register_converter(new daq::core::SubstituteVariables(*partConfig));

        // get DQM parameters configuration tree
        std::vector<const dqm_config::dal::DQRegion*> regions;
        dqm_config::dal::DQRegion::get_root_regions(config, *partConfig, regions);
        ERS_DEBUG(1, "Config: found " << regions.size() << " root regions in configuration");

        // make a mapping from parameter name to its parent name
        for (const auto& region: regions) {
            paramTree[region->UID()] = std::string();
            scanTree(paramTree, paramHist, region);
        }
    }

    // instantiate archiver
    std::unique_ptr<IDqmArchiver> archiver;
    if (type == "sqlite") {
        archiver.reset(new DqmArchiverSQLite(fname, paramTree));
    } else if (type == "ROOT") {
        auto fnameFactory = std::make_unique<FixedFileNameFactory>(fname);
        archiver.reset(new DqmArchiverRoot(std::move(fnameFactory), std::shared_ptr<ILTSArchiver>(),
                                           paramTree, checkpointMinutes, maxFileTimeIntervalSec,
                                           maxFileResultCount, std::chrono::milliseconds::zero(),
                                           g_rootMutex));
    } else {
        std::cerr << "unexpected name for output type: " << type << "\n";
        return;
    }


    // start writer thread
    std::thread writer(ResultWriterThread(std::move(archiver), coreQueue, g_semaphore));

    // start converter thread
    std::shared_ptr<DqmaRootReceiver> rootReceiver = 
        std::make_shared<DqmaDefaultRootReceiver>(partition, g_rootMutex);
    std::thread cvtThread(DqmaResultConverter(isQueue, coreQueue, rootReceiver));

    // instantiate receiver and tell it to start running in background
    std::unique_ptr<DqmaResultReceiver> recv(new DqmaResultReceiver(partition, serverName, isQueue,
            archiveHist, updateInterval, workers, paramHist));
    recv->start();

    // wait until someone decides to stop and raises semaphore for us
    g_semaphore.wait();

    // stop receiver
    recv->stop();

    // send special beacon down the line to say that we are done
    isQueue->push(std::make_pair(std::string(), std::shared_ptr<dqmf::is::Result>()), true);

    // wait till writer thread finishes its job
    writer.join();
    cvtThread.join();
}

}

// ------------
// --- Main ---
// ------------
int
main (int argc, char **argv)
try {
    // register signal handler for usual set of terminating signals
    signal(SIGTERM, ::sighandler);
    signal(SIGINT, ::sighandler);
    signal(SIGQUIT, ::sighandler);

    // init IPC stuff
    IPCCore::init (argc, argv);

    // command line parsing
    po::options_description cmdline_options("Available options");
    cmdline_options.add_options()
            ("help,h", "print usage and exit")
            ("type,t", po::value<std::string>()->default_value("ROOT"), "Type of output file, ROOT or sqlite.")
            ("queue-size,q", po::value<unsigned>()->default_value(10000U), "Size of the result queue.")
            ("workers,w", po::value<unsigned>()->default_value(0), "Number of IS worker threads.")
            ("update-interval,u", po::value<unsigned>()->default_value(300), "Seconds between updates when result is not changing.")
            ("checkpoint-minutes,c", po::value<unsigned>()->default_value(10), "Minutes between checkpointing.")
            ("max-file-interval-sec", po::value<unsigned>()->default_value(0), "Limit on time period stored in one file in seconds, 0=no limit.")
            ("max-file-result-count", po::value<unsigned>()->default_value(0), "Limit on number of results stored in one file, 0=no limit.")
            ("archive-histo,H", "Archive histograms.")
            ("add-cfg-histo", "Add histograms from DQParameter configuration.")
            ("run-in-thread", "Run initialization code in a separate thread.")
            ;

    po::options_description pos_options("Hidden options");
    pos_options.add_options()
            ("partition", po::value<std::string>()->required(), "TDAQ partition name, required.")
            ("server", po::value<std::string>()->required(), "IS server name for DQMF results, required.")
            ("config-db", po::value<std::string>()->required(), "Configuration database to use.")
            ("output-file", po::value<std::string>()->required(), "Output file name, required.")
            ;

    po::options_description all_options("All options");
    all_options.add(cmdline_options).add(pos_options);

    po::positional_options_description positional;
    positional.add("partition", 1);
    positional.add("server", 1);
    positional.add("config-db", 1);
    positional.add("output-file", 1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(all_options).positional(positional).run(), vm);

    if (vm.count("help")) {
        std::cout << "Archiving of DQMF results.\n\n"
              << "Usage: " << argv[0] << " [options] partition server config-db output-file\n\n"
              << cmdline_options << "\n";
        return 0;
    }

    po::notify(vm);

    // check archiver type
    const std::string type = vm["type"].as<std::string>();
    if (type != "sqlite" && type != "ROOT") {
        std::cerr << "unexpected name for output type: " << type << "\n";
        return 2;
    }

    bool runInThread = vm.count("run-in-thread");
    if (runInThread) {
        std::cout << "starting dedicated thread\n";
        std::thread shebang(runShebang, vm);
        shebang.join();
    } else {
        runShebang(vm);
    }

} catch (const po::error& ex) {
    std::cerr << ex.what() << "\nUse --help option for usage information.\n";
    return 1;
} catch (const std::exception& ex) {
    ers::error(Issues::UncaughtException(ERS_HERE, ex));
    return 1;
} catch (...) {
    ers::error(Issues::UncaughtException(ERS_HERE));
    return 1;
}
