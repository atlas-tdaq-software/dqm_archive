/**
 *  Unit test for CocaFileNameCache.
 */

#include <memory>
#include <string>

#include "../src/client/CocaFileNameCache.h"
#include "dqma_test_utils.h"

#define BOOST_TEST_MODULE dqma_unit_coca_cache
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

namespace tt = boost::test_tools;
using namespace dqm_archive;

namespace {

struct Fixture {

    Fixture() : coca(dataset), cocaCache(coca, dataset) {}

    std::string const dataset = "dqm-dataset";
    test::TestDBClient coca;
    CocaFileNameCache cocaCache;
};

}

// ==============================================================

BOOST_FIXTURE_TEST_CASE(test_coca_cache_count, Fixture)
{
    // test case for runCount() method

    BOOST_TEST(cocaCache.runCount() == 0);

    std::pair<const char*, int> paths[] = {
        {"/data/coca/dqm_results-PARTITION-0000000001.root", 1},
        {"dqm_results-PART-0000120.root", 2},
        {"/data/coca/dqm_results-PART1-0000120-1.root", 2},
        {"/data/coca/dqm_results-PART2-0000120-1234567.root", 2},
        {"root://eosatlas/data/coca/archive-P-1234567890.root", 3},
        {"root://eosatlas/data/coca/archive-P-1234567891.root", 4},
        {"dqm_results-PART-0000125.root", 5}
    };

    for (auto&& pair: paths) {
        coca.registerFile(pair.first);
        BOOST_TEST_MESSAGE("added new file to coca, path=" << pair.first);
        BOOST_TEST(cocaCache.runCount() == pair.second);
    }
}

BOOST_FIXTURE_TEST_CASE(test_coca_cache_runs, Fixture)
{
    // test case for runs() method

    std::vector<RunInfo> runs;

    cocaCache.runs(runs, 1000);
    BOOST_TEST(runs.empty());

    const char* paths[] = {
        "/data/coca/dqm_results-PARTITION-0000000001.root",
        "dqm_results-PART-0000120.root",
        "/data/coca/dqm_results-PART1-0000120-1.root",
        "/data/coca/dqm_results-PART2-0000120-1234567.root",
        "root://eosatlas/data/coca/archive-P-1234567890.root",
        "root://eosatlas/data/coca/archive-P-1234567891.root",
        "dqm_results-PART-0000125.root"
    };

    for (auto&& path: paths) {
        coca.registerFile(path);
        BOOST_TEST_MESSAGE("added new file to coca, path=" << path);
    }

    {
        // get all runs, runs are returned in reverse registration order
        std::vector<RunInfo> runs;
        std::vector<RunInfo> expect_runs = {
            {125, "PART"}, {1234567891, "P"}, {1234567890, "P"},
            {120, "PART"}, {1, "PARTITION"}};
        cocaCache.runs(runs, 1000);
        BOOST_TEST(runs.size() == 5);
        BOOST_TEST(std::equal(runs.begin(), runs.end(), expect_runs.begin()));
    }
}

BOOST_FIXTURE_TEST_CASE(test_coca_cache_files, Fixture)
{
    // test case for files() method

    auto files = cocaCache.files(1);
    BOOST_TEST(files.empty());

    const char* paths[] = {
        "/data/coca/dqm_results-PARTITION-0000000001.root",
        "/data/coca/dqm_results-PART1-0000120-1.root",
        "/data/coca/dqm_results-PART2-0000120-1234567.root",
        "dqm_results-PART-0000120.root",
        "root://eosatlas/data/coca/archive-P-1234567895.root",
        "root://eosatlas/data/coca/archive-P-1234567891.root",
        "dqm_results-PART-0000125.root"
    };

    for (auto&& path: paths) {
        coca.registerFile(path);
        BOOST_TEST_MESSAGE("added new file to coca, path=" << path);
    }

    {
        std::vector<std::string> expect_files = {
            "dqm_results-PARTITION-0000000001.root"
        };
        auto files = cocaCache.files(1);
        BOOST_TEST(files == expect_files, tt::per_element());
    }
    {
        // Files are re-ordered to be sorted by name
        std::vector<std::string> expect_files = {
            "dqm_results-PART-0000120.root",
            "dqm_results-PART1-0000120-1.root",
            "dqm_results-PART2-0000120-1234567.root"
        };
        auto files = cocaCache.files(120);
        BOOST_TEST(files == expect_files, tt::per_element());
    }
    {
        std::vector<std::string> expect_files = {
            "archive-P-1234567891.root"
        };
        auto files = cocaCache.files(1234567891);
        BOOST_TEST(files == expect_files, tt::per_element());
    }
    {
        std::vector<std::string> expect_files = {
            "archive-P-1234567895.root"
        };
        auto files = cocaCache.files(1234567895);
        BOOST_TEST(files == expect_files, tt::per_element());
    }
    {
        std::vector<std::string> expect_files = {
            "dqm_results-PART-0000125.root"
        };
        auto files = cocaCache.files(125);
        BOOST_TEST(files == expect_files, tt::per_element());
    }
}
