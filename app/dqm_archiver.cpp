//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "dqm_archive/archiver/DqmaControllable.h"
#include "ers/ers.h"
#include "ipc/partition.h"
#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/ItemCtrl/ItemCtrl.h"
#include <TSystem.h>

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

    ERS_DECLARE_ISSUE(Issues, UncaughtException, "Uncaught exception, terminating.", )

    // stolen from some other place
    struct TRestoreSignals {
        TRestoreSignals() {
            if ( gSystem ) {
                for (int sig = 0; sig < kMAXSIGNALS; ++sig)
                    gSystem->ResetSignal((ESignals)sig,true);
            }
        }
    };

}

// ------------
// --- Main ---
// ------------
int
main (int argc, char **argv)
try {


    daq::rc::CmdLineParser cmdParser(argc, argv, true);

    // instantiate controllable
    auto controllable = std::make_shared<dqm_archive::DqmaControllable>();

    daq::rc::ItemCtrl ctrl(cmdParser, controllable);

    {
        // constructor of this class resets all
        // signal handlers to their default values
        TRestoreSignals __restore_signals__;
    }

    ctrl.init();

    ERS_LOG( "dqm_archiver has been started in the '"<< cmdParser.partitionName() << "' partition." );

    ctrl.run();

} catch (const daq::rc::CmdLineHelp& ex) {
    std::cout << "Archiving of DQMF results.\n\n"
          << "Usage: " << argv[0] << " [options]\n\n"
          << ex.message() << std::endl;
    return 0;
} catch (const daq::rc::CmdLineError& ex) {
    std::cerr << ex.what() << "\nUse --help option for usage information.\n";
    return 1;
} catch (const std::exception& ex) {
    ers::error(Issues::UncaughtException(ERS_HERE, ex));
    return 1;
} catch (...) {
    ers::error(Issues::UncaughtException(ERS_HERE));
    return 1;
}
