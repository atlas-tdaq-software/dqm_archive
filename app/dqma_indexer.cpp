//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "TFile.h"
#include "TTree.h"
#include "cmdl/cmdargs.h"
#include "dqm_archive/archiver/DqmaRootIndexer.h"
#include "ers/ers.h"
#include "RunControl/Common/CmdLineParser.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

ERS_DECLARE_ISSUE(Issues, UncaughtException, "Uncaught exception, terminating.", )

}

// ------------
// --- Main ---
// ------------
int
main (int argc, char **argv)
try {

    CmdLine cmdl(argv[0]);

    CmdArgUsage usage_arg('h', "help", "print usage and exit");
    cmdl << usage_arg;

    CmdArgStr rTreeName_arg('r', "result-tree", "<string>", "Result TTree name, def: Results");
    rTreeName_arg = "Results";
    cmdl << rTreeName_arg;

    CmdArgStr idxTreeName_arg('i', "index-tree", "<string>", "ResultIndex TTree name, def: ResultIndex");
    idxTreeName_arg = "ResultIndex";
    cmdl << idxTreeName_arg;

    CmdArgStr statTreeName_arg('s', "status-tree", "<string>", "StatusCounters TTree name, def: StatusCounters");
    statTreeName_arg = "StatusCounters";
    cmdl << statTreeName_arg;

    CmdArgBool remove_arg('d', "delete", "Delete existing trees.");
    cmdl << remove_arg;

    CmdArgStr fileName_arg("file", "File name", CmdArg::isPOSVALREQ);
    cmdl << fileName_arg;

    CmdArgvIter arg_iter(argc - 1, argv + 1);
    cmdl.description("Indexing of the ROOT files produced by dqm_archive.");
    cmdl.parse(arg_iter);

    // stringify all options
    std::string rTreeName = (const char*)rTreeName_arg;
    std::string idxTreeName = (const char*)idxTreeName_arg;
    std::string statTreeName = (const char*)statTreeName_arg;
    bool remove = int(remove_arg);

    // open ROOT file
    TFile* file = TFile::Open((const char*)fileName_arg, "UPDATE");

    // remove old trees
    if (remove) {
        std::string trees[] = {idxTreeName, statTreeName};
        for (auto treeName: trees) {
            auto obj = file->Get(treeName.c_str());
            if (obj) {
                auto tree = dynamic_cast<TTree*>(obj);
                if (tree) {
                    ERS_LOG("Removing existing tree " << treeName);
                    tree->Delete("all");
                } else {
                    ERS_LOG("Removing non-tree object with name " << treeName);
                    file->Delete((treeName+";*").c_str());
                }
            }
        }
    }

    // re-index
    dqm_archive::DqmaRootIndexer indexer(rTreeName, idxTreeName, statTreeName);
    try {
        indexer.index(file);
        ERS_LOG("Successfully indexed file " << (const char*)fileName_arg);
        file->Close();
        delete file;
    } catch (const ers::Issue& ex) {
        ers::error(ex);
        return 1;
    }

} catch (const std::exception& ex) {
    ers::error(Issues::UncaughtException(ERS_HERE, ex));
    return 1;
} catch (...) {
    ers::error(Issues::UncaughtException(ERS_HERE));
    return 1;
}
