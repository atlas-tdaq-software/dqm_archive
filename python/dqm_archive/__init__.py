"""Wrapper module for libpydqm_archive, see help('libpydqm_archive') for details"""

import sys
import os

# libpydqm_archive needs PyROOT to convert ROOT histograms into Python objects.
# PyROOT in some cases complains about missing header files for some classes
# if ROOT_INCLUDE_PATH is not set correctly.
#
# The piece of code below adds $TDAQC_INST_PATH/include to ROOT_INCLUDE_PATH
# to avoid those warnings. Note that if PyROOT is imported before this module
# then this workaround is not going to work.

if 'TDAQC_INST_PATH' in os.environ:
    # if ROOT is imported already we are too late for the game
    if 'ROOT' in sys.modules:
        import warnings
        warnings.warn("ROOT is already imported, use `import dqm_archive` before you import ROOT")
    else:
        roo_incl_path = os.environ.get('ROOT_INCLUDE_PATH', "")
        if roo_incl_path:
            roo_incl_path += ":"
        roo_incl_path += os.path.join(os.environ['TDAQC_INST_PATH'], "include")
        os.environ['ROOT_INCLUDE_PATH'] = roo_incl_path
        del roo_incl_path

if sys.platform == 'linux2':
    # on Linux with g++ one needs RTLD_GLOBAL for dlopen
    # which Python does not set by default
    import DLFCN
    flags = sys.getdlopenflags()
    sys.setdlopenflags( flags | DLFCN.RTLD_GLOBAL )
    from libpydqm_archive import *
    sys.setdlopenflags( flags )
    del flags
    del DLFCN
else:
    from libpydqm_archive import *

del os
del sys

# This is to get help() display the content
__all__ = [n for n in dir() if not n.startswith('_')]
